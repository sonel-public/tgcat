import dash
import dash_daq as daq
from dash import dcc, html, Input, Output, State
import pandas as pd
import sys

from tgcat.config import _catalog_infos
from tgcat.collect import scrape

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Checkbox Example with dcc.Store"),
    dcc.Checklist(
        id='checkboxes',
        labelStyle={'display': 'block',
                    'font-size': '85%', 'color': 'black',
                    'text-indent': '0px', 'margin': '0px 5px 0px 0px'},
        options=[{'label': v['sn'], 'value': v['function_name']} for v in _catalog_infos if v['type'] == 'data'],
        inputStyle={'vertical-align': 'middle', 'margin': '0px 5px 0px 0px'},
        style={'-webkit-column-count': '2', 'column-count': '2', 'column-width': '5rem'}),
    html.Div(id='output-container'),
    daq.BooleanSwitch(id='store-toggle', label='Show/Hide Data', on=True),
    html.Div(id='store-size-container'),  # New div to display the size of dcc.Store
    html.Div(id='store-content'),
    dcc.Store(id='data-store', data={})  # Store the dataframe as JSON in a dcc.Store
])


@app.callback(
    [Output('output-container', 'children'),
     Output('store-content', 'children'),
     Output('store-size-container', 'children')],
    [Input('checkboxes', 'value'),
     Input('store-toggle', 'on')],
    [State('data-store', 'data')]
)
def update_output(selected_values, store_on, data):
    if not selected_values:
        return "No option selected.", "", ""
    else:
        for val in selected_values:
            cat_name = val.replace('-', '_')
            archive_data = scrape.find(cat_name + '_?[0-9]*', scrape._metadata_path)
            cat = scrape.catalog_from_pickle(archive_data[0])
            print(archive_data)
            # if len(archive_data) == 1:
            #     cat = scrape.catalog_from_pickle(archive_data[0])
            # else:
            #     print(f"WARNING : No archive for the catalog {cat_name}")
            #     return None, None, None
            # cat = cat.union(cat)
    data =  cat.to_dataframe().to_dict('records')
    store_size = sys.getsizeof(str(data)) / (1024 * 1024) if data else 0  # Size of dcc.Store in MB
    output_text = f"Selected option(s): {', '.join(selected_values)}"
    store_content = f"Data stored in dcc.Store:\n{data}" if store_on else f"{store_size} MB"

    return output_text, store_content, f"Size of dcc.Store: {store_size:.2f} MB"


if __name__ == '__main__':
    app.run_server(debug=True)
