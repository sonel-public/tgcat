from pathlib import Path
import pandas as pd
import xarray as xr
import warnings
import logging

logging.basicConfig(level=logging.DEBUG)
log=logging.getLogger(__name__)
_default_null_resolution = pd.Timedelta(hours=0) #null resolution set to zero timedelta for ploting compatibility


@pd.api.extensions.register_dataframe_accessor("chrono")
class Chrono:
    def __init__(self, df: pd.DataFrame):
        self._validate(df)
        self._df = df
        self.data = pd.DataFrame(index=df.dropna().index,
                                 data={'dt': df.dropna().index.to_series().diff(),
                                       'start': df.dropna().index,
                                       })

    @staticmethod
    def _validate(df: pd.DataFrame):
        if not isinstance(df.index, pd.DatetimeIndex):
            raise TypeError(f"index of the dataframe should be a datetime")

    # def _get_orginial_resolution(self):
    #     "compute the original time resolution of the datetimeindex"
    #     if self._df.index.freqstr is None:
    #         resolution = self._get_principal_resolution()
    #         if resolution is None:
    #             resolution = _default_null_resolution
    #     else:
    #         resolution = pd.to_timedelta(self._df.index.freqstr)
    #     return resolution

    def _get_principal_resolution(self):
        "compute the principal resolution"
        freq_occurence = self._get_timedelta_distribution() #as a dictionnary
        if len(freq_occurence) == 0 :
            resolution = _default_null_resolution
        else:
            resolution = list(freq_occurence.keys())[0] #first key is TimeDelta of max occurence
        return resolution

    def _get_timedelta_distribution(self):
        """ get the distribution of the different sampling

        :return a dataframe sorted from maximum occurence to minimum (empty dataframe is no occurence)
        """
        dt_dico = self._df.dropna().index.to_series().diff().value_counts().sort_values(ascending=False).to_dict()
        if len(dt_dico) > 0:
            hist = dt_dico
        else:
            hist = {}
            log.debug(f" /!\ size of the histogram = {len(hist)}")
        return hist

    def _filter(self, resolution: pd.Timedelta = None):
        "filter the data by removoving all rows < resolution and comp"
        if resolution is None :
            return self.data
        return self.data.mask(self.data['dt'] <= resolution)

    def to_dataframe(self,
                     name: str = 'name',
                     resolution: pd.Timedelta = _default_null_resolution,
                     gap: pd.Timedelta = None):
        """compute the chronogram for a given resolution and gaps
        :resolution the resolution to take into account
        :gap pd.Timedelta minimum gaps to considers
        :return DataFrame with dt,start,end,name,sampling
        """
        # step 1 : filter the data <= than the resolution
        if resolution is _default_null_resolution:
            resolution = self._get_principal_resolution()

        data = self._filter(resolution=resolution)

        # step 2 : fill the chrono DataFrame
        if data['dt'].dropna().empty:
            chrono = pd.DataFrame(index=[self._df.index[0]],
                                  data={'dt': pd.NaT,
                                        'start': self._df.index[0],
                                        'end': self._df.index[-1],
                                        'name': name,
                                        'sampling': self._get_principal_resolution()})
        else:
            ends = (data['start'] - data['dt']).dropna().to_list() + [self._df.index[-1]]
            starts = data['start'].dropna().to_list()
            dts=[pd.NaT] + data['dt'].dropna().to_list()
            chrono = pd.DataFrame(index=starts,
                                  data={'dt': dts,
                                        'start': starts,
                                        'end': ends,
                                        'name': name,
                                        'sampling': self._get_principal_resolution()})
        return chrono


class GeslaDataset:
    """A class for loading data from GESLA text files into convenient in-memory
    data objects. By default, single file requests are loaded into
    `pandas.DataFrame` objects, which are similar to in-memory spreadsheets.
    Multifile requests are loaded into `xarray.Dataset` objects, which are
    similar to in-memory NetCDF files."""

    def __init__(self, meta_file, data_path):
        """Initialize loading data from a GESLA database.

        Args:
            meta_file (string): path to the metadata file in .csv format.
            data_path (string): path to the directory containing GESLA data
                files.
        """
        self.meta = pd.read_csv(meta_file)
        self.meta.columns = [
            c.replace(" ", "_")
            .replace("(", "")
            .replace(")", "")
            .replace("/", "_")
            .lower()
            for c in self.meta.columns
        ]
        self.meta.loc[:, "start_date_time"] = [
            pd.to_datetime(d) for d in self.meta.loc[:, "start_date_time"]
        ]
        self.meta.loc[:, "end_date_time"] = [
            pd.to_datetime(d) for d in self.meta.loc[:, "end_date_time"]
        ]
        self.meta.rename(columns={"file_name": "filename"}, inplace=True)
        self.data_path = data_path

    def file_to_pandas(self, filename, return_meta=True):
        """Read a GESLA data file into a pandas.DataFrame object. Metadata is
        returned as a pandas.Series object.

        Args:
            filename (string): name of the GESLA data file. Do not prepend path.
            return_meta (bool, optional): determines if metadata is returned as
                a second function output. Defaults to True.

        Returns:
            pandas.DataFrame: sea-level values and flags with datetime index.
            pandas.Series: record metadata. This return can be excluded by
                setting return_meta=False.
        """
        with open(self.data_path / filename, "r") as f:
            data = pd.read_csv(
                f,
                skiprows=41,
                names=["date", "time", "sea_level", "qc_flag", "use_flag"],
                sep="\s+",
                parse_dates=[[0, 1]],
                index_col=0,
                na_values=-99.9999
            )
            duplicates = data.index.duplicated()
            if duplicates.sum() > 0:
                data = data.loc[~duplicates]
                warnings.warn(
                    "Duplicate timestamps in file " + filename + " were removed.",
                )
            if return_meta:
                meta = self.meta.loc[self.meta.filename == filename].iloc[0]
                return data.dropna(), meta
            else:
                return data.dropna()

    def files_to_xarray(self, filenames):
        """Read a list of GESLA filenames into a xarray.Dataset object. The
        dataset includes variables containing metadata for each record.

        Args:
            filenames (list): list of filename strings.

        Returns:
            xarray.Dataset: data, flags, and metadata for each record.
        """
        data = xr.concat(
            [self.file_to_pandas(f, return_meta=False).to_xarray() for f in filenames],
            dim="station",
        )

        idx = [s.Index for s in self.meta.itertuples() if s.filename in filenames]
        meta = self.meta.loc[idx]
        meta.index = range(meta.index.size)
        meta.index.name = "station"
        data = data.assign({c: meta[c] for c in meta.columns})

        return data

    def load_N_closest(self, lat, lon, N=1, force_xarray=False):
        """Load the N closest GESLA records to a lat/lon location into a
        xarray.Dataset object. The dataset includes variables containing
        metadata for each record.

        Args:
            lat (float): latitude on the interval [-90, 90]
            lon (float): longitude on the interval [-180, 180]
            N (int, optional): number of locations to load. Defaults to 1.
            force_xarray (bool, optional): if N=1, the default behavior is to
                return a pandas.DataFrame object containing data/flags and a
                pandas.Series object containing metadata. Set this argument to
                True to return a xarray Dataset even if N=1. Defaults to False.

        Returns:
            xarray.Dataset: data, flags, and metadata for each record.
        """
        N = int(N)
        if N <= 0:
            raise Exception("Must specify N > 0")

        d = (self.meta.longitude - lon) ** 2 + (self.meta.latitude - lat) ** 2
        idx = d.sort_values().iloc[:N].index
        meta = self.meta.loc[idx]

        if (N > 1) or force_xarray:
            return self.files_to_xarray(meta.filename.tolist())

        else:
            data, meta = self.file_to_pandas(meta.filename.values[0])
            return data, meta

    def load_lat_lon_range(
        self,
        south_lat=-90,
        north_lat=90,
        west_lon=-180,
        east_lon=180,
        force_xarray=False,
    ):
        """Load GESLA records within a rectangular lat/lon range into a xarray.
        Dataset object.

        Args:
            south_lat (float, optional): southern extent of the range. Defaults
                to -90.
            north_lat (float, optional): northern extent of the range. Defaults
                to 90.
            west_lon (float, optional): western extent of the range. Defaults
                to -180.
            east_lon (float, optional): eastern extent of the range. Defaults
                to 180.
            force_xarray (bool, optional): if there is only one record in the
                lat/lon range, the default behavior is to return a
                pandas.DataFrame object containing data/flags and a
                pandas.Series object containing metadata. Set this argument to
                True to return a xarray.Dataset even if only one record is
                selected. Defaults to False.

        Returns:
            xarray.Dataset: data, flags, and metadata for each record.
        """
        if west_lon > 0 & east_lon < 0:
            lon_bool = (self.meta.longitude >= west_lon) | (
                self.meta.longitude <= east_lon
            )
        else:
            lon_bool = (self.meta.longitude >= west_lon) & (
                self.meta.longitude <= east_lon
            )
        lat_bool = (self.meta.latitude >= south_lat) & (self.meta.latitude <= north_lat)
        meta = self.meta.loc[lon_bool & lat_bool]

        if (meta.index.size > 1) or force_xarray:
            return self.files_to_xarray(meta.filename.tolist())

        else:
            data, meta = self.file_to_pandas(meta.filename.values[0])
            return data, meta

meta_file = Path(r'C:\Users\ltestut\PycharmProjects\tgcat\tgcat\collect\archives\data\GESLA3_ALL.csv')
data_path = Path(r'C:\Users\ltestut\PycharmProjects\tgcat\tgcat\collect\archives\data\GESLAv3')

g3 = GeslaDataset(meta_file=meta_file, data_path=data_path)


#%%data from a single file
filename = "nordrerose-nor-dnk-cmems"
data, meta = g3.file_to_pandas(filename)
data


#%%data for a list of files
filenames = [
    "abrams_river-380-can-meds",
    "acajutla-082c-slv-uhslc",
    "yoshioka-hd26-jpn-jodc_jcg",
    "west_point_a_la_hache-8761494-usa-noaa",
]
xr_dataset = g3.files_to_xarray(filenames)
print(xr_dataset)


#%%Nclosest record from lat/lon
data = g3.load_N_closest(lat=43.83, lon=-65.95, N=10)
print(data.site_name.values)

#Load data from the records in a lat/lon range
south_lat = 15
north_lat = 30
west_lon = -180
east_lon = -140

data = g3.load_lat_lon_range(
    south_lat=south_lat,
    north_lat=north_lat,
    west_lon=west_lon,
    east_lon=east_lon,
)
print(data.site_name.values)

