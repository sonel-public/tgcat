import os
from datetime import datetime, timedelta
import pandas as pd
import netCDF4
from tgcat.config import _raw_data_path
from ftplib import FTP
import xarray as xr

def parser_decyear2dt(s):
    """ parse a string decimal '   1992.3256' year to datetime """
    dec_year = float(s.strip())
    year = int(dec_year)
    rem = dec_year - year
    base = datetime(year, 1, 1)
    result = base + timedelta(seconds=(base.replace(year=base.year + 1) - base).total_seconds() * rem)
    return result


def psmsl(file):
    df = pd.read_csv(file, delimiter=';', names=['date', 'level', 'code', 'code2'], na_values=-99999)
    if len(df) >= 1 :
        index = df['date'].astype(str).apply(parser_decyear2dt)
        df.set_index(index.round('D'), inplace=True)
        df.drop(columns=['date', 'code', 'code2'], inplace=True)
        return df
    else:
        return None

def uhslc_fd_d(file):
    data = pd.read_csv(
        file,
        sep=',',
        skiprows=1,
        names=['Year', 'Month', 'Day', 'Level'],
        na_values=[-32767]
    )
    data['Date'] = pd.to_datetime(data[['Year', 'Month', 'Day']])
    data = data.drop(columns=['Year', 'Month', 'Day'])
    data = data.set_index('Date')
    return data

def uhslc_fd_h(file):
    data = pd.read_csv(
        file,
        sep=',',
        skiprows=1,
        names=['Year', 'Month', 'Day', 'Hour', 'Level'],
        na_values=[-32767]
    )
    data['Date'] = pd.to_datetime(data[['Year', 'Month', 'Day', 'Hour']])
    data = data.drop(columns=['Year', 'Month', 'Day', 'Hour'])
    data = data.set_index('Date')
    return data

def uhslc_rq(file):
    data = pd.read_csv(
        file,
        sep=',',
        skiprows=1,
        na_values=[-32767]
    )
    data = data.drop(columns=['degrees_north', 'degrees_east', 'Unnamed: 4',
                              'Unnamed: 5', 'Unnamed: 6', 'Unnamed: 7', 'Unnamed: 8', 'Unnamed: 9',
                              'Unnamed: 10', 'Unnamed: 11', 'Unnamed: 12', 'Unnamed: 13',
                              'millimeters.1'])
    data = data.rename(columns={'millimeters': 'Level', 'UTC': 'Date'})
    data['Date'] = pd.to_datetime(data.Date)
    data['Date'] = pd.to_datetime(data['Date']).dt.strftime('%Y-%m-%d %H:%M:%S')
    data['Date'] = data.Date.astype('datetime64[ns]')
    data = data.set_index('Date')
    return data

def gesla(file):
    data = pd.read_csv(file,
                       skiprows=41,
                       names=["date", "time", "sea_level", "qc_flag", "use_flag"],
                       sep="\s+",
                       parse_dates=[[0, 1]],
                       index_col=0,
                       na_values=-99.9999
                       )
    duplicates = data.index.duplicated()
    if duplicates.sum() > 0:
        data = data.loc[~duplicates]
    return data.dropna()

def misela(file):
    file2read = netCDF4.Dataset(file,'r')
    start_end_time = file2read.variables['time'].getncattr('start_end_time').split('\n')
    start = start_end_time[0]
    end = start_end_time[1]
    date = pd.date_range(start=start, end=end, freq='min')
    level = file2read.variables['nslott'][:]
    data = pd.DataFrame(data=list(zip(date, level)), columns=['Date', 'Level'])
    data = data.set_index('Date')
    return data

def cmems(file):

    if not (_raw_data_path / 'cmems').exists():
        (_raw_data_path / 'cmems').mkdir()

    ftp_site = r'nrt.cmems-du.eu'
    ftp_file = file.split('ftp://nrt.cmems-du.eu')[-1]
    filename = ftp_file.split('/')[-1]
    ftp = FTP(ftp_site)
    ftp.login('ltestut', '@Copernicus2017')
    handle = open(_raw_data_path / 'cmems' / filename, 'wb')
    ftp.retrbinary("RETR %s" % ftp_file, handle.write)
    handle.close()

    ds = xr.open_dataset(_raw_data_path / 'cmems' / filename)
    df = pd.DataFrame(data=[ds['TIME'].values[0], ds['SLEV'].values[0]], columns=['Date', 'Level'])
    df = df.set_index('Date')

    df.to_pickle(_raw_data_path / 'cmems' / f"{filename.split('.nc')[0]}_{int(ds['SLEV'].time_sampling/60)}_h.pickle")
    os.remove(_raw_data_path / 'cmems' / filename)





