"""
This module regroups all tgcat main objects :

- **Catalog** : *objetc to handle tide gauge sites*
- **Site** : *object to handle all informations from a tide gauge*

.. Warning:: This code is still under development and significant changes may take place in future versions.
"""

# --- General imports ---
# -----------------------

from pathlib import Path
import os
# import cartopy.crs as ccrs
import numpy as np
# import matplotlib.pyplot as plt
import pandas as pd
# import folium
# from folium.plugins import MarkerCluster
# import branca.colormap as cm
from iso3166 import countries_by_alpha2, countries
from shapely.geometry import Polygon
import h3

from geopy.geocoders import Nominatim
from copy import deepcopy
# from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
# import cartopy.feature as cfeature
# import matplotlib.patheffects as PathEffects
# import shapely
import geopandas as gpd

# TODO implement the H3 hierarchical geospatial hexagonal index grid system (https://h3geo.org/docs/)
# TODO install the version 4 of H3 (big changes since 3.7)
# TODO use h3 to locate the country of installation


# --- General settings ---
# ------------------------

# Turn off the auto_truncate of links in pd.DataFrame
pd.set_option('display.max_colwidth', None)

_site_updatable_attr_list = ['provider', 'name']
_site_updatable_attr_dict = ['data', 'id', 'link', 'pip']
_site_updatable_attr = _site_updatable_attr_dict + _site_updatable_attr_list
_default_null_resolution = pd.Timedelta(hours=0)  # null resolution set to zero timedelta for ploting compatibility
_default_h3_resolution = 7


# --- General functions ---
# -------------------------


def as_a_list(value):
    """
    Function to transform a str/int/float to a list

    :param value: Value to transform
    :type value: str, int or float
    :return: list
    """
    if isinstance(value, (str, int, float)):
        val = [value]
    else:
        val = value
    return val


# --- Main classes ---
# --------------------
@pd.api.extensions.register_dataframe_accessor("chrono")
class Chrono:
    def __init__(self, df: pd.DataFrame):
        self._validate(df)
        self._df = df
        self.data = pd.DataFrame(index=df.dropna().index,
                                 data={'dt': df.dropna().index.to_series().diff(),
                                       'start': df.dropna().index,
                                       })

    @staticmethod
    def _validate(df: pd.DataFrame):
        if not isinstance(df.index, pd.DatetimeIndex):
            raise TypeError(f"index of the dataframe should be a datetime")

    def _get_orginial_resolution(self):
        "compute the original time resolution of the datetimeindex"
        if self._df.index.freqstr is None:
            resolution = self._get_principal_resolution()
            if resolution is None:
                resolution = _default_null_resolution
        else:
            resolution = pd.to_timedelta(self._df.index.freqstr)
        return resolution

    def _get_principal_resolution(self):
        "compute the principal resolution"
        freq_occurence = self._get_timedelta_distribution()  # as a dictionnary
        if len(freq_occurence) == 0:
            resolution = _default_null_resolution
        else:
            resolution = list(freq_occurence.keys())[0]  # first key is TimeDelta of max occurence
        return resolution

    def _get_timedelta_distribution(self):
        """ get the distribution of the different sampling

        :return a dataframe sorted from maximum occurence to minimum (empty dataframe is no occurence)
        """
        dt_dico = self._df.dropna().index.to_series().diff().value_counts().sort_values(ascending=False).to_dict()
        if len(dt_dico) > 0:
            hist = dt_dico
        else:
            hist = {}
        #           log.debug(f" /!\ size of the histogram = {len(hist)}")
        return hist

    def _filter(self, resolution: pd.Timedelta = None):
        "filter the data by removoving all rows < resolution and comp"
        if resolution is None:
            return self.data
        return self.data.mask(self.data['dt'] <= resolution)

    def to_dataframe(self,
                     name: str = 'name',
                     resolution: pd.Timedelta = _default_null_resolution,
                     gap: pd.Timedelta = None):
        """compute the chronogram for a given resolution and gaps
        :resolution the resolution to take into account
        :gap pd.Timedelta minimum gaps to considers
        :return DataFrame with dt,start,end,name,sampling
        """
        # step 1 : filter the data <= than the resolution
        if resolution is _default_null_resolution:
            resolution = self._get_principal_resolution()

        data = self._filter(resolution=resolution)

        # step 2 : fill the chrono DataFrame
        if data['dt'].dropna().empty:
            chrono = pd.DataFrame(index=[self._df.index[0]],
                                  data={'dt': pd.NaT,
                                        'start': self._df.index[0],
                                        'end': self._df.index[-1],
                                        'name': name,
                                        'sampling': self._get_principal_resolution()})
        else:
            ends = (data['start'] - data['dt']).dropna().to_list() + [self._df.index[-1]]
            starts = data['start'].dropna().to_list()
            dts = [pd.NaT] + data['dt'].dropna().to_list()
            chrono = pd.DataFrame(index=starts,
                                  data={'dt': dts,
                                        'start': starts,
                                        'end': ends,
                                        'name': name,
                                        'sampling': self._get_principal_resolution()})
        return chrono


class Catalog(object):
    """
    Class Catalog container of Site() objects
    """

    def __init__(self, name, sites):
        self.name = name
        self.sites = sites

        # compute the label and find the duplicates
        self.build_label()
        self.duplicates = self._count_same_label()

    def __str__(self):
        return f" {self.name.upper()} catalog contains {len(self.sites)} sites"

    def __repr__(self):
        return f" Catalog : {self.name} with #{len(self.sites)} sites "

    def __len__(self):
        return len(self.sites)

    def __iter__(self):
        for s in self.sites:
            yield s

    def __getitem__(self, key):
        """
        Redefine slicing for Catalog object :
            - if key is a single number, return the correspondign site
            - if key is a slice, return the corresponding Catalog
        """

        # Index on int values
        if isinstance(key, int):
            item = key
            return self.sites[key]

        # Slice on values
        if isinstance(key, slice):
            _start, _stop, _step = key.start, key.stop, key.step
            if len(self.sites[_start:_stop:_step]) != 1.:
                return Catalog(sites=self.sites[_start:_stop:_step], name=self.name)
            elif len(self.sites[_start:_stop:_step]) == 1.:
                return self.sites[_start:_stop:_step][0]

        else:
            raise KeyError(f"Bad key type ({key}) - Must be integer or slice")

    def __add__(self, other):
        """
        The add method is quivalent to the union of two catalogs

        :type other: Catalog
        :return: Catalog

        ..todo: why the pip attribute not adding together ?
        """
        if self == other:
            return self

        name = self.name + '_' + other.name
        sites = self.sites[:]  # shallow copy of site
        [sites.append(site) for site in other.sites]
        return Catalog(name=name, sites=sites)

    def __eq__(self, other):
        """
        Boolean methods to test if a catalog is equal to another one (same labels)

        :param other: other Catalog
        """
        if id(self) == id(other):  # return True if it is the same object
            return True
        else:
            self_labels = [s.h3 for s in self]
            other_labels = [s.h3 for s in other]
            if self_labels == other_labels:
                return True
            else:
                return False

    def clip(self, area):
        """
        Return the Catalog of sites in the given area

        :param area: list of [lon_min, lon_max, lat_min, lat_max] or string with continent name
        :return: the clipped Catalog

        ..Note: The supported continent name are : 'Africa', 'Antarctica', 'Asia', 'Europe', 'North America',
                    'Oceania', 'Seven seas (open ocean)', 'South America'
        ..todo: clip on a given shpaley.Polygon object
        todo implement or remove this method to be discussed
        """

        gdf = self.to_gdataframe()

        if isinstance(area, list) and len(area) == 4.:
            lon_min, lon_max, lat_min, lat_max = area
            polygon = Polygon([(lon_min, lat_min), (lon_min, lat_max),
                               (lon_max, lat_max), (lon_max, lat_min), (lon_min, lat_min)])
            poly_gdf = gpd.GeoDataFrame([1], geometry=[polygon], crs=gdf.crs)
            gdf_clipped = gdf.clip(poly_gdf)
        elif isinstance(area, str):
            world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
            continent = world[world["continent"] == area.title()]
            gdf_clipped = gdf.clip(continent)
        else:
            raise ValueError('Area should be a list of 4 coordinates or a continent name.')

        s_clipped = [self[int(i)] for i in gdf_clipped.index.values]

        return Catalog(name=self.name + '_clipped', sites=s_clipped)

    # def find_country(self, output=False, **kwargs): #TODO this method could be update with geopy or removed ? to be decided
    #     """
    #     Find the country based on the location coordinates.
    #
    #     :param output: if True, return the list of countries
    #
    #     >>> Catalog.find_country()
    #
    #     ..todo: Optimize for Catalog with a lot of countries (geopadnas?)
    #     """
    #
    #     print("WARNING : This method could take time for large datasets !")
    #
    #     if kwargs.get('force_update', False):
    #         [delattr(site, 'country') for site in self]
    #     # Test if ounctry already exist
    #     try:
    #         country_list = [site.country for site in self]
    #         print(" This Catalog already have country information.")
    #     except:
    #         # 1 First turn with geopandas
    #         world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    #         world.loc[world.name == 'France', 'iso_a3'] = 'FRA'
    #         world.loc[world.name == 'Norway', 'iso_a3'] = 'NOR'
    #         world.loc[world.name == 'Kosovo', 'iso_a3'] = 'XKX'
    #         world.loc[world.name == 'N. Cyprus', 'iso_a3'] = 'CYP'
    #         world.loc[world.name == 'Somaliland', 'iso_a3'] = 'SOM'
    #         world_crs = world.crs
    #         cat_df = self.to_dataframe()
    #         cat_gdf = gpd.GeoDataFrame(cat_df, geometry=gpd.points_from_xy(cat_df.longitude, cat_df.latitude),
    #                                    crs=world_crs)
    #
    #         join_gdf = gpd.sjoin(cat_gdf, world, how='left', predicate='intersects')
    #
    #         # Update country parameter for known sites
    #         for i in join_gdf[~pd.isna(join_gdf.name_right)].index:
    #             self[i].country = countries.get(join_gdf.iloc[i].iso_a3).name
    #
    #         geolocator = Nominatim(user_agent='tgcat')
    #         # find country for unknowns sites
    #         for k, i in enumerate(join_gdf[pd.isna(join_gdf.name_right)].index):
    #             print(f" Finding country {k + 1} / {len(join_gdf[pd.isna(join_gdf.name_right)].index)}", end='\r')
    #             lat, lon = np.mean(self[i].lat), np.mean(self[i].lon)
    #             location = geolocator.reverse((lat, lon))
    #             try:
    #                 count = countries_by_alpha2[location.raw['address']['country_code'].upper()].name
    #             except:
    #                 count = 'Unknown'
    #             self[i].country = count
    #
    #     if output:
    #         country_list = [site.country for site in self]
    #         return country_list

    def build_label(self, **kwargs):
        """
        Build a label for each site based on the latitude and longitude information.

        :return: Updated Sites objects
        """
        # country = self.find_country()
        for i, s in enumerate(self.sites):
            latlist = f"{s.lat[0]:07.3f}".replace('-', 'S').split('.')
            if latlist[0].startswith('0'):
                latlist[0] = latlist[0][0].replace('0', 'N') + latlist[0][1:]
            lonlist = f"{s.lon[0]:08.3f}".replace('-', 'W').split('.')
            if lonlist[0].startswith('0'):
                lonlist[0] = lonlist[0][0].replace('0', 'E') + lonlist[0][1:]
            label = latlist[0] + lonlist[0] + '-' + latlist[1] + lonlist[1]
            # self.sites[i].country = country[i].alpha2  # 3 digit iso code
            self.sites[i].label = label
        return self.sites

    def merge_duplicates(self, inplace=True, **kwargs):
        """
        Merge the duplicates, remove them and return the Catalog in place.

        :param inplace: if False, return the updated Catalog, else modify the initial one (defaut is True)
        :return: The updated Catalog without duplicates
        """

        verbose = kwargs.get('verbose', False)
        delete_list = []
        catalog = deepcopy(self)
        if len(catalog.duplicates) == 0:
            print(f" Find no duplicates for this catalog.")
            return self

        for label, index in catalog.duplicates.items():
            first = index[0]
            for i in index[1:]:
                delete_list.append(i)
                if verbose: print(f"cat.merge_duplicates ==> cat.sites[{first}] with cat.sites[{i}]")
                catalog.sites[first].__add__(catalog.sites[i], **kwargs)
        for i in sorted(delete_list, reverse=True):
            if verbose: print(f"{i} delete site {self.sites[i].label}/{self.sites[i].name}")
            del catalog.sites[i]
        catalog.duplicates = catalog._count_same_label()  # update the duplicate
        catalog.name = catalog.name + '_merged'

        if inplace:
            self.sites = catalog.sites
            self.name = catalog.name
            self.build_label()
            self.duplicates = self._count_same_label()
        else:
            return catalog

    def uniq_label(self):
        """
        Return the uniq string label of the catalog

        :return: List of the uniq labels
        """
        labels = set()
        [labels.add(s.label) for s in self.sites]  # list the uniq label
        return labels

    def uniq_h3(self, resolution=_default_h3_resolution):
        """
        Return the uniq h3 of the catalog
        :return: List of uniq h3 dict
        """
        cellid = set()
        [cellid.add(site.h3[f'hex{resolution}']) for site in self.sites]
        return cellid

    def select(self, input, on='label'):
        """
        Extract data from a Catalog with a given parameter

        :param input: values of the selection parameter
        :type input: list or str
        :param on: selection parameter type (default is 'label', could be 'id','country','label_zone', 'name')
        :return: Catalog with only extracted informations

        >>> Catalog.select(['France', 'Italy'], on='country'])

        ..todo : Finish the extraction tool and test
        """

        extracted = []
        if isinstance(input, list):  # set the list to a set()
            listing = set(input)
        if isinstance(input, str):  # set the string to a set()
            listing = set([input])

        if on == 'id':
            for site in self.sites:
                for i in listing:
                    if i.lower() in {k.lower() for k in site.id.keys()}:
                        extracted.append(site)

        if on == 'label':
            for site in self.sites:
                if site.label in listing:
                    extracted.append(site)

        if on == 'country':
            try:
                if self.sites[0].country:
                    pass
            except AttributeError:
                self.find_country(output=False)

            for site in self.sites:
                if site.country in input:
                    extracted.append(site)

        if on == 'label_zone':
            for site in self.sites:
                if site.label.split('-')[0] in listing:
                    print(f" ... extraction of {site.name}")
                    extracted.append(site)

        if on == 'name':
            for site in self.sites:
                match = [site.has_name(name, contains=True) for name in listing]
                if True in match:
                    extracted.append(site)

        return Catalog(name='selection_of_' + self.name + '_on_' + on, sites=extracted)

    def intersection(self, other):
        """
        Compute the *label* intersection between two catalogs

        :type other: Catalog
        :return: Catalog

        ..todo: implement the catalog intersection
        """
        raise NotImplementedError()

    def union(self, other, **kwargs):
        """
        Compute the union between two catalogs

        :type other: Catalog
        :return: Catalog

        ..todo : test if the 2 sites belong to 2 different catalog otherwise do not merge (keep "site" definition)
        """
        cat_united = self + other
        if cat_united == self:
            return self.merge_duplicates(inplace=True)
        if kwargs.get('merge_duplicates', False):
            cat_united_merged = cat_united.merge_duplicates(inplace=False)
            cat_united_merged.name = self.name + '_U_' + other.name
            cat_united = cat_united_merged
        return cat_united

    def _label_index_dict(self):
        """
        Get the list of sites index associated with a uniq label.

        :rtype: dict

        >>> cat._label_index_dict()
        >>> {'N45W001-914328': [0, 1, 2], 'N14W017-633450': [3], 'N14W017-633417': [4]}
        """
        label_index_dict = {key: [] for key in self.uniq_label()}
        for i, s in enumerate(self.sites):
            label_index_dict[s.label].append(i)
        return label_index_dict

    def _h3_index_dict(self, resolution=_default_h3_resolution):
        """
        Get the list of sites  h3 index at a given resolution associated

        :rtype: dict

        >>> cat._h3_index_dict()
        >>> {'80dbfffffffffff': [0, 1, 2], '80e5fffffffffff': [3]}
        """
        h3_index_dict = {key: [] for key in self.uniq_h3(resolution=resolution)}
        for i, s in enumerate(self.sites):
            h3_index_dict[s.h3[f'hex{resolution}']].append(i)
        return h3_index_dict

    def _count_same_label(self):
        """
        Get the duplicated label and the associates sites index.

        :rtype: dict

        >>> cat._count_same_label()
        >>> {'N45W001-914328': [0, 1, 2]}
        """
        # init a dictionary with label as key and list of index as value
        duplicates_dict = {}
        for k, v in self._label_index_dict().items():
            if len(v) > 1:
                duplicates_dict.update({k: v})
        return duplicates_dict

    def _count_same_h3(self, resolution=_default_h3_resolution):
        """
        Get the duplicated label and the associates sites index.

        :rtype: dict

        >>> cat._count_same_h3()
        >>> {'88ce4ffff': [0, 1, 2]}
        """
        # init a dictionary with label as key and list of index as value
        duplicates_dict = {}
        for k, v in self._h3_index_dict(resolution=resolution).items():
            if len(v) > 1:
                duplicates_dict.update({k: v})
        return duplicates_dict

    def _count_same_root_label(self):
        """
        Count the number of sites with same root_label (format : SXXWXX)

        :rtype: dict

        >>> cat._count_same_root_label()
        >>> {'N45W001': [0, 1, 2], 'N14W017': [3, 4]}
        """
        possible_duplicates = {key.split('-')[0]: [] for key in self._label_index_dict()}
        for key, value in self._label_index_dict().items():
            possible_duplicates[key.split('-')[0]] += value
        return possible_duplicates

    def count_pip_len(self):

        names = []
        for s in self.sites:
            for k in s.pip.keys():
                if k not in names:
                    names.append(k)

        len_list = []
        for n in names:
            ll = []
            for s in self.sites:
                try:
                    if s.pip[n]:
                        ll += [s.name]
                except:
                    pass
            len_list.append(len(ll))

        return pd.DataFrame(data={'catalog': names, 'len_tot': len_list,
                                  'len_selected': len(names) * [0]})

    def to_dataframe(self):
        """
        Create a dataframe of the Catalog

        :return: dataframe object

        ..todo : add the number of duplicates
        """
        columns = ['id', 'label',
                   'name', 'longitude', 'latitude',
                   'cat',
                   '#id', 'ids',
                   '#link', 'links',
                   '#data', 'datas',
                   'h3']
        data = []
        for i, s in enumerate(self.sites):
            data.append([i, s.label,
                         s.name[0], s.lon[0], s.lat[0],
                         [k for k, v in s.pip.items() if v == [True]][0],
                         len(s.id), s.id,
                         len(s.link), s.link,
                         len(s.data.values()), s.data, s.h3])
        return pd.DataFrame(data=data, columns=columns)

    def to_gdataframe(self):
        """
        Create a geodataframe of the Catalog

        :return: dataframe object
        """
        df = self.to_dataframe()
        gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude)).set_crs(epsg=4326)
        return gdf

    def to_csv(self, **kwargs):
        """
        Export a catalogue to a csv file.

        :param filename: path of the file to save (default is None)
        :type filename: str
        """
        df = self.to_dataframe()

        _root_dir = os.path.dirname(os.path.abspath(__file__))
        _csv_path = Path(_root_dir) / 'tgcat/collect/csv/'
        save_path = kwargs.get('filename', _csv_path / Path(self.name + '.csv'))

        if kwargs.get('verbose', False): print(f" Export catalog to {save_path}")

        df.to_csv(save_path, index=False)

    def describe(self):
        """
        Describe some statistics on the catalogue
        """
        print(f" Catalog : {self.name}")
        print(f"  #{len(self):04} stations => #{len(self.uniq_label())} uniq label")
        for res in np.arange(0, 16):
            print(
                f"                           => #{len(self.uniq_h3(resolution=res))} different cell ID at resolution={res}")
        print(f"  {' ' * 15}=> #{len(self) - len(self.uniq_label())} duplicates")

    def display(self, duplicates=False, name_length=10):
        """
        Display the Catalog in the console

        :param duplicates: choose to only show duplicated sites (default is False)
        :param name_length: number of characters to display for site names
        """
        display_name_length = name_length
        if duplicates:
            for label, index in self.duplicates.items():
                for i in index:
                    print(i, self.sites[i])
                print(f"-------- ")
        else:
            for i, s in enumerate(self):
                if len(s.name[0]) > display_name_length:
                    name = s.name[0][:display_name_length] + ' ...'
                else:
                    name = s.name[0]
                print(f" site #{i:05} : {name:{display_name_length + 5}} "
                      f"[lon={s.lon[0]:8.3f}/lat={s.lat[0]:8.3f}]")


class Site(object):
    """
    Class Site() to handle a single Tide Gauge Site
         - label is unique and is created at the creation of a Catalog
         - all list attributes are updatable and remove the duplicates

    :param lat: list of latitude
    :param lon: list on longitude
    :param name: list of names
    :param link: list of web links
    :param provider: list of providers for this site
    :param data: dict with path access to data
    :param id: dict of dataportals ID (GLOSS, PSMSL)
    :param pip: dict of boolean on presence in a catalog
    """

    def __init__(self, lat=0.0, lon=-30., name=None, provider=[],
                 link={}, id={}, data={}, pip={}, hex={}, **kwargs):

        self.lat = lat  # [] of lat
        lon = np.array([lon])  # [] of lon
        lon[lon > 180.] -= 360.  # To have longitudes in -180, 180°
        # self.lon = [np.round(ll, 3) for ll in lon]
        self.lon = lon
        self.name = [n.lower() for n in [name]]  # [] of names
        self.provider = provider  # [] of providers for this site

        self.data = data  # {} of path to access data
        self.id = id  # {} of dataportal ID (GLOSS, PSMSL)
        self.link = link  # {} of web links

        self.pip = pip  # {} of presence in catalog
        self.h3 = {f"hex{res}": h3.latlng_to_cell(lat, lon, res) for res in np.arange(0, 16)}

    def __setattr__(self, attr, value, **kwargs):
        """
        Add and update the attributes values of a Site object

        >>> Site.lon = 56.3
        >>> Site.id = {'gloss':569}
        """
        verbose = kwargs.get('verbose', False)
        try:
            previous_attr_value = getattr(self, attr)
        except AttributeError:
            previous_attr_value = None

        # case for list attribute
        if attr in ['lat', 'lon'] + _site_updatable_attr_list:
            # transform all attributes to [] if not already (and lower case for name)
            if verbose: print(f"site.__setattr__ ==> update liste site.{attr}")
            if attr == 'name':
                new_attr_val = [value.lower()] if isinstance(value, str) else [x.lower() for x in value]
            else:
                new_attr_val = as_a_list(value)
            # if original values exist update and remove the duplicates
            if previous_attr_value:
                [new_attr_val.append(val) for val in previous_attr_value]
                new_attr_val = set(new_attr_val)
            super().__setattr__(attr, [v for v in new_attr_val])

        # case for dict attribute
        elif attr in _site_updatable_attr_dict:  # TODO debug inifinite loop when update dictionary test if value exist
            if previous_attr_value:
                previous_dict = getattr(self, attr)
                try:  # In case the input value doesn't have the good format
                    for key in value.keys():  # loop on keys of the new site
                        if key in previous_dict.keys():  # test if present on previous site
                            if verbose:
                                print(f"site.__setattr__ ==> update dictionary site.{attr} for key={key}")
                                print(f"value[key] = {value[key]}")
                            [previous_dict[key].append(v) for v in as_a_list(value[key]) if v not in previous_dict[key]]
                            uniq_val = set(previous_dict[key])
                            previous_dict[key] = [val for val in uniq_val]
                        else:
                            previous_dict[key] = as_a_list(value[key])
                    new_attr_val = previous_dict
                except AttributeError:
                    raise AttributeError(f"WARNING !! {attr} attribute must be a dictionnary !")
            else:
                for key in value.keys():
                    if isinstance(value[key], (str, float, int)):
                        value[key] = [value[key]]
                new_attr_val = value
            super().__setattr__(attr, new_attr_val)

        else:
            super().__setattr__(attr, value)

    def __str__(self):
        return f"Site ==> names={self.name} / provider={self.id}  / link={self.link} / id={self.id} / data={self.data}\"" \
               f"/pip={self.pip}"

    def __repr__(self):
        return f"Site : {self.name} / {self.id}"

    def __add__(self, other, **kwargs):
        """
        Combine 2 sites into a single one if label is the same

        :param other: Site object
        """
        verbose = kwargs.get('verbose', False)
        if self.same_label_as(other):
            for attr in _site_updatable_attr:
                if verbose: print(f"  ... updating {self.name[0]} and {other.name[0]} for {attr}")
                setattr(self, attr, getattr(other, attr))
            return self
        else:
            raise ValueError('cannot add two sites with different label')

    def isnull_for(self, name):
        """
        Boolean method to test the existance of a given attributes

        :param name: the attribute to test (could be 'name', 'link', 'data', 'country')
        """
        if name == 'name':
            return False if len(self.name) != 0 else True
        if name == 'link':
            return False if len(self.link) != 0 else True
        if name == 'data':
            return False if len(self.data_path) != 0 else True
        if name == 'country':
            return False if len(self.country) != 0 else True
        return None

    def has_id(self, dataportal, **kwargs):
        """
        Boolean method to check if the site is identidied in a dataportal

        :param dataportal: name of the dataportal to check
        :type dataportal: str
        """
        verbose = kwargs.get('verbose', False)
        answer = False
        for portal in self.id:
            if portal.strip().lower() == dataportal.strip().lower() and self.id[portal] is not None:
                if verbose: print(f" Site {self.name} as a identifier in {dataportal} = {self.id[portal]}")
                answer = True
        return answer

    def has_name(self, entry, **kwargs):
        """
        Find by name either short or long name

        :param entry: str of the name to test
        :param contains: if True, test if the name contain the entry (default is False)
        """
        contains = kwargs.get('contains', False)
        verbose = kwargs.get('verbose', False)
        answer = False

        for name in self.name:
            if verbose: print(f" {name} == {entry.strip().lower()}")
            if contains:
                if name.strip().lower().__contains__(entry.strip().lower()):
                    answer = True
            else:
                if name.strip().lower() == entry.strip().lower():
                    answer = True
        return answer

    def same_id_as(self, other, **kwargs):
        """
        Check common data portal in .id attribute

        :param other: Site object
        """
        verbose = kwargs.get('verbose', False)
        common_portal = set(self.id).intersection(other.id)
        same_id = []
        if verbose: print(f"common portal are {common_portal}")
        if len(common_portal) >= 1:
            for portal in common_portal:
                same_id.append(self.id[portal] == other.id[portal])
                return same_id
        else:
            return False

    def same_name_as(self, other, **kwargs):
        """
        Check Site on their names

        :param other: Site object
        """
        common = set(self.name).intersection(other.name)
        return True if len(common) != 0 else False

    def same_label_as(self, other, **kwargs):
        """
        Check Site on their label

        :param other: Site object
        """
        try:
            if self.label:
                return self.label == other.label
        except AttributeError:
            raise AttributeError("WARNING !! Site missing 'label' attribute (need to create a Catalog object first)")

    def __eq__(self, other, **kwargs):
        """
        Check if Site are identical based on several criteria
        """
        raise NotImplementedError
