import pickle
from pathlib import Path
from datetime import datetime
import fnmatch, os
import pandas as pd
from tgcat.core.generic import Site, Catalog, Chrono, _default_null_resolution
from tgcat.core import reader
from tgcat.config import _connectivity_path, _chrono_path, _raw_data_path, _metadata_path
import logging
import requests
import zipfile
import io

logging.basicConfig(format='%(levelname)8s - %(name)3s : %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)
log.setLevel(level=logging.DEBUG)



# define some utility functions
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def create_station_chrono(stname=None, df=pd.DataFrame(data=None)):
    sampling = [1 / 23, 1, 7, 32, 366]
    sample = ['1H', '1D', '1W', '1M', '1Y']
    sta_dict = {key: {} for key in sampling}
    for i in range(len(sampling)):
        sta_dict[sampling[i]] = {'chrono': df.chrono.to_dataframe(stname, resolution=pd.Timedelta(days=sampling[i]))}
        sta_dict[sampling[i]]['chrono']['sample'] = sample[i]
    return sta_dict


def concat_chrono_station(dico):
    df_list = []
    sampling = [1 / 25, 1, 7, 32, 366]
    for sampling in dico:
        df_list.append(dico[sampling]['chrono'])
    return pd.concat(df_list)


def save_chrono(folder, name, df, delete_older=True, **kwargs):  # TODO change delete_older by update
    """Save chrono to pickle"""
    saving_date = datetime.now().strftime("%Y%m%d-%H%M%S")

    if not _chrono_path.exists():
        _chrono_path.mkdir()

    if not (_chrono_path / folder).exists():
        (_chrono_path / folder).mkdir()

    older_file = find(name + '_[0-9]*', (_chrono_path / folder))
    if (len(older_file) != 0.) and (delete_older == True):
        for f in older_file:
            os.remove(f)

    pickle_name = _chrono_path / folder / Path(name + '_' + saving_date).with_suffix('.pickle')
    df.to_pickle(pickle_name)
    return pickle_name


def save_connectivity_table(folder, name, dict, update=True):
    """ save the connectivity table for a catalogue"""
    saving_date = datetime.now().strftime("%Y%m%d-%H%M%S")

    if not (_connectivity_path / folder).exists():
        (_connectivity_path / folder).mkdir()

    older_file = find(name + '_[0-9]*', (_connectivity_path / folder))
    if (len(older_file) != 0.) and (update == True):
        for f in older_file:
            os.remove(f)

    output_name = _connectivity_path / folder / Path(name + '_' + saving_date).with_suffix('.pickle')
    with open(output_name, 'wb') as fp:
        pickle.dump(dict, fp)
    return output_name

def read_zip_gnssir(url):
    r = requests.get(url)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall()
    list_filename=[]
    for zz in z.filelist:
        if zz.filename.split('.')[-1] == 'csv':
            list_filename.append(zz.filename)
    list_df = []
    for filename in list_filename:
        data = pd.read_csv(filename,
                    skiprows=10,
                    sep=',',
                    names=['raw_height', 'adjusted_height', 'fitted_tide', 'prn', 'signal', 'azimuth', 'elevation'],
                    )
        list_df.append(data)
    data = pd.concat(list_df)
    os.remove(filename)
    return data


# TODO finish to implement the download of the connectivity table (Cell2Chrono)
def psmsl_rlr(frequency=['monthly', 'annual']):
    """ read the raw annual and monthly data and compute the connectivity table"""
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('psmsl_rlr*'))[0])  # read the psmsl_rlr catalog
    for sampling in frequency:
        connectivity = {}  # initialize the connectivity table
        log.info(f"initialize connectivity table {len(connectivity)=:}")
        path = _raw_data_path / f"rlr_{sampling}/data"
        for site in cat:
            id = site.id['psmsl_rlr'][0]
            file = path.as_posix() + f"/{id}.rlrdata"
            data = reader.psmsl(file)
            name = f"psmsl_{sampling}_{id}"
            log.info(f"download {sampling} - psmsl[{id}]={site.name} (from local archive) --> {file} ")

            # load the data, build and save the chrono
            if data is not None:
                chrono = data.chrono.to_dataframe(name=name,
                                                  resolution=pd.Timedelta(days=31 if sampling == 'monthly' else 366))
                output_name = save_chrono(f'psmsl_rlr_{sampling}', name, chrono)
            else:
                log.warning(f"{name} has no data")
                chrono = pd.DataFrame(index=[None],
                                      data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                            'name': name, 'sampling': _default_null_resolution})
                output_name = save_chrono(f'psmsl_rlr_{sampling}', name, chrono)
                log.info(f"write chrono table to {output_name}")
            # build the connectivity dictionnary
            for cell_id in list(site.h3.values()):
                if cell_id not in connectivity:
                    connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
                else:
                    connectivity[cell_id]['path'].append(output_name)
                    connectivity[cell_id]['name'].append(site.name[0])
        save_connectivity_table(f'psmsl_rlr_{sampling}', 'cell2chrono', connectivity, update=True)
    return connectivity


def psmsl_metric():
    """ read the raw annual and monthly data compute the connectivity table"""
    connectivity={}
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('psmsl_metric*'))[0])
    path = _raw_data_path / "met_monthly/data"
    for site in cat:
        id = site.id['psmsl_metric'][0]
        file = path.as_posix() + f"/{id}.metdata"
        data = reader.psmsl(file)
        name = f"psmsl_metric_{id}"
        log.info(f"download - psmsl[{id}]={site.name} (from local archive)")

        # load the data, build and save the chrono
        if data is not None:
            chrono = data.chrono.to_dataframe(name=name,
                                              resolution=pd.Timedelta(days=31))
            output_name = save_chrono(f'psmsl_metric', name, chrono)
        else:
            log.warning(f"{name} has no data")
            chrono = pd.DataFrame(index=[None],
                                    data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                        'name': name, 'sampling': _default_null_resolution})
            output_name = save_chrono(f'psmsl_metric', name, chrono)
            log.info(f"write chrono table to {output_name}")
        # build the connectivity dictionnary
        for cell_id in list(site.h3.values()):
            if cell_id not in connectivity:
                connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
            else:
                connectivity[cell_id]['path'].append(output_name)
                connectivity[cell_id]['name'].append(site.name[0])
    save_connectivity_table(f'psmsl_metric', 'cell2chrono', connectivity, update=True)
    return connectivity

def psmsl_gnssir():
    """ read the raw annual and monthly data compute the connectivity table"""
    connectivity = {}
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('psmsl_gnssir*'))[0])
    for site in cat:
        id = site.id['psmsl_gnssir'][0]
        data = pd.read_pickle(list((_raw_data_path).rglob(f'psmsl_gnssir/{id}_*'))[0])
        data = data.drop(columns=['adjusted_height', 'fitted_tide', 'prn', 'signal', 'azimuth', 'elevation'])
        data.index = data.index.astype('datetime64[ns]')
        name = f"psmsl_gnssir_{id}"
        log.info(f"download - psmsl[{id}]={site.name} (from local archive)")

        # load the data, build and save the chrono
        if data is not None:
            chrono = data.chrono.to_dataframe(name=name,
                                              resolution=pd.Timedelta(days=6/24))
            output_name = save_chrono(f'psmsl_gnssir', name, chrono)
        else:
            log.warning(f"{name} has no data")
            chrono = pd.DataFrame(index=[None],
                                  data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                        'name': name, 'sampling': _default_null_resolution})
            output_name = save_chrono(f'psmsl_gnssir', name, chrono)
            log.info(f"write chrono table to {output_name}")
        # build the connectivity dictionnary
        for cell_id in list(site.h3.values()):
            if cell_id not in connectivity:
                connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
            else:
                connectivity[cell_id]['path'].append(output_name)
                connectivity[cell_id]['name'].append(site.name[0])
    save_connectivity_table(f'psmsl_gnssir', 'cell2chrono', connectivity, update=True)
    return connectivity



def uhslc_fd(frequency=['daily', 'hourly']):
    """ read the raw daily and hourly data and compute the connectivity table"""
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('uhslc_fd*'))[0])
    for sampling in frequency:
        connectivity = {}  # initialize the connectivity table
        log.info(f"initialize connectivity table {len(connectivity)=:}")
        path = _raw_data_path / f"uhslc_fd_{sampling}/"
        for site in cat:
            id = site.id['uhslc'][0]
            file = path.as_posix() + f"/{id}.pickle"
            data = pd.read_pickle(list((path).rglob(f'{id}_*'))[0])
            data['Date'] = pd.to_datetime(data['Date'])
            data = data.set_index('Date')
            name = f"uhslc_{sampling}_fd_{id}"
            log.info(f"download {sampling} - uhslc_fd[{id}]={site.name} (from local archive) --> {file} ")

            # load the data, build and save the chrono
            if data is not None:
                chrono = data.chrono.to_dataframe(name=name,
                                                  resolution=pd.Timedelta(days=1 if sampling == 'daily' else 1/23))
                output_name = save_chrono(f'uhslc_fd_{sampling}', name, chrono)
            else:
                log.warning(f"{name} has no data")
                chrono = pd.DataFrame(index=[None],
                                      data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                            'name': name, 'sampling': _default_null_resolution})
                output_name = save_chrono(f'uhslc_rq_{sampling}', name, chrono)
                log.info(f"write chrono table to {output_name}")
            # build the connectivity dictionnary
            for cell_id in list(site.h3.values()):
                if cell_id not in connectivity:
                    connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
                else:
                    connectivity[cell_id]['path'].append(output_name)
                    connectivity[cell_id]['name'].append(site.name[0])
        save_connectivity_table(f'uhslc_fd_{sampling}', 'cell2chrono', connectivity, update=True)
    return connectivity

def uhslc_rq(frequency=['daily', 'hourly']):
    """ read the raw daily and hourly data and compute the connectivity table"""
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('uhslc_rq*'))[0])
    for sampling in frequency:
        connectivity = {}  # initialize the connectivity table
        log.info(f"initialize connectivity table {len(connectivity)=:}")
        path = _raw_data_path / f"uhslc_rq_{sampling}/"
        for site in cat:
            id = site.id['uhslc'][0]
            version = site.id['uhslc_version'][0][-1]
            file = path.as_posix() + f"/{id}.pickle"
            data = pd.read_pickle(list((path).rglob(f'{id}{version}_*'))[0])
            data['Date'] = pd.to_datetime(data['Date'])
            data = data.set_index('Date')
            name = f"uhslc_{sampling}_rq_{id}{version}"
            log.info(f"download {sampling} - uhslc_rq[{id}{version}]={site.name} (from local archive) --> {file} ")

            # load the data, build and save the chrono
            if data.empty is False:
                chrono = data.chrono.to_dataframe(name=name,
                                                  resolution=pd.Timedelta(days=1 if sampling == 'daily' else 1/23))
                output_name = save_chrono(f'uhslc_rq_{sampling}', name, chrono)
            else:
                log.warning(f"{name} has no data")
                chrono = pd.DataFrame(index=[None],
                                      data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                            'name': name, 'sampling': _default_null_resolution})
                output_name = save_chrono(f'uhslc_rq_{sampling}', name, chrono)
                log.info(f"write chrono table to {output_name}")
            # build the connectivity dictionnary
            for cell_id in list(site.h3.values()):
                if cell_id not in connectivity:
                    connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
                else:
                    connectivity[cell_id]['path'].append(output_name)
                    connectivity[cell_id]['name'].append(site.name[0])
        save_connectivity_table(f'uhslc_rq_{sampling}', 'cell2chrono', connectivity, update=True)
    return connectivity

def sonel(frequency=['daily', 'monthly', 'annual']):
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('sonel*'))[0])
    sample = {'daily': 1, 'monthly': 31, 'annual': 366}
    for sampling in frequency:
        connectivity = {}  # initialize the connectivity table
        log.info(f"initialize connectivity table {len(connectivity)=:}")
        path = _raw_data_path / f"sonel_{sampling}/"
        for site in cat:
            id = site.id['sonel'][0]
            file = path.as_posix() + f"/{id}.pickle"
            data = pd.read_pickle(list((path).rglob(f'{id}_*'))[0])
            data['Date'] = pd.to_datetime(data['Date'], format='%Y' if sampling=='annual' else None)
            data = data.set_index('Date')
            name = f"sonel_{sampling}_{id}"
            log.info(f"download {sampling} - sonel[{id}]={site.name} (from local archive) --> {file} ")
            # load the data, build and save the chrono
            if data.empty is False:
                chrono = data.chrono.to_dataframe(name=name,
                                                  resolution=pd.Timedelta(days=sample[sampling]))
                output_name = save_chrono(f'sonel_{sampling}', name, chrono)
            else:
                log.warning(f"{name} has no data")
                chrono = pd.DataFrame(index=[None],
                                      data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                            'name': name, 'sampling': _default_null_resolution})
                output_name = save_chrono(f'sonel_{sampling}', name, chrono)
            log.info(f"write chrono table to {output_name}")
            # build the connectivity dictionnary
            for cell_id in list(site.h3.values()):
                if cell_id not in connectivity:
                    connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
                else:
                    connectivity[cell_id]['path'].append(output_name)
                    connectivity[cell_id]['name'].append(site.name[0])
        save_connectivity_table(f'sonel_{sampling}', 'cell2chrono', connectivity, update=True)
    return connectivity

def gesla():
    connectivity = {}  # initialize the connectivity table
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('gesla*'))[0])
    for site in cat:
        id = f"{site.id['gesla'][0]}"
        name = f"gesla_{site.name[0][:3].upper()}{id}"
        log.info(f"download gesla [{id}]={name} (from local archive) --> {site.data['hourly'][0]} ")
        data = reader.gesla(_raw_data_path / "geslaV3/GESLA3" / site.data['hourly'][0])
        if data.empty is False:
            chrono = data.chrono.to_dataframe(name=name,
                                              resolution=pd.Timedelta(days=1/23))
            # output_name = save_chrono('gesla', name, chrono)
        else:
            log.warning(f"{name} has no data")
            chrono = pd.DataFrame(index=[None],
                                  data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                        'name': name, 'sampling': _default_null_resolution})

        output_name = save_chrono('gesla', name, chrono)

        log.info(f"write chrono table to {output_name}")
        # build the connectivity dictionnary
        for cell_id in list(site.h3.values()):
            if cell_id not in connectivity:
                connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
            else:
                connectivity[cell_id]['path'].append(output_name)
                connectivity[cell_id]['name'].append(site.name[0])
    save_connectivity_table(f'gesla', 'cell2chrono', connectivity, update=True)
    return connectivity

def misela():
    connectivity = {}  # initialize the connectivity table
    log.debug(f"{_metadata_path.exists()=:}")
    cat = pd.read_pickle(list((_metadata_path).rglob('misela*'))[0])
    for site in cat:
        # id = site.id[''][0]
        name = f"misela_{site.name[0]}"
        log.info(f"download misela {name} (from local archive) --> {site.data['minute (nc)'][0]} ")
        data = reader.misela(site.data['minute (nc)'][0])
        if data.empty is False:
            chrono = data.chrono.to_dataframe(name=name,
                                              resolution=pd.Timedelta(days=1 / 25))
            output_name = save_chrono('misela', name, chrono)
        else:
            log.warning(f"{name} has no data")
            chrono = pd.DataFrame(index=[None],
                                  data={'dt': pd.NaT, 'start': pd.NaT, 'end': pd.NaT,
                                        'name': name, 'sampling': _default_null_resolution})
            output_name = save_chrono('misela', name, chrono)

        log.info(f"write chrono table to {output_name}")
        # build the connectivity dictionnary
        for cell_id in list(site.h3.values()):
            if cell_id not in connectivity:
                connectivity[cell_id] = {'path': [output_name], 'name': [site.name[0]]}
            else:
                connectivity[cell_id]['path'].append(output_name)
                connectivity[cell_id]['name'].append(site.name[0])
    save_connectivity_table(f'misela', 'cell2chrono', connectivity, update=True)
    return connectivity




