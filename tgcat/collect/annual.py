import os
from tgcat.config import _raw_data_path
from tgcat.collect import download,scrape, compute_chrono


#PSMSL
print(f"upatde the PSMSL_RLR")
scrape.psmsl_rlr()
download.psmsl_rlr()
compute_chrono.psmsl_rlr()
os.remove(_raw_data_path/'rlr_annual')
os.remove(_raw_data_path/'rlr_monthly')

print(f"upatde the PSMSL_METRIC")
scrape.psmsl_metric()
download.psmsl_metric()
compute_chrono.psmsl_metric()
os.remove(_raw_data_path/'met_monthly')


print(f"upatde the PSMSL_GNSSIR")
scrape.psmsl_gnssir()
download.psmsl_gnssir()
compute_chrono.psmsl_gnssir()
os.remove(_raw_data_path/'psmsl_gnssir')

print(f"Success")

