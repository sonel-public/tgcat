
"""
Main script to scrap and archive data from Catalogs.
"""

from tgcat.collect.scrape import *
from tgcat.config import  _catalog_infos


# --- Archive creation ---
# ------------------------

def create_archives(catalogs=[], all_catalogs=False, exclude_chromedriver=True, **kwargs):
    """
    Catalog archives creation

    :param catalogs: list of catalogs to update (ex : catalogs=['gesla', 'ssc']
    :type catalogs: list of str
    :param all_catalogs: if True, update all catalogs listed in "_catalog_info"
    :type all_catalogs: bool
    :param exclude_chromedriver: if True, does not update the catalogs requiring selenium and ChromeDriver
    :type exclude_chromedriver: bool

    .. Note: This function does not return anything, but update or creat files in the "collect/archives" folder
    """

    all_cat_names = [d['function_name'] for d in _catalog_infos if 'function_name' in d.keys()]
    if exclude_chromedriver:
        for cat_exlcude in ['cantgn', 'psmsl_gnssir']:
            all_cat_names.remove(cat_exlcude)

    # User give a list of catalogs to scrape
    if len(catalogs) != 0 and all_catalogs == False:
        print(f"Update {len(catalogs)} catalogs ...")

        for i, v in enumerate(catalogs):
            if v in all_cat_names:
                cat = globals()[v.split('-')[0]](save=True, rq=v.split('-')[-1],
                                                 update=True, precise_lonlat=True, complete_scrape=True)
            else:
                print(f"This value {v} is not a correct catalog name. \n=> Valid catalogs name : {all_cat_names}")
                continue

    elif all_catalogs:
        print("Update all catalogs ...")

        if len(all_cat_names) != 0:
            for i, v in enumerate(all_cat_names):
                cat = globals()[v.split('-')[0]](save=True, rq=v.split('-')[-1],
                                                 update=True, precise_lonlat=True, complete_scrape=True)

    else:
        print("No updated archives ...")


# Launch in command line
if __name__ == '__main__':

    # Get the command line parameters
    l = sys.argv

    if ('-h' in l) or ('--help' in l):
        print("\nPython function to scrap tide gauge portals and create archives.\n\n"
              ">>> python archive.py [-cat CATALOGS_NAME] [all] [no_chrome] [--help]\n"
              "\n"
              "Options :\n"
              "[-cat CATALOGS_NAME] \t List of the catalogs to scrape (ex: ['ssc','gloss'])\n"
              "[all] \t\t\t Scrape and update all the available catalogs\n"
              "[no_chrome] \t\t Do not scrap catalogs that need ChromeDriver execution\n"
              "[-h][--help] \t\t Display the help menu\n")
    else:
        print("... Scrap and archiving Tide Gauge Catalogs ...")

        if 'all' in l:
            all_catalogs = True
            print("-> Scrap and archive all available Catalogs")
        else:
            all_catalogs = False

        if "no_chrome" in l:
            exclude_chromedriver = True
            print("-> Do not scrap catalogs that need ChromeDriver execution.")
        else:
            exclude_chromedriver = False

        if "-cat" in l:
            i = l.index('-cat')
            cat_str = l[i + 1].strip('][').split(',')
            catalogs = [c.replace('"', "").replace("'", '') for c in cat_str]
            print(catalogs)
            print(f"-> Scrap and archive the given Catalogs : {catalogs}")
        else:
            catalogs = []

        create_archives(catalogs=catalogs, all_catalogs=all_catalogs, exclude_chromedriver=exclude_chromedriver)
