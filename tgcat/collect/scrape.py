"""
A collection of function to get tide gauge data
from data portals and transform them into Catalogs.
"""

# --- General imports ---
# -----------------------
import pickle
import sys
import os
import fnmatch
from ftplib import FTP
from pathlib import Path

import numpy as np
import pandas as pd
import geopandas as gpd
import urllib3
import urllib
import netCDF4                                                                # netCDF4 version 1.6.3

from bs4 import BeautifulSoup
import requests
from datetime import timedelta

import time
from datetime import datetime
import logging
# from selenium import webdriver
# from selenium.webdriver.common.by import By

from tgcat.core.generic import Site, Catalog
from tgcat.config import  _resources_path, _archives_path, _metadata_path,  _app_config, _raw_data_path
from tgcat.info import _portal_color
from iso3166 import countries_by_alpha2, countries_by_alpha3, countries

logging.basicConfig(
    format='%(asctime)s - %(levelname)8s - %(name)3s - L%(lineno)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    level=logging.INFO)
log = logging.getLogger(__name__)


# --- General functions ---
# -------------------------

# TODO remove all color from portals
def fake_catalog():  # TODO move outside scrape
    """
    Create a fake Catalog for testing purposes.

    :return: Catalog
    """
    sites = []
    sites.append(Site(lat=45.914, lon=-1.328, name='lacote', id={'gloss': 179}, pip={'slsmf': True}))
    sites.append(Site(lat=45.914, lon=-1.328, name='cotiniere', id={'gloss': 189}, pip={'bodc': False}))
    sites.append(Site(lat=45.914, lon=-1.3282, name='la cotiniere', pip={'refmar': True}))
    sites.append(Site(lat=45.914, lon=-1.3282, name=['cotiniere', 'coti2'], pip={'refmar': True}))
    return Catalog(name='fake', sites=sites, color='black')


def get_source_from_url(url, **kwargs):
    """
    Retrieve the html code from the url

    :param url: str with URL
    :return: HTPP content of a web page
    """
    verbose = kwargs.get('verbose', True)
    if verbose: log.info(f" ... scraping the url : {url}")
    http = urllib3.PoolManager()
    response = http.request('GET', url).data.decode('utf-8')
    return response


def get_function_name():
    """
    Return the name of the function from where this function is run

    :rtype: str
    """
    return sys._getframe(1).f_code.co_name


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def save_to_pickle(catalog, delete_older=True, **kwargs):
    """
    Save raw scraped catalog to pickle

    :param catalog: Catalog object to save into pickle file
    """
    now = datetime.now()
    saving_date = now.strftime("%Y%m%d-%H%M%S")

    if not _resources_path.exists():
        _resources_path.mkdir()
    if not _archives_path.exists():
        _archives_path.mkdir()
    if not _metadata_path.exists():
        _metadata_path.mkdir()

    older_file = find(catalog.name + '_[0-9]*', _metadata_path)
    if (len(older_file) != 0.) and (delete_older == True):
        for f in older_file:
            os.remove(f)

    pickle_name = _metadata_path / Path(catalog.name + '_' + saving_date).with_suffix('.pickle')
    log.info(f" Catalog saved in  {pickle_name}")
    with open(pickle_name, 'wb') as file:
        pickle.dump(catalog, file)


def catalog_from_pickle(pickle_path, **kwargs):
    """
    Read a catalog pickle file .

    :return: catalog
    """
    cat = pd.read_pickle(pickle_path)
    return Catalog(name=cat.name, sites=cat.sites)


def reformat_column_to_list(row, numeric=False):
    """
    Force some dataframe columns to be numeric
    :param row:
    :param numeric:
    :return:
    """

    if isinstance(row, float):
        if np.isnan(row):
            return []
        else:
            return [int(row)]
    if isinstance(row, str):
        if numeric:
            return [int(row)]
        else:
            return [row]
    if isinstance(row, list):
        if numeric:
            return [int(x) for x in row]
        else:
            return row


def is_url(text):
    return urllib.parse.urlparse(text).scheme in ('http', 'https',)


# --- Scraping functions ---
# -------------------------

def gloss(save=True, **kwargs):
    """
    Scrape the GLOSS Station Handbook

    :param save: if True, save the Catalog to pickle file (default is False)

    :return: Catalog object

    .. Note: The URL source is : https://gloss-sealevel.org/gloss-station-handbook
    """
    cat_name = get_function_name()

    url = r'https://gloss-sealevel.org/gloss-station-handbook'
    log.info(f" ... scraping the GLOSS catalog : {url}")

    df = pd.read_html(url)[0]

    sites = []
    for i, row in df.iterrows():  # iteration over all the rows
        s = Site(lat=row['Latitude (+ve N)'], lon=row['Longitude (+ve E)'],
                 name=row['Station'].lower(), country=row['Country'],
                 id={cat_name: [row['GLOSS number']]},
                 pip={cat_name: True})
        sites.append(s)
    catalog = Catalog(name=cat_name, sites=sites)
    if save: save_to_pickle(catalog)
    return catalog


def ssc(save=True, **kwargs): #TODO remove columns columns_with_numeric_id a optimizer
    """
    Scrape the IOC SSC catalog.

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : http://www.ioc-sealevelmonitoring.org/ssc/service.php?format=json
    """
    # create a the catalog name from the name of the scrapping function
    cat_name = get_function_name()

    url = r'http://www.ioc-sealevelmonitoring.org/ssc/service.php?format=json'
    station_link = r'http://www.ioc-sealevelmonitoring.org/ssc/stationdetails.php?id='
    log.info(f" ... scraping the IOC SSC catalog : {url}")

    df = pd.read_json(url)
    columns_with_numeric_id = ['gloss', 'psmsl', 'sonel_gps', 'sonel_tg', 'uhslc']
    columns_with_string_id = ['ioc', 'ptwc']
    columns_portal = columns_with_numeric_id + columns_with_string_id
    for c in columns_with_numeric_id:
        df[c] = df[c].apply(reformat_column_to_list, numeric=True)
    for c in columns_with_string_id:
        df[c] = df[c].apply(reformat_column_to_list, numeric=False)
    columns_ordered = ['country', 'ssc_id', 'geo:lon', 'geo:lat', 'name'] + columns_portal
    df = df[columns_ordered]
    sites = []
    for i, row in df.iterrows():  # iteration over all the rows
        id = {}
        for portal in columns_portal:
            if len(row[portal]) >= 1:
                id.update({portal: row[portal]})
        id.update({cat_name: [row.ssc_id.lower()]})
        s = Site(lat=row['geo:lat'], lon = row['geo:lon'],
        name = [row['name'].lower()],
        id = id, pip = {cat_name: True},
        link = {cat_name: [station_link + row.ssc_id]})
        sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
    return catalog


def psmsl_rlr(save=True):
    """ read the RLR catalog from geojson URL provided by Andy"""
    cat_name = get_function_name()
    root_url = r'https://psmsl.org/data/obtaining/map/data/rlrMarkerList.json'
    r = requests.get(root_url)
    data = r.json()['features']
    log.info(f" ... scraping the {cat_name.upper()} Data Portal : {root_url}")
    sites = []
    for i, row in enumerate(data):  # iteration over all the rows
        print(f"{i}/{len(data)}", end='\r')
        id = {}
        id.update({cat_name: row['properties']['stations'][0]['id']})
        s = Site(lon=row['geometry']['coordinates'][0],
                 lat=row['geometry']['coordinates'][1],
                 name=row['properties']['stations'][0]['name'],
                 id=id,
                 pip={cat_name: True})
        sites.append(s)
    log.info(_portal_color[cat_name])
    catalog = Catalog(name=cat_name, sites=sites)
    if save: save_to_pickle(catalog)
    return catalog


def psmsl_metric(save=True):
    """ read the RLR catalog from geojson ULR"""
    cat_name = get_function_name()
    root_url = r'https://psmsl.org/data/obtaining/map/data/metricMarkerList.json'
    r = requests.get(root_url)
    data = r.json()['features']
    log.info(f" ... scraping the {cat_name.upper()} Data Portal : {root_url}")
    sites = []
    for i, row in enumerate(data):  # iteration over all the rows
        print(f"{i}/{len(data)}", end='\r')
        id = {}
        id.update({cat_name: row['properties']['stations'][0]['id']})
        s = Site(lon=row['geometry']['coordinates'][0],
                 lat=row['geometry']['coordinates'][1],
                 name=row['properties']['stations'][0]['name'],
                 id=id,
                 pip={cat_name: True})
        sites.append(s)
    catalog = Catalog(name=cat_name, sites=sites)
    if save: save_to_pickle(catalog)
    return catalog


def psmsl_links(dataframe=False, save=True, **kwargs):
    """
    Scrape RLR stations which can be linked to a geocentric ellipsoid using data from GNSS receivers

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : https://www.psmsl.org/data/obtaining/ellipsoidal_links.json
    .. todo: test on Mederic .xml file ==> https://www.sonel.org/PSMSL/RLR_ell_heights.xml (but no geometry inside)
    """

    cat_name = get_function_name().replace('', '')
    root_url = 'https://www.psmsl.org/data/obtaining/'
    url_file = root_url + 'ellipsoidal_links.json'
    json = pd.read_json(url_file)
    df = pd.json_normalize(json['features'])

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the PSMSL Ellispoidal Links Portal {url_file}")
        for i, row in df.iterrows():  # iteration over all the rows
            id = {'sonel_gps': row['properties.gnssId']}
            id.update({cat_name: int(row['id'])})
            s = Site(lat=row['geometry.coordinates'][1], lon=row['geometry.coordinates'][0],
                     pip={cat_name: True},
                     name=row['properties.name'], id=id,
                     link={cat_name: [root_url + r'/stations/' + str(row['id']) + '.php']})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites, color=_portal_color[cat_name])
        if save: save_to_pickle(catalog)
        return catalog


def psmsl_gnssir(dataframe=False, save=True, **kwargs):
    """
    Scrape GNSS-IR stations

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : https://psmsl.org/data/gnssir/table.php
    """

    cat_name = get_function_name().replace('', '')
    root_url = 'https://psmsl.org/data/gnssir/data/maplayers/good_sites.json'

    json = pd.read_json(root_url)
    df = pd.json_normalize(json['features'])

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the {cat_name.upper()} Portal : {root_url}")
        for i, row in df.iterrows():  # iteration over all the rowNei4re=v7a
            s = Site(lat=row['geometry.coordinates'][1],
                     lon=row['geometry.coordinates'][0],
                     name=row['properties.name'],
                     id={cat_name: [row['id'], row['properties.code']]},
                     pip={cat_name: True},
                     link={cat_name: [r'https://psmsl.org/data/gnssir/site.php?id=' + str(row['id'])]},
                     data={'daily_means (zipped file)': [f'https://psmsl.org/data/gnssir/data/main/{row["id"]}.zip']})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog

def uhslc(rq=None, dataframe=False, save=True, **kwargs):
    """
    Scrape the UHSLC for Fast Delivery (fd) and Research Quality (rq)

    :param rq: if True, use the research quality data (rq), else the fast delivery data (fd)
    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : http://uhslc.soest.hawaii.edu/data/
    """
    suffix = 'rq' if rq == 'rq' else 'fd'
    cat_name = get_function_name() + '_' + suffix
    sites = []
    suffix = 'rq' if rq == 'rq' else 'fd'
    log.info(f" ... scraping the {cat_name}")
    if rq == 'rq':
        df = pd.read_html("https://uhslc.soest.hawaii.edu/data/rq.html?_=1692195471894", extract_links='body')[0]
        for i, row in df.iterrows():  # iteration over all rows of the catalog
            id = {}  # init the ID dictionary
            uhslc_id = row['UH#'][0]
            version = row.Version[0]
            region = row['CSV'][1].split('/rqds/')[-1].split('/')[0]
            data_url_prefix = "https://uhslc.soest.hawaii.edu/"
            name = str(row.Location[0])
            data = {'daily': [f'{data_url_prefix}/data/csv/rqds/{region}/daily/d{uhslc_id}{version}.csv'],
                    'hourly': [f'{data_url_prefix}/data/csv/rqds/{region}/hourly/h{uhslc_id}{version}.csv']}
            id.update({'uhslc': int(uhslc_id), 'uhslc_version': uhslc_id + version})
            if row['GLOSS#'][0] != '': id.update({'gloss':int(row['GLOSS#'][0])})
            s = Site(lat=float(row.Latitude[0]), lon=float(row.Longitude[0]), name=name,
                     id=id, pip={cat_name: True},data=data)
            sites.append(s)
    else:
        df = pd.read_html("https://uhslc.soest.hawaii.edu/data/fd.html?_=1692196131166", extract_links='body')[0]
        for i, row in df.iterrows():  # iteration over all rows of the catalog
            print(row.Latitude)
            id = {}  # init the ID dictionary
            uhslc_id = row['UH#'][0]
            data_url_prefix = "https://uhslc.soest.hawaii.edu/"
            name = str(row.Location[0])
            print(name)
            # stn = uhslc_id + row.Version
            data = {'daily': [f'{data_url_prefix}/data/csv/fast/daily/d{uhslc_id}.csv'],
                    'hourly': [f'{data_url_prefix}/data/csv/fast/hourly/h{uhslc_id}.csv']}

            id.update({'uhslc': int(uhslc_id)})
            if row['GLOSS#'][0] != '': id.update({'gloss':int(row['GLOSS#'][0])})
            s = Site(lat=float(row.Latitude[0]), lon=float(row.Longitude[0]), name=name,
                     id=id, pip={cat_name: True},data=data)
            sites.append(s)

    catalog = Catalog(name=cat_name, sites=sites)
    if save:
        save_to_pickle(catalog)
    if dataframe:
        return df
    return catalog


def slsmf(dataframe=False, save=True, **kwargs):
    """
    Scrape the SLSMF (old VLIZ) dataportal

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : http://www.ioc-sealevelmonitoring.org/list.php?operator=&showall=all&output=contacts#
    """
    cat_name = get_function_name()
    url = r'http://www.ioc-sealevelmonitoring.org/list.php?operator=&showall=all&output=contacts#'
    html_source = get_source_from_url(url, verbose=False)

    soup = BeautifulSoup(html_source, features='html.parser')
    i = 0
    res = []
    col_name = ['CODE', 'GLOSS ID', 'Lat', 'Lon', 'Country', 'Location',
                'Connection', 'Contact', 'View']

    table = soup.find('table')
    table_rows = table.find_all('tr')
    for tr in table_rows[12:-1]:  # for all rows
        td = tr.find_all('td')  # all line of the table
        liste = [t.text for t in td]
        liste[1] = [int(x) for x in td[1].text.split(",") if x != '']
        liste[0] = [td[0].text]
        res.append(liste)
    df = pd.DataFrame(data=res, columns=col_name)
    log.info(f" ... scraping the {cat_name} portal : {url}")

    if dataframe:
        return df
    else:
        sites = []
        for i, row in df.iterrows():  # iteration over all the rows
            url = r'http://www.ioc-sealevelmonitoring.org/station.php?code=' + row['CODE'][0]
            #            lon, lat = scrape_vliz_lon_lat_active_station(url)
            # log.info(f"Station {row['CODE ID']} ==> {lon, lat}")
            id = {}
            if len(row['GLOSS ID']) >= 1: id.update({'gloss': row['GLOSS ID']})
            id.update({cat_name: row['CODE']})
            s = Site(lat=float(row.Lat), lon=float(row.Lon), name=row.Location.strip(),
                     id=id, pip={cat_name: True}, link={cat_name: [url]},
                     data={'raw data (1 month)': [f"https://www.ioc-sealevelmonitoring.org/bgraph.php?code=" + str(
                         row['CODE'][0]) + "&output=tab&period=30"]},
                     provider=row.Contact)
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def bodc(dataframe=False, save=True, **kwargs):
    """
    Scrape the BODC portal

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : https://www.bodc.ac.uk/data/hosted_data_systems/sea_level/international/zones/#inventory
    """

    _zones = ['north_atlantic', 'south_atlantic', 'indian_ocean',
              'north_pacific', 'south_pacific']
    verbose = kwargs.get('verbose', False)
    cat_name = get_function_name()
    url = 'https://www.bodc.ac.uk/data/hosted_data_systems/sea_level/international/zones/#inventory'

    list_data = []
    for z in _zones:
        ds = pd.read_html(f"https://www.bodc.ac.uk/data/hosted_data_systems/sea_level/international/{z}/#inventory")[0]
        ds['region'] = z
        list_data.append(ds)
    df = pd.concat(list_data)
    latitude = []
    longitude = []
    for index, row in df.iterrows():
        a = row['Position'].split(' ')
        if len(a) == 4:
            lat_mul = -1. if a[1].__contains__('S') else 1.
            lon_mul = -1. if a[3].__contains__('W') else 1.
            lat = lat_mul * (float(a[0]) + float(a[1][:-1]) / 60.)
            lon = lon_mul * (float(a[2]) + float(a[3][:-1]) / 60.)
            latitude.append(lat)
            longitude.append(lon)
        if len(a) == 5:
            lat_mul = -1. if a[1].__contains__('S') else 1.
            lon_mul = -1. if a[4].__contains__('W') else 1.
            lat = lat_mul * (float(a[0]) + float(a[1][:-1]) / 60.)
            lon = lon_mul * (float(a[3]) + float(a[4][:-1]) / 60.)
            latitude.append(lat)
            longitude.append(lon)
        if len(a) == 6:
            lat_mul = -1. if a[1].__contains__('S') else 1.
            lon_mul = -1. if a[5].__contains__('W') else 1.
            lat = lat_mul * (float(a[0]) + float(a[1][:-1]) / 60.)
            lon = lon_mul * (float(a[3]) + float(a[4][:-1]) / 60.)
            latitude.append(lat)
            longitude.append(lon)
        if len(a) > 6:
            latitude.append(np.nan)
            longitude.append(np.nan)
    df['lon'] = longitude
    df['lat'] = latitude

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the {cat_name} Data Portal")
        for i, row in df.iterrows():  # iteration over all the rows
            id = {}
            if row['GLOSS no.'] < 9000.: id.update({'gloss': int(row['GLOSS no.'])})
            id.update({cat_name: row['Site name']})
            s = Site(lat=float(row.lat), lon=float(row.lon),
                     name=row['Site name'], id=id,
                     link={cat_name: 'https://www.bodc.ac.uk/data/hosted_data_systems/sea_level/international/'},
                     data={'BODC (zipped)': [f"https://www.bodc.ac.uk//data/hosted_data_systems/sea_level/international/"
                                             + f"{row['region']}/ascii/g{row['GLOSS no.']}"
                                             + f"{'' if len(row['Site name'].split('-'))==1 else row['Site name'].split('-')[-1][-1].lower()}.zip"]},
                     pip={cat_name: True})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def cmems(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the Copernicus CMEMS

    :param update: if True, access CMEMS ftp to download site list (default is False)
    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source was : nrt.cmems-du.eu
    .. todo : Need to update data recovery method
    """
    cat_name = get_function_name().replace('_stations', '')
    header = ['catalog_id', 'file_name', 'geospatial_lat_min', 'geospatial_lat_max',
              'geospatial_lon_min', 'geospatial_lon_max', 'time_coverage_start', 'time_coverage_end',
              'provider', 'date_update', 'data_mode', 'parameters']
    cmems_file = _resources_path / 'cmems_station_list.txt'

    # Todo : update data recovery
    if update:
        ftp_site = r'nrt.cmems-du.eu'
        ftp_file = r'/Core/INSITU_GLO_PHY_SSH_DISCRETE_MY_013_053/cmems_obs-ins_glo_phy-ssh_my_na_irr/index_history.txt'
        ftp = FTP(ftp_site)
        ftp.login('ltestut', '@Copernicus2017')
        log.info(f" ... Logging successfully ! Download the CMEMS station list ...")
        ftp.retrbinary("RETR " + ftp_file, open(_resources_path / 'cmems_station_list.txt', 'wb').write)
        return cmems(update=False, dataframe=dataframe, save=save)

    else:
        df = pd.read_csv(cmems_file, sep=',', comment='#', names=header)
        df['instrument'] = df['file_name'].str.split('/').str[-2]
        df = df[df['instrument'] == "TG"]
        if dataframe:
            return df
        else:
            sites = []
            log.info(f" ... scraping the CMEMS Data Portal from {cmems_file}")
            for i, row in df.iterrows():
                if row.parameters.__contains__('SLEV'):
                    name = row.file_name.split('/')[-1][:-3].split('_')[-1]
                    if "MINUTE" in name.upper():
                        name = '_'.join(row.file_name.split('/')[-1][:-3].split('_')[-2:])
                    s = Site(lat=row.geospatial_lat_min, lon=row.geospatial_lon_min,
                             name=name, id={cat_name: row.catalog_id},
                             provider=row.provider, pip={cat_name: True},
                             data={'Raw data (netcdf)': [row.file_name]})
                    sites.append(s)
            catalog = Catalog(name=cat_name, sites=sites)
            if save: save_to_pickle(catalog)
            return catalog


def eutgn(dataframe=False, save=True, **kwargs):
    """
    Scrape the European Tide Gauge Network Inventory

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : http://eutgn.marine.ie/geonetwork/srv/fre/catalog.search#/search
    .. Warning: This one is provided by a .txt file and cannot be scraped from url
    .. todo : Need to update the .txt file
    """
    cat_name = get_function_name()
    file = _resources_path / "eutgn_20190925_.csv"
    df_in = pd.read_csv(file, header=2).rename(columns={'Location WGS84 lat.': 'lat', \
                                                        'Location WGS84 lon.': 'lon'})
    df = df_in.iloc[:, [1, 3, 25, 26, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 70]].dropna(subset=['lat'])
    data_portal = [txt for txt in df.columns.values if 'Data' in txt]
    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the European Tide Gauge Network Inventory from {file}")
        for i, row in df.iterrows():
            stn = row['Station Name'] if isinstance(row['Station Name'], str) else 'unknown'
            data_provider = row['Host Organisation'] if isinstance(row['Host Organisation'], str) else 'unknown'
            link = {}
            for l in row[data_portal].values:
                if isinstance(l, str):
                    if is_url(l):
                        link[str(Path(l).parts[1])] = [l]
            # link = {str(Path(l).parts[1]):[l] for l in row[data_portal].values if isinstance(l, str)}
            s = Site(lat=row['lat'], lon=row['lon'],
                     name=stn.lower(), id={cat_name: stn},
                     link=link, pip={cat_name: True},
                     provider={cat_name: data_provider})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def redmar(dataframe=False, save=True, **kwargs):
    """
    Scrape the SPANISH REDMAR network from Bego file

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is :
    .. Warning: This one is provided by a .txt file and cannot be scraped from url
    .. todo : Need to update the .txt file
    """
    cat_name = get_function_name()
    file = _resources_path / "REDMAR_Coordinates.dat"
    df = pd.read_csv(file, delim_whitespace=True).rename(columns={'LATITUDE': 'lat', 'LONGITUDE': 'lon'})
    data_portal = [txt for txt in df.columns.values if 'Data' in txt]
    data_provider = 'Puertos del Estado'
    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the REDMAR from {file}")
        for i, row in df.iterrows():
            stn = row['STATION'] if isinstance(row['STATION'], str) else 'unknown'
            link = {}
            s = Site(lat=row['lat'], lon=row['lon'],
                     name=stn.lower(), id={cat_name: stn},
                     link=link, pip={cat_name: True},
                     provider={cat_name: data_provider})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def emodnet(dataframe=False, save=True, **kwargs):
    """
    Scrape the European Marine Observation and Data Network

    :param dataframe:
    :param save:
    :param kwargs:
    :return:

    .. Note: Data soruce comes from WMS flux :
    https://geoserver.emodnet-physics.eu/geoserver/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=emodnet:EP_GEO_INT_ALLP_TG_PP_GLO&outputFormat=application/json
    """
    cat_name = get_function_name().replace('', '')

    json_url = r"https://geoserver.emodnet-physics.eu/geoserver/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=emodnet:EP_GEO_INT_ALLP_TG_PP_GLO&outputFormat=application/json"
    json = pd.read_json(json_url, orient='index', typ='series')
    df = pd.json_normalize(json['features'])

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the EMODNET Portal")
        for i, row in df.iterrows():  # iteration over all the rows
            id = {cat_name: int(row['properties.PlatformID'])}
            s = Site(lat=float(row['geometry.coordinates'][1]), lon=float(row['geometry.coordinates'][0]),
                     pip={cat_name: True}, data_provider=row['properties.DataProvider'],
                     country=row['properties.Country'],
                     name=row['properties.PlatformCode'], id=id,
                     link={cat_name: [row['properties.PlatformInfoLink']]},
                     data={'hourly (csv)': [
                         f'https://erddap.emodnet-physics.eu/erddap/tabledap/EP_ERD_INT_SLEV_AL_TS_NRT.csv?&EP_PLATFORM_ID=%22' + str(
                             row["properties.PlatformID"]) + '%22']})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog

def misela(dataframe=False, save=True, **kwargs):
    cat_name = get_function_name().replace('', '')

    ds=[]
    list_files = list((_raw_data_path / "misela").glob('*.nc'))
    for file in list_files:
        file2read = netCDF4.Dataset(file,'r')
        lon = file2read.longitude.split('degree')
        lat = file2read.latitude.split('degree')
        lon = float(lon[0])*-1 if lon[-1]==' W' else float(lon[0])
        lat = float(lat[0])*-1 if lat[-1]==' S' else float(lat[0])
        station_name = file2read.station_code
        infos = {
            'Longitude': lon,
            'Latitude': lat,
            'Station Name': station_name,
            'Original Data': file2read.original_data
        }
        ds.append(infos)
    df = pd.DataFrame(ds)
    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... Collecting MISELA informations")
        for i, row in df.iterrows():  # iteration over all the rows
            id = {cat_name: row['Station Name']}
            s = Site(lat=row['Latitude'], lon=row['Longitude'],
                     name=row['Station Name'],
                     id=id, pip={cat_name: True},
                     link={cat_name: [row['Original Data']]},
                     data={'minute (nc)': [_raw_data_path / f"misela/{row['Station Name']}.nc"]})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog

# def ilico(dataframe=False, save=False, **kwargs):
#     """
#     Scrape the Ilico inventory
#
#     :param dataframe: if True, return a dataframe (default is False)
#     :param save: if True, save the Catalog to pickle file (default is False)
#
#     :return: pd.dataframe or Catalog object
#
#     .. todo: need update the method to get the data ! invalid URL !
#     """
#     cat_name = get_function_name().replace('', '')
#     url = r'https://www.sonel.org/squelettes/outils/?a=ilico&web'
#     df = pd.read_csv(url, sep=';')
#     if dataframe:
#         return df
#     else:
#         sites = []
#         log.info(f" ... scraping the SONEL Data Portal from {url} ")
#         for i, row in df.iterrows():
#             s = Site(lat=row.latitude, lon=row.longitude, name=row['site'].lower(),pip={cat_name: True},
#                      data_provider=row['organisme_responsable'], country=row['pays'])
#             sites.append(s)
#         catalog = Catalog(name=cat_name, sites=sites, color=_portal_color[cat_name])
#         if save: save_to_pickle(catalog)
#         return catalog


def refmar(dataframe=False, save=True, **kwargs):
    """
    Scrape the RAFEMAR/DataShom inventory

   :param dataframe: if True, return a dataframe (default is False)
   :param save: if True, save the Catalog to pickle file (default is False)

   :return: pd.dataframe or Catalog object

   .. Note: This URL source is : https://services.data.shom.fr/maregraphie/service/tidegauges
   """
    cat_name = get_function_name().replace('', '')
    url = r"https://services.data.shom.fr/maregraphie/service/tidegauges"
    df = pd.read_json(url, orient='records')

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the REFMAR Data Portal from {url} ")
        for i, row in df.iterrows():
            idte = row['shom_id']
            url = fr"https://services.data.shom.fr/maregraphie/service/completetidegauge/{idte}"
            df = pd.read_json(url, orient='index').T
            #     try:
            #         start = pd.to_datetime(df.date_prem_obs[0] + 'T00:00:00.0Z')
            #         end = start + timedelta(days=32)
            #     except:
            #         start = pd.to_datetime('1990-01-01T00:00:00.0Z')
            #         end = pd.Timestamp.now()

            s = Site(lat=row.latitude,
                     lon=row.longitude,
                     name=row['name'],
                     id={cat_name: idte}, pip={cat_name: True},
                     link={cat_name: [fr"https://data.shom.fr/donnees/refmar/{idte}"]},
                     provider=[[k['nom'] for k in df.organismes[0]]])
            # data_link = f'https://services.data.shom.fr/maregraphie/observation/json/{idte}?sources=4&dtStart={start.to_datetime64()}Z&dtEnd={end.to_datetime64()}Z&interval=1'
            # if not pd.read_json(data_link, orient='split').empty:
            #     s.data = {'raw_hourly': [data_link]}
            s.collocalisation = [df.collocalisation[0]]
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def sonel(dataframe=False, save=True, complete_scrape=False, **kwargs):
    """
    Scrape the SONEL Tide Gauge inventory

   :param dataframe: if True, return a dataframe (default is False)
   :param save: if True, save the Catalog to pickle file (default is False)
   :param complete_scrape: if True, load metadata and data from src page
            !!! Warning : this option is time consuming

   :return: pd.dataframe or Catalog object

   .. Note: This URL source is API SONEL: https://api.sonel.org//v1/doc/index.html#/operations/getSlvMetaTidegauge
   """
    cat_name = get_function_name().replace('', '')
    url = "http://api.sonel.org/v1/products/slv/msl/meta/tidegauges"
    response = requests.get(url)
    df = pd.DataFrame(response.json())
    df.lon = df.lon.astype(float)
    df.lat = df.lat.astype(float)

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the SONEL Inventory from {url} ")
        for k, (i, row) in enumerate(df.iterrows()):
            log.info(f'... Load site {k}/{len(df)}')
            # Get metadata and data info
            zurl = fr"https://www.sonel.org/?page=maregraphe&idStation={row['id']}"
            links = {cat_name: [fr"https://www.sonel.org/?page=maregraphe&idStation={row['id']}"]}
            datas = {'daily_means (download)': [
                f'https://www.sonel.org/msl/Doodson/VALIDATED/d{row["acro"].upper()}.slv'],
                'monthly_means (download)': [
                    f'https://www.sonel.org/msl/Doodson/VALIDATED/m{row["acro"].upper()}.slv'],
                'annual_means (download)': [
                    f'https://www.sonel.org/msl/Doodson/VALIDATED/y{row["acro"].upper()}.slv']}
            s = Site(
                lat=row['lat'],
                lon=row['lon'],
                name=row['acro'],
                id={cat_name: row['id']},
                pip={cat_name: True},
                link=links,
                data=datas,
                provider=["https://www.sonel.org/"],
            )
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def ptwc(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the PTWC

    :param update: if True, update the txt file with station from URL (default is False)
    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    ..Note : Scrape data frome ptwc_station_list.txt file in Resources folder (from ftp.soest.hawaii.edu)
    """
    if update:
        ftp_site = r'ftp.soest.hawaii.edu'
        ftp_file = r'/ptwc/Tide_Toolv10/NEW/COMP_META'
        ftp = FTP(ftp_site)
        ftp.login('anonymous', 'anonymous')
        ftp.retrbinary("RETR " + ftp_file, open(_resources_path / 'ptwc_station_list.txt', 'wb').write)
        log.info(f" ... download the PTWC station list")
        df = pd.read_csv(_resources_path / 'ptwc_station_list.txt', delim_whitespace=True, comment='#', header=None,
                         names=['code', 'name', 'id', 'wmo', 'gauge', 'sample_rate',
                                'num_sample', 'unit', '8', '9', 'scale', '11', '12',
                                'latitude', 'longitude', '15', 'owner', 'gloss_id', '18'])
    else:
        try:
            df = pd.read_csv(_resources_path / 'ptwc_station_list.txt', delim_whitespace=True, comment='#', header=None,
                             names=['code', 'name', 'id', 'wmo', 'gauge', 'sample_rate',
                                    'num_sample', 'unit', '8', '9', 'scale', '11', '12',
                                    'latitude', 'longitude', '15', 'owner', 'gloss_id', '18'])
        except:
            raise FileExistsError("..To download the ptwc datalist, you must run scrape.ptwc(update=True) first.")

    if dataframe:
        return df

    else:
        cat_name = get_function_name().replace('', '')
        sites = []
        log.info(f" ... scraping the PTWC Data Portal from {_resources_path / 'ptwc_station_list.txt'} ")
        for i, row in df.iterrows():
            s = Site(lat=row.latitude, lon=row.longitude, name=' '.join(row['name'].split('_')[:-1]),
                     provider=[row['owner']], pip={cat_name: True},
                     id={cat_name: row['code']})
            try:
                if int(row['gloss_id']): s.id['gloss'] = [int(row['gloss_id'])]
            except:
                pass

            try:
                if countries_by_alpha2.get(row['name'].split('_')[-1]).name:
                    s.country = countries_by_alpha2.get(row['name'].split('_')[-1]).name
            except:
                s.country = 'Unknown'

            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def gesla(dataframe=False, save=True, **kwargs):
    """
    Scrape GESLA dataset

    :param update:
    :param dataframe:
    :param save:

    ..Warning : Need to download the .CSV file here : https://www.icloud.com/iclouddrive/0EDpZSvHxx68twkXUaHrDOS0g#GESLA3_ALL
    """
    cat_name = get_function_name().replace('', '')
    gesla_file = _raw_data_path / 'geslaV3/GESLA3_ALL.csv'

    try:
        df = pd.read_csv(gesla_file)
    except:
        meta_link = r"https://www.icloud.com/iclouddrive/0EDpZSvHxx68twkXUaHrDOS0g#GESLA3_ALL"
        raise FileNotFoundError(f" No data file => you must download metadata from {meta_link} into "
                                f"the 'resources' repository.")

    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the GESLA  Data Portal from {gesla_file} ")
        for i, row in df.iterrows():
            s = Site(lat=row.LATITUDE, lon=row.LONGITUDE, name=row['SITE NAME'], pip={cat_name: True},
                     provider=[row['CONTRIBUTOR (ABBREVIATED)'], row['ORGINATOR']],
                     id={cat_name: row['SITE CODE']})
            if row['CONTRIBUTOR WEBSITE'] != 'Unspecified':
                s.link = {row['CONTRIBUTOR (ABBREVIATED)']: [row['CONTRIBUTOR WEBSITE']]}
            s.data = {'hourly': [row['FILE NAME']]}
            s.country = countries_by_alpha3.get(row.COUNTRY).name
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def cantgn(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the Canadian portal with Sea Level permanent stations

    :param update:
    :param dataframe:
    :param save:

    .. Note : Scrape data from online portal at : https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/maps-cartes/inventory-inventaire-eng.asp?user=isdm-gdsi&region=MEDS&tst=1&perm=1
    .. Warning : This method need the chromedriver.exe in resources
    """

    # Read the online portal

    log.info(f" .. Load Google ChromeWebpage")
    options = webdriver.ChromeOptions()
    options.add_argument("start-maximized")

    if os.name == 'nt':  # If system runs on Windows
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        wd = webdriver.Chrome(_resources_path / 'chromedriver.exe', options=options)
    else:  # If system run on Linux
        options.add_argument("user-data-dir=" + _app_config['CHROME_DATA_DIR'])
        wd = webdriver.Chrome(_resources_path / 'chromedriver', options=options)

    url = r"https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/maps-cartes/inventory-inventaire-eng.asp?user=isdm-gdsi&region=MEDS&tst=1&perm=1"
    wd.get(url)
    wd.maximize_window()

    time.sleep(3)

    html = wd.page_source
    df = pd.read_html(html)[0]
    page_list = wd.find_elements(By.XPATH, "//a[@class='paginate_button ']")
    total_page = np.arange(int(page_list[0].text.replace('Jump to: Page\n', '')),
                           int(page_list[-1].text.replace('Jump to: Page\n', '')) + 1, 1)
    stop = False
    while not stop:
        if int([p.text.replace('Jump to: Page\n', '') for p in page_list][-1]) == total_page[0]:
            stop = True
        for p in page_list:
            if int(p.text.replace('Jump to: Page\n', '')) == total_page[0]:
                log.info_page = p.text.replace('Jump to: Page\n', '')
                log.info(f" .. Scrapping page {log.info_page} / {total_page[-1]}")
                total_page = total_page[1:]
                p.click()
                time.sleep(1)
                page_list = wd.find_elements(By.XPATH, "//a[@class='paginate_button ']")
                html = wd.page_source
                df = pd.concat((df, pd.read_html(html)[0]), ignore_index=True)
                break
    wd.quit()

    if dataframe:
        return df
    else:
        cat_name = get_function_name().replace('', '')
        sites = []
        for i, raw in df.iterrows():
            s = Site(lat=raw['LAT (ºN)'],
                     lon=-raw['LONG (ºW)'],
                     name=raw['Station Name'], pip={cat_name: True},
                     id={cat_name: raw['Station']},
                     link={cat_name:
                         [
                             fr"https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/inventory-inventaire/sd-ds-eng.asp?no={raw.Station}&user=isdm-gdsi&region=MEDS"]},
                     data={
                         'Download page': [
                             fr"https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/inventory-inventaire/interval-intervalle-eng.asp?user=isdm-gdsi&region=MEDS&tst=1&no={raw.Station}"]})
            sites.append(s)

        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog


def noaa(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the US TG portal with Sea Level permanent stations

    :param update: if True, update the txt file with station from URL (default is False)
    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note : Scrape data from online portal at : https://opendap.co-ops.nos.noaa.gov/stations/stationsXML.jsp
    """
    cat_name = get_function_name()

    url = r'https://opendap.co-ops.nos.noaa.gov/stations/stationsXML.jsp'
    log.info(f" ... scraping the US NOAA catalog : {url}")

    html_source = get_source_from_url(url, verbose=False)
    soup = BeautifulSoup(html_source, features='html.parser')
    html_stations = soup.find_all('station')

    sites = []
    for i, s in enumerate(html_stations):  # iteration over the stations
        is_sea_level = [True for p in s.find_all('parameter') if p.attrs['name'] == 'Water Level']
        if is_sea_level:
            s = Site(lat=float(s.find("location").find('lat').text),
                     lon=float(s.find("location").find('long').text),
                     name=[s.attrs['name']],
                     provider=cat_name,
                     link={cat_name: f"https://opendap.co-ops.nos.noaa.gov/stations/station.jsp?id={i}"},
                     data={
                         'raw_6min': f"https://opendap.co-ops.nos.noaa.gov/stations/data/getdata.jsp?p=0&id={s.attrs['id']}"},
                     id={cat_name: [s.attrs['id']]},
                     pip={cat_name: True})
            sites.append(s)
    catalog = Catalog(name=cat_name, sites=sites)

    if save: save_to_pickle(catalog)

    if dataframe:
        return catalog.to_dataframe()
    else:
        return catalog


def norwtgn(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the Norwegian TG network with Sea Level permanent stations

    :param update: if True, update the txt file with station from URL (default is False)
    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note : Scrape data from online portal at : https://opendap.co-ops.nos.noaa.gov/stations/stationsXML.jsp
    """

    cat_name = get_function_name()

    url = r'http://api.sehavniva.no/tideapi.php?type=perm&tide_request=stationlist'
    log.info(f" ... scraping the Norwegian TG portal : {url}")

    html_source = get_source_from_url(url, verbose=False)
    soup = BeautifulSoup(html_source, features='html.parser')
    html_stations = soup.find_all('location')

    now = datetime.now()
    now_month = now - timedelta(days=30)

    sites = []
    for i, s in enumerate(html_stations):  # iteration over the stations
        s = Site(lat=float(s.attrs['latitude']),
                 lon=float(s.attrs['longitude']),
                 name=[s.attrs['name']],
                 provider=cat_name,
                 link={cat_name: f"https://www.kartverket.no/en/at-sea/se-havniva/result?location={s.attrs['name']}"},
                 data={
                     'raw_hourly_1month': f"https://api.sehavniva.no/tideapi.php?tide_request=locationdata&lat={s.attrs['latitude']}&lon={s.attrs['longitude']}&refcode=CD&dst=1&fromtime={now_month.strftime('%Y-%m-%d')}&totime={now.strftime('%Y-%m-%d')}&interval=60&uncertainty=1&lang=en&place={s.attrs['name']}&file=TXT"},
                 id={cat_name: [s.attrs['code']]},
                 pip={cat_name: True})
        sites.append(s)
    catalog = Catalog(name=cat_name, sites=sites)

    if save: save_to_pickle(catalog)

    if dataframe:
        return catalog.to_dataframe()
    else:
        return catalog


def indonesian(update=False, dataframe=False, save=True, **kwargs):
    """
    Scrape the INDONESIAN TG network from BGI

    :param dataframe: if True, return a dataframe (default is False)
    :param save: if True, save the Catalog to pickle file (default is False)

    :return: pd.dataframe or Catalog object

    .. Note: The URL source is : https://srgi.big.go.id/pasut-active
    """
    tg_url = 'https://srgi.big.go.id/api/pasut/stations/89./-20./150./20.'
    gnss_url = r'https://srgi.big.go.id/api/cors/stations/89./-20./150./20.'

    # scrape the geosjon from this request
    gdf = gpd.read_file(tg_url)
    df = pd.DataFrame(data={'lon': gdf['geometry'].x,
                            'lat': gdf['geometry'].y,
                            'name': gdf['NAMA_STS']})

    cat_name = get_function_name()
    data_provider = 'Badan Informasi Geospasial'
    if dataframe:
        return df
    else:
        sites = []
        log.info(f" ... scraping the Indonesian TG network from {tg_url}")
        for i, row in df.iterrows():
            stn = row['name'] if isinstance(row['name'], str) else 'unknown'
            link = {}
            s = Site(lat=row['lat'], lon=row['lon'],
                     name=stn.lower(), id={cat_name: stn},
                     link=link, pip={cat_name: True},
                     provider={cat_name: data_provider})
            sites.append(s)
        catalog = Catalog(name=cat_name, sites=sites)
        if save: save_to_pickle(catalog)
        return catalog
