"""
A collection of function to get tide gauge data
from data portals and transform them into Catalogs.
"""


# --- General imports ---
# -----------------------
import pickle
import sys
import os
import fnmatch
from ftplib import FTP
from pathlib import Path

import time
from datetime import datetime
import glob as glob
import numpy as np
import pandas as pd
from tgcat.config import _portal_color, _resources_path, _archives_path, _data_path, _metadata_path,  _app_config
from tgcat.core.generic import Site, Catalog


#%%

_default_null_resolution = pd.Timedelta(hours=0)  # null resolution set to zero timedelta for ploting compatibility

#%%
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def save_to_pickle(_path_provider, name, df, delete_older=True, **kwargs):
    """
    Save raw scraped catalog to pickle

    :param catalog: Catalog object to save into pickle file
    """
    now = datetime.now()
    saving_date = now.strftime("%Y%m%d-%H%M%S")

    if not _resources_path.exists():
        _resources_path.mkdir()
    if not _archives_path.exists():
        _archives_path.mkdir()
    if not _data_path.exists():
        _data_path.mkdir()
    if not (_data_path / _path_provider).exists():
        (_data_path / _path_provider).mkdir()


    older_file = find(name + '_[0-9]*', (_data_path / _path_provider))
    if (len(older_file) != 0.) and (delete_older == True):
        for f in older_file:
            os.remove(f)

    pickle_name = _data_path / _path_provider / Path(name + '_' + saving_date).with_suffix('.pickle')
    df.to_pickle(pickle_name)


def catalog_from_pickle(pickle_path, **kwargs):
    """
    Read a catalog pickle file .

    :return: catalog
    """
    cat = pd.read_pickle(pickle_path)
    return Catalog(name=cat.name, sites=cat.sites, color=cat.color)



def decimal_dt(dt):
    y = pd.to_datetime(dt, format='%Y')
    nday = 366 if y.is_leap_year else 365
    t = y + pd.to_timedelta(f"{dt % 1 * nday}D")
    return t


def concat_chrono_station(dico):
    df_list = []
    sampling = [1 / 24, 1, 7, 31, 365]
    for sampling in dico:
        df_list.append(dico[sampling]['chrono'])
    return pd.concat(df_list)


def create_station_chrono(stname=None, df=pd.DataFrame(data=None)):
    sampling = [1 / 24, 1, 7, 31, 365]
    sample = ['1H', '1D', '1W', '1M', '1Y']
    sta_dict = {key: {} for key in sampling}
    for i in range(len(sampling)):
        sta_dict[sampling[i]] = {'chrono': df.chrono.to_dataframe(stname, resolution=pd.Timedelta(days=sampling[i]))}
        sta_dict[sampling[i]]['chrono']['sample'] = sample[i]
    return sta_dict


#%%

def data_slsmf():
    """
        Scrape data from the SLSMF stattion (old VLIZ) dataportal

        :param dataframe: if True, return a dataframe (default is False)
        :param save: if True, save the Catalog to pickle file (default is False)

        :return: pd.dataframe or Catalog object
    """

    url = 'https://www.ioc-sealevelmonitoring.org/bgraph.php?code=mars&output=tab&period=30'
    df = pd.read_html(url)[0]
    df.columns = df.iloc[0]
    df = df.drop([0])
    df['Time (UTC)']=pd.to_datetime(df['Time (UTC)'], format='%Y-%m-%d %H:%M:%S')
    df = df.set_index('Time (UTC)')
    df['rad(m)'] = df['rad(m)'].astype('float')

    return df

def data_psmsl(frequency=['monthly', 'annual'], save=False):
    path = glob.glob('tgcat/collect/archives/metadata/psmsl*')[0]
    cat = catalog_from_pickle(path)
    gdf = cat.to_gdataframe()
    for index, row in gdf.iterrows():
        stnid = row.ids['psmsl'][0]
        for f in frequency:
            url = f"https://psmsl.org/data/obtaining/rlr.{f}.data/{stnid}.rlrdata"
            try:
                data = pd.read_csv(
                    url,
                    sep=';',
                    header=None,
                    names=['Datetime', 'Level', 'nMissing', 'Flag'],
                    na_values=[-99999]
                )
                if f == 'monthly':
                    data['Datetime'] = data['Datetime'].apply(decimal_dt)
                    data['Datetime'] = pd.to_datetime(data.Datetime.dt.strftime('%Y-%m'))
                    data = data.set_index('Datetime')
                    data = data.drop(columns=['nMissing', 'Flag'])
                    name = f"psmsl_monthly_{stnid}"
                    sta_dic = create_station_chrono(stname=name, df=data)
                    chrono = concat_chrono_station(sta_dic)
                    if save: save_to_pickle('psmsl', name, chrono)
                elif f == 'annual':
                    data['Datetime'] = pd.to_datetime(data['Datetime'], format='%Y')
                    data = data.set_index('Datetime')
                    data = data.drop(columns=['nMissing', 'Flag'])
                    name = f"psmsl_annual_{stnid}"
                    sta_dic = create_station_chrono(stname=name, df=data)
                    chrono = concat_chrono_station(sta_dic)
                    if save: save_to_pickle('psmsl', name, chrono)

                else:
                    print('ERROR wrong frequency')
                    pass
            except:
                pass
    return data


def data_uhslc_fd(frequency=['hourly', 'daily'], save=False):
    path = glob.glob('tgcat/collect/archives/metadata/uhslc_fd*')[0]
    cat = catalog_from_pickle(path)
    gdf = cat.to_gdataframe()
    for index, row in gdf.iterrows():
        stnid = str(row.links['uhslc_fd'][0].split('stn=')[-1][0:3])
        for f in frequency:
            url = f"https://uhslc.soest.hawaii.edu/data/csv/fast/{f}/{f[0]}{stnid}.csv"

            if f[0] == 'd':
                data = pd.read_csv(
                    url,
                    sep=',',
                    skiprows=1,
                    names=['Year', 'Month', 'Day', 'Level'],
                    na_values=[-32767]
                )
                data['Date'] = pd.to_datetime(data[['Year', 'Month', 'Day']])
                data = data.drop(columns=['Year', 'Month', 'Day'])
                data = data.set_index('Date')
                name = f"uhslc_daily_{stnid}"
                sta_dic = create_station_chrono(stname=name, df=data)
                chrono = concat_chrono_station(sta_dic)
                if save: save_to_pickle('uhslc_fd', name, chrono)
            elif f[0] == 'h':
                data = pd.read_csv(
                    url,
                    sep=',',
                    skiprows=1,
                    names=['Year', 'Month', 'Day', 'Hour', 'Level'],
                    na_values=[-32767]
                )
                data['Date'] = pd.to_datetime(data[['Year', 'Month', 'Day', 'Hour']])
                data = data.drop(columns=['Year', 'Month', 'Day', 'Hour'])
                data = data.set_index('Date')
                name = f"uhslc_hourly_{stnid}"
                sta_dic = create_station_chrono(stname=name, df=data)
                chrono = concat_chrono_station(sta_dic)
                if save: save_to_pickle('uhslc_fd', name, chrono)

    return data

def data_uhslc_rq(frequency=['hourly', 'daily'], save=True):
    path = glob.glob('tgcat/collect/archives/metadata/uhslc_rq*')[0]
    cat = catalog_from_pickle(path)
    gdf = cat.to_gdataframe()
    for index, row in gdf.iterrows():
        stnid = str(row.links['uhslc_rq'][0].split('stn=')[-1][0:3])
        for f in frequency:
            url = f"https://uhslc.soest.hawaii.edu/erddap/tabledap/global_{f}_rqds.csv?sea_level%2Ctime%2Clatitude%2Clongitude%2Cstation_name%2Cstation_country%2Cstation_country_code%2Crecord_id%2Cuhslc_id%2Cversion%2Cgloss_id%2Cssc_id%2Cdecimation_method%2Creference_code%2Creference_offset&uhslc_id={stnid}"
            data = pd.read_csv(
                url,
                sep=',',
                skiprows=1,
                na_values=[-32767]
            )
            data = data.drop(columns=['degrees_north', 'degrees_east', 'Unnamed: 4',
                                      'Unnamed: 5', 'Unnamed: 6', 'Unnamed: 7', 'Unnamed: 8', 'Unnamed: 9',
                                      'Unnamed: 10', 'Unnamed: 11', 'Unnamed: 12', 'Unnamed: 13',
                                      'millimeters.1'])
            data = data.rename(columns={'millimeters': 'Level', 'UTC': 'Date'})
            data['Date'] = pd.to_datetime(data.Date)
            data['Date'] = pd.to_datetime(data['Date']).dt.strftime('%Y-%m-%d %H:%M:%S')
            data = data.set_index('Date')
            name = f"uhslc_{f}_{stnid}"
            if save: save_to_pickle('uhslc_rq', name)
    return(data)

#def data_gesla():


#%%

sample = ['1H', '1D', '1W', '1M', '1Y']
sampling = [1/24, 1, 7, 31, 365]