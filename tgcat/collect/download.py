import os
from pathlib import Path
import urllib.request
import requests
import ftplib
import zipfile
import io
import pandas as pd
import requests
from pathlib import Path
from tgcat.config import _metadata_path, _raw_data_path
from datetime import datetime
import fnmatch
import shutil
import numpy as np
import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


#################################################################################
#################################################################################
#################################################################################
# def remove_starting_nan_rows(df, column_name):
#     valid_start_index = df[column_name].first_valid_index()
#     df_cleaned = df.iloc[valid_start_index:].reset_index(drop=True)
#     return df_cleaned

# def split_uhslc(df, type, frequency):
#     for id in pd.unique(df.index):
#         ndf = df.loc[id]
#         ndf = ndf.rename(columns={'sea_level (millimeters)': "Level", 'time (UTC)': "Date"})
#         ndf['Date'] = pd.to_datetime(ndf['Date'], format='%Y-%m-%dT%H:%M:%SZ')
#         name = ndf.index[0].lower()
#         save_raw(f'uhslc_{type}_{frequency}', f'{name}', ndf, update=True)
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def delete_folder(folder):
    if Path(folder).exists():
        shutil.rmtree(folder)
        log.debug(f"deleted {folder}")


# def ftp(host, user, password, remote_path, local_path):
#     ftp = ftplib.FTP(host)
#     ftp.login(user, password)
#     ftp.cwd(remote_path)
#     files = ftp.nlst()
#
#
#     for filename in files[1:10]:
#         local_filename = os.path.join(local_path, filename)
#         print('1..')
#         file = open(local_filename, 'wb')
#         print('..2..')
#         ftp.retrbinary('RETR ' + filename, file.write)
#         print('..3')
#         file.close()
#         # local_file = local_file.replace('\\', '/')
#         # with open(local_file, 'wb') as f:
#         #     ftp.retrbinary('RETR ' + file, f.write)

def save_raw(folder, name, df, delete_older=True, **kwargs):  # TODO change delete_older by update
    """Save chrono to pickle"""
    saving_date = datetime.now().strftime("%Y%m%d-%H%M%S")

    if not _raw_data_path.exists():
        _raw_data_path.mkdir(parents=True)

    if not (_raw_data_path / folder).exists():
        (_raw_data_path / folder).mkdir(parents=True)

    older_file = find(name + '_[0-9]*', (_raw_data_path / folder))
    if (len(older_file) != 0.) and (delete_older == True):
        for f in older_file:
            os.remove(f)

    pickle_name = _raw_data_path / folder / Path(name + '_' + saving_date).with_suffix('.pickle')
    df.to_pickle(pickle_name)
    return pickle_name


#################################################################################
#################################################################################
#################################################################################
def psmsl_rlr(delete_zip=True):
    """ download the zip archive of PMSL_RLR data, uncompress and delete the zip archive"""
    links = ["https://psmsl.org/data/obtaining/rlr.monthly.data/rlr_monthly.zip",
             "https://psmsl.org/data/obtaining/rlr.annual.data/rlr_annual.zip"]
    for url in links:
        tmp_zip_archive = _raw_data_path / Path(url).name
        raw_data_folder = _raw_data_path / Path(url).stem
        delete_folder(raw_data_folder)
        urllib.request.urlretrieve(url, tmp_zip_archive)
        with zipfile.ZipFile(tmp_zip_archive, 'r') as zip_ref:
            zip_ref.extractall(_raw_data_path)
        log.info(f"downloaded and extracted {tmp_zip_archive}")
        if delete_zip:
            tmp_zip_archive.unlink()
            log.info(f"delete {tmp_zip_archive}")


def psmsl_metric(delete_zip=True):
    """ download the zip archive of PMSL_METRIC data, uncompress and delete the zip archive"""
    url = 'https://psmsl.org/data/obtaining/met.monthly.data/met_monthly.zip'
    tmp_zip_archive = _raw_data_path / Path(url).name
    raw_data_folder = _raw_data_path / Path(url).stem
    delete_folder(raw_data_folder)
    urllib.request.urlretrieve(url, tmp_zip_archive)
    with zipfile.ZipFile(tmp_zip_archive, 'r') as zip_ref:
        zip_ref.extractall(_raw_data_path)
    log.info(f"downloaded and extracted {tmp_zip_archive}")
    if delete_zip:
        tmp_zip_archive.unlink()
        log.info(f"delete {tmp_zip_archive}")


def psmsl_gnssir():
    cat = pd.read_pickle(list((_metadata_path).rglob('psmsl_gnssir*'))[0])
    for site in cat:
        url, id = site.data.get('daily_means (zipped file)')[0], site.id.get('psmsl_gnssir')[0]
        r = requests.get(url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall()
        list_filename = []
        log.debug(f"download/unzip {url}")
        for zz in z.filelist:
            if zz.filename.split('.')[-1] == 'csv':
                list_filename.append(zz.filename)
        list_df = []
        for filename in list_filename:
            data = pd.read_csv(filename,
                               skiprows=10,
                               sep=',',
                               names=['raw_height', 'adjusted_height', 'fitted_tide', 'prn', 'signal', 'azimuth',
                                      'elevation'],
                               )
            list_df.append(data)
        data = pd.concat(list_df)
        os.remove(filename)
        save_raw('psmsl_gnssir', f'{id}', data, update=True)
    return data


def uhslc(rq=False):
    # if you want downloaded rq raw datas, it can take  it may take some time, ~>30min
    sample = ['daily', 'hourly']
    function = 'rq' if rq is True else 'fd'
    if rq is True:
        cat = pd.read_pickle(list((_metadata_path).rglob('uhslc_rq*'))[0])
    else:
        cat = pd.read_pickle(list((_metadata_path).rglob('uhslc_fd*'))[0])

    for site in cat:
        filename = site.id['uhslc'][0]
        version = site.id['uhslc_version'][0][-1] if rq == True else ''
        for frequency in sample:
            try:
                df = pd.read_csv(site.data[frequency][0], na_values=-32767, low_memory=True)
                if frequency == 'daily':
                    df["Date"] = df[df.columns[0]].astype(str) + '-' + df[df.columns[1]].astype(str) + '-' + df[
                        df.columns[2]].astype(str)
                    df = df.drop(columns=[df.columns[0], df.columns[1], df.columns[2]])
                if frequency == 'hourly':
                    df["Date"] = df[df.columns[0]].astype(str) + '-' + df[df.columns[1]].astype(str) + '-' + df[
                        df.columns[2]].astype(str) + ' ' + df[df.columns[3]].astype(str) + ':00'
                    df = df.drop(columns=[df.columns[0], df.columns[1], df.columns[2], df.columns[3]])
                df = df.rename(columns={df.columns[0]: "Level"})
            except pd.errors.EmptyDataError:
                df = pd.DataFrame(data={'Date': [pd.NaT], 'Level': [np.nan]})
            df.set_index('Date')
            save_raw(f'uhslc_{function}_{frequency}', f'{filename}{version}', df, update=True)


def sonel():
    sample = ['daily_means (download)',
              'monthly_means (download)',
              'annual_means (download)']
    cat = pd.read_pickle(list((_metadata_path).rglob('sonel*'))[0])
    for site in cat:
        filename = site.id['sonel'][0]
        for frequency in sample:
            df = pd.read_csv(site.data[frequency][0], header=None, sep='\t',
                             names=['Date', 'Level'])
            df.set_index('Date')
            freq = frequency.split('_')[0]
            save_raw(f'sonel_{freq}', f'{filename}', df, update=True)
