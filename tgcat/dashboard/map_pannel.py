"""
This code allow to handle the middle pannel of the dashboard
"""

from dash import dcc, html, dash_table
import dash_daq as daq
import dash_bootstrap_components as dbc
import plotly.express as px
# from tgcat.dashboard.dropdowns import _zoom_region, _zoom_country, _zoom_TG
from tgcat.dashboard.dropdowns import _zoom_region, _zoom_country
from tgcat.info import _catalog_infos

import pandas as pd
import plotly.graph_objs as go


chrono_info = [dcc.Markdown(children="Number of days or hours from which a gap is created in the calculation of chronograms.\n Please note that when too many timelines are displayed, the names may overlap, so please take into account the highlighted names of the chrono.",
                                 style={'textAlign': 'left'}),
                    html.Hr(),
                    dcc.Markdown(children="Portals",
                                 style={'textAlign': 'center', 'font-weight':'bold'}),
                    *[dcc.Markdown(f'- [{v["sn"]}]({v["link"]}) - {v["chrono"]}') for v in _catalog_infos if v['type']=='data' or v['type']=='national']
                    ]

country_info =  [dcc.Markdown(children="The country list is generated from Marine and land zones "
                                       "(the union of world country boundaries and EEF's) from https://www.marineregions.org/downloads.php ",
                                 style={'textAlign': 'left'}),
                    ]
# Initial map
init_map = px.scatter_mapbox(lat=[0], lon=[0],size=[0],
                             mapbox_style="open-street-map", zoom=0.8)
init_map.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0}, height=360)

# -------- Map --------

title_map = dbc.Row(
    [
        dbc.Container(
            [
                dcc.Store(id={"type": "store_map", "index": "loading"}),
                dcc.Store(id={"type": "store_map", "index": "selected"}),
                dbc.Container(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    html.I(className="fas fa-arrow-right", style={'textAlign': 'center', 'font-size': '100%', 'color': '#008cba'}),
                                    style={'padding-right': '0'}, width='auto'
                                ),
                                dbc.Col(
                                    html.H5(children='Sites map', style={'font-weight': 'bold', 'font-size': '125%', 'color': '#008cba','margin-bottom': '0'})
                                )
                            ]
                        )
                    ], className='subtitle',fluid=True, style={"height": "3.2vh"}
                ),
            ], style={"background-color": "white", }
        )
    ]
)

selection_map = dbc.Row(
    [
        dbc.Col(
            dbc.Container(
                [
                dcc.Markdown(children="Select region :", style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%', 'font-weight': 'bold'}),
                dcc.Dropdown(options=[{'label': k.replace('_', ' ').title(), 'value': k} for k in _zoom_region.keys()], placeholder="Select a specific region..", style={'textAlign': 'left', 'font-size': '90%'}, id="region_dropdown"),
                ], style={'padding-left': '0px', 'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            ),
        ),
        dbc.Col(
            dbc.Container([
                dcc.Markdown(children="Select country :", style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%', 'font-weight': 'bold'}),
                dcc.Dropdown(options=[{'label': k.replace('_', ' ').title(), 'value': k} for k in _zoom_country.keys()], placeholder="Select a specific country..", style={'textAlign': 'left', 'font-size': '90%'}, id="country_dropdown"),
                ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            ),
        ),
        dbc.Col(
            dbc.Container(
                [
                dcc.Markdown(children="Select tide gauge :", style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%', 'font-weight': 'bold'}),
                dcc.Dropdown(multi= True, placeholder="Select a specific tide gauge..", style={'textAlign': 'left', 'font-size': '90%'}, id="tg_dropdown"),
                ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            ),
        ),
        html.Span(id='dynamic_dropdown'),
        # dbc.Col(
        #     dbc.Container(
        #         [
        #             dcc.Markdown(children="Show h3 resolution:", style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%', 'font-weight': 'bold'}),
        #             dcc.Checklist(options=[{'label': '', 'value': 'H3res'}], style={'textAlign': 'left', 'font-size': '90%'}, id="hexagon"),
        #         ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
        #     )
        # ),
        # dbc.Col(
        #     dbc.Container(
        #         [
        #             dcc.Markdown(children="Show GLOSS stations :",
        #                          style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%',
        #                                 "font-weight": 'bold'}),
        #             daq.BooleanSwitch(id='gloss_source_switch', on=False, color="#008cba",
        #                               style={'height': '20px', 'size': '35', 'className': 'custom-switch',
        #                                      'padding-left': '10px'}),
        #         ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
        #     )
        # ),
        dbc.Col(
            dbc.Container(
                [
                    dcc.Markdown(children="Show catalog source :",
                                 style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%',
                                        "font-weight": 'bold'}),
                    daq.BooleanSwitch(id='cat_source_switch', on=False, color="#008cba",
                                      style={'height': '20px', 'size': '35', 'className': 'custom-switch',
                                             'padding-left': '10px'}),
                ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            )
        ),
        dbc.Col(
            dbc.Container(
                [
                    dcc.Markdown(children="Show density map :",
                                 style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%',
                                        "font-weight": 'bold'}),
                    daq.BooleanSwitch(id='density_switch', on=False, color="#008cba",
                                      style={'height': '20px', 'size': '35', 'className': 'custom-switch',
                                             'padding-left': '10px'}),
                ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            )
        ),
    ], style={'padding-bottom': '1%'}
)

interact_map = dbc.Row(
    [
        dcc.Graph(id='map', figure=init_map, style={"padding-bottom":"15px"})
    ]
)

map = html.Div(
    [
        title_map,
        selection_map,
        interact_map
    ]
)

# Initial chrono
dx = pd.DataFrame(columns=['Stations', 'Start', 'End'])
init_chrono = px.timeline(dx, x_start='Start', x_end='End', y='Stations')
init_chrono.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0}, height=360)
#

# -------- Chronogram --------

title_chrono =  dbc.Row(
    [
        dbc.Container(
            [
                dcc.Store(id={"type": "store_chrono", "index": "loading"}),
                dcc.Store(id={"type": "store_chrono", "index": "selected"}),
                dbc.Container(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    html.I(className="fas fa-arrow-right", style={'textAlign': 'center', 'font-size': '100%', 'color': '#008cba'}),
                                    style={'padding-right': '0'}, width='auto'
                                ),
                                dbc.Col(
                                    html.H5(children='Sites chrono', style={'font-weight': 'bold', 'font-size': '125%', 'color': '#008cba','margin-bottom': '0'}),
                                    style={'padding-right': '1%'}, width='auto'
                                ),
                                dbc.Col(
                                    [
                                        html.I(className="fas fa-info-circle ml-1", id='chrono_info',
                                               style={'textAlign': 'right', 'font-size': '100%'}),
                                        dbc.Modal(
                                            [
                                                dbc.ModalHeader("Way to compute chronograms", style={'font-weight': 'bold'}),
                                                dbc.ModalBody(chrono_info),
                                                dbc.ModalFooter(
                                                    dbc.Button("Close", id="closeChrono", class_name="ml-auto")
                                                )
                                            ], id="modalChrono", centered=True, scrollable=True)
                                    ]
                                ),
                            ]
                        )
                    ], className='subtitle',fluid=True, style={"height": "3.2vh"}
                ),
            ], style={"background-color": "white", }
        )
    ]
)


interact_chrono = dbc.Row(
    [
        dcc.Graph(id='timeline', figure=init_chrono, style={"padding-bottom":"15px"})
    ]
)



chrono = html.Div(
    [
        title_chrono,
        interact_chrono,
    ]
)

# -------- Table --------

title_table = dbc.Row(
    [
        dbc.Container(
            [
                dcc.Store(id={"type": "store_table", "index": "columns"}),
                dcc.Store(id={"type": "store_table", "index": "loading"}),
                dcc.Store(id={"type": "store_table", "index": "selected"}),
                dcc.Store(id={"type": "store_table", "index": "site"}),
                dcc.Store(id={'type':"store_infos", 'index':'loading'}),
                dcc.Download(id="download-table"),
                dbc.Container(
                    [
                        dbc.Row([
                            dbc.Col(
                                html.I(
                                    className="fas fa-arrow-right", style={'textAlign': 'center', 'font-size': '100%', 'color': '#008cba'}
                                ), style={'padding-right': '0'}, width='auto'
                            ),
                            dbc.Col(
                                html.H5(
                                    children='Sites table', style={'font-weight': 'bold', 'font-size': '125%', 'color': '#008cba', 'margin-bottom': '0'}
                                )
                            )
                        ])
                    ],
                    className='subtitle',
                    fluid=True, style={"height": "3.2vh"}),
            ], style={"background-color": "white", }
        )
    ]
)

selection_table = dbc.Row(
    [
        dbc.Col(
            html.Button('Update selection', id='update_sel_button', n_clicks=0, className='export'),
        ),
        dbc.Col(
            html.Button('Clear selection', id='clear_sel_button', n_clicks=0, className='export', style={'margin-left': '10px'}),
        ),
        dbc.Col(
            children="No site loaded", id='site_loaded_info', style={'textAlign': 'right', 'font-size': '90%'}
        ),
        dbc.Col(
            html.Button('Export table', id='export_button', n_clicks=0, className='export'), style={'textAlign': 'right'}, width=2,
        ),
    ], style={'padding-bottom': '1%', 'padding-top': '1%'}
)

interact_table = dbc.Row(
    [
        dash_table.DataTable(
            id='catalog_table',
            columns=[{"name": "Load Catalog(s) to display tide gauge sites informations ...",
            "id": "info", "editable":False}],
            data=[],
            style_table={'overflow': 'auto','width':'100%', 'height':'250px', 'padding-left': '5px'},
            page_size=25,
            row_selectable='single',
            sort_action='native', #export_format='xlsx',
            filter_action='native',
            style_header={'fontWeight': 'bold', 'textAlign': 'center'},
            style_cell={'textAlign': 'left','fontSize':15,'font-family':'Open Sans'}),
    ]
)

table = html.Div(
    [
        title_table,
        selection_table,
        interact_table
    ]
)


map_pannel = dbc.Container([html.Br(),
                            map,
                            table,
                            chrono,
                            html.Br()],
                            style={"background-color":"white"}, fluid=True)
