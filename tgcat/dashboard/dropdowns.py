import geopandas as gpd
# import shapefile
from shapely.geometry import Polygon, MultiPolygon
from pathlib import Path
from tgcat.collect.scrape import find
from tgcat.config import _shp_path, _connectivity_path


# --- Handle sources for dashboard interface ---
# ----------------------------------------------

# Map zoom paramteres
# shape_region = shapefile.Reader(str(_shp_path / 'world_area.shp'))
# _zoom_region = {}
# for poly in shape_region.shapeRecords():
#     a = poly.__geo_interface__['properties']['area']
#     if a == 'oceania':
#         p = MultiPolygon([Polygon(pp[0]) for pp in poly.__geo_interface__['geometry']['coordinates']])
#     else:
#         p = Polygon(poly.__geo_interface__['geometry']['coordinates'][0])
#     _zoom_region[a] = p

shape_region = gpd.read_file(str(_shp_path / 'world_area.shp'))
_zoom_region = {}
for index, row in shape_region.iterrows():
    a = row['area']
    p = row['geometry']
    _zoom_region[a] = p


# shape_country = shapefile.Reader(str(_shp_path / 'EEZ_Land_v3_202030.shp'))
# _zoom_country = {}
# for poly in shape_country.shapeRecords():
#     if  poly.__geo_interface__['properties']['POL_TYPE'] != 'Landlocked country' and poly.__geo_interface__['properties']['POL_TYPE'] != 'Joint regime (EEZ)' and poly.__geo_interface__['properties']['POL_TYPE'] != 'Overlapping claim':
#         a = poly.__geo_interface__['properties']['UNION']
#         try:
#             p = Polygon(poly.__geo_interface__['geometry']['coordinates'][0])
#         except:
#             p = MultiPolygon([Polygon(pp[0]) for pp in poly.__geo_interface__['geometry']['coordinates']])
#         _zoom_country[a] = p
#     else:
#         pass

shape_country = gpd.read_file(str(_shp_path / 'EEZ_Land_v3_202030.shp'))
ds = shape_country[(shape_country["POL_TYPE"] == 'Landlocked country') | (shape_country["POL_TYPE"] == 'Joint regime (EEZ)') | (shape_country["POL_TYPE"] == 'Overlapping claim') ]
shape_country = shape_country[~shape_country['UNION'].isin(list(ds['UNION'].values))]
_zoom_country = {}
for index, row in shape_country.iterrows():
    a = row['UNION']
    p = row['geometry']
    _zoom_country[a] = p

# all_path_connectiviy = find('*.pickle', _connectivity_path)
# pt = []
# nm = []
# ky = []
# for path in all_path_connectiviy:
#     connectivity_table = pd.read_pickle(path)
#     keys = [i for i in list(connectivity_table.keys()) if len(i.split('f')) == 7]
#     new_connectivity_table = {key: connectivity_table[key] for key in keys}
#     for key in list(new_connectivity_table.keys()):
#         if len(list(new_connectivity_table[key].values())[0]) > 1:
#             for i in range(len(list(new_connectivity_table[key].values())[0])-1):
#                 ky.append(key)
#         pt = pt + list(new_connectivity_table[key].values())[0]
#         nm = nm + list(new_connectivity_table[key].values())[1]
#         ky.append(key)
# d = {'keys': ky, 'names': nm, 'paths': pt}
# df = pd.DataFrame(data=d)
# df = df.drop_duplicates(subset=['keys', 'names'], keep='last')
# _zoom_TG = {}
# for index, row in df.iterrows():
#     a = row.names
#     p = row['keys']
#     _zoom_TG[a] = p



