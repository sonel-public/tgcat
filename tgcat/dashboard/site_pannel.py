"""
This code allow to handle the middle pannel of the dashboard
"""

from dash import dcc, html
import dash
import dash_bootstrap_components as dbc


# -------- Site informations --------

site_info = dbc.Row(
    html.Div([
        dcc.Store(id={'type':"store_infos", 'index':'loading'}),
        dcc.Store(id={"type": "identic_table", "index": "identic_sites"}),
        # Portal option title
        dbc.Container(
            [
                dbc.Row([
                    dbc.Col(html.I(className="fas fa-arrow-right",
                                   style={'textAlign': 'center', 'font-size': '100%', 'color': '#008cba'}),
                            style={'padding-right': '0'}, width='auto'),
                    dbc.Col(html.H5(children='Site informations',
                                    style={'font-weight': 'bold', 'font-size': '125%', 'color': '#008cba',
                                           'margin-bottom': '0'}))
                ])
            ],
            className='subtitle',
            fluid=True, style={"height": "3.2vh"}),
        html.Br(),
        dbc.Container(id="site_content", className="m-0",
                      style={"height": "30vh"}, fluid=True),
        html.Br(),
        dbc.Container(id="site_data", className="m-0",
                      style={"height": "34vh", "border": "0px grey solid",
                             "background-color": "white"}, fluid=True)
    ],
    style={"border":"0px grey solid"}),
    class_name="h-100")

site_pannel = dbc.Container([html.Br(),
                                site_info,
                                html.Br()],
                                style={"height": "80vh",
                                       "background-color":"white"}, fluid=True)