"""
This code allow to modify the left pannel of the dashboard
"""

from dash import dcc, html
import dash_bootstrap_components as dbc
from tgcat.info import _catalog_infos
from tgcat.collect.scrape import find
from tgcat.config import _metadata_path
import pandas as pd
from pathlib import Path

def display_archives_date():
    archives_data = find('*', _metadata_path)
    if len(archives_data) != 0. :
        dates = pd.DatetimeIndex([pd.to_datetime(Path(f).stem.split('_')[-1]) for f in archives_data])
        older = dates.min().strftime('%Y-%m-%d %H:%M')
        newer = dates.max().strftime('%Y-%m-%d %H:%M')
        return older+' / '+newer
    else:
        return 'No archives'



# -------- Catalog picker --------

meta_portal_info = [dcc.Markdown(children="Portals that only give sites description "
                                          "without link to download data.",
                                 style={'textAlign': 'left'}),
                    html.Hr(),
                    dcc.Markdown(children="Available portals",
                                 style={'textAlign': 'center', 'font-weight':'bold'}),
                    *[dcc.Markdown(f'- [{v["sn"]}]({v["link"]}) - {v["ln"]}') for v in _catalog_infos if v['type']=='meta']
                    ]
data_portal_info = [dcc.Markdown(children="Portals that provide data description and a "
                                          "possible link to the data (complete or not, free or not).",
                                 style={'textAlign': 'left'}),
                    html.Hr(),
                    dcc.Markdown(children="Available portals",
                                 style={'textAlign': 'center', 'font-weight':'bold'}),
                    *[dcc.Markdown(f'- [{v["sn"]}]({v["link"]}) - {v["ln"]}') for v in _catalog_infos if v['type']=='data']
                    ]
national_portal_info = [dcc.Markdown(children="National portals that provide data description and a "
                                          "possible link to the data (complete or not, free or not).",
                                 style={'textAlign': 'left'}),
                    html.Hr(),
                    dcc.Markdown(children="Available portals",
                                 style={'textAlign': 'center', 'font-weight':'bold'}),
                    *[dcc.Markdown(f'- [{v["sn"]}]({v["link"]}) - {v["ln"]}') for v in _catalog_infos if v['type']=='national']
                    ]


displayed_load_tab = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.Checklist(id='select_all_cat',
                                      labelStyle={'display': 'block', 'font-size': '90%', 'color': 'black',
                                                  'text-indent': '0px', 'margin': '0px 5px 0px 0px'},
                                      options=[{'label': 'Select/unselect all catalog(s)', 'value': 'select'}],
                                      inputStyle={'vertical-align': 'middle', 'margin': '0px 5px 0px 0px'})
                    ]
                ),
    ]
        ),
    html.Br(),
    html.Div(
        [
            dbc.Button("Load selected catalog(s)",
                       id="cat_button",
                       className="analyse",
                       style={'display': 'flex', 'font-size': '90%', 'align-items':'center', 'justify-content': 'center'})
        ], className="d-grid gap-1 col-8 mx-auto"
    )
    ]
)


displayed_update_tab = dbc.Container(
    [
        html.P('Older/newer archive : ' + display_archives_date(), style={'font-size': '80%'}, id='archive_date'),
        html.Br(),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Button(
                            "Update selected",
                            id="update_select_button",
                            className="me-2",
                            color="primary",
                            style={'display': 'flex', 'font-size': '90%', 'align-items': 'center', 'justify-content': 'center'}
                        )
                    ], className="d-grid gap-2 col-5 mx-auto"
                ),
                dbc.Col(
                    [
                        dbc.Button(
                            "Update all",
                            id="update_all_button",
                            className="me-2",
                            color="primary",
                            style={'display': 'flex', 'font-size': '90%', 'align-items': 'center', 'justify-content': 'center'}
                        )
                    ], className="d-grid gap-2 col-5 mx-auto"
                )
            ]
        )
    ], style={'border': '0px #008cba solid', 'position': 'relative'},
    fluid=True
)


catalog_picker = dbc.Row(
    dbc.Container(
        [
            # Portal option title
            dbc.Container(
                [
                    dbc.Row(
                        [
                            dbc.Col(
                                html.I(className="fas fa-arrow-right", style={'textAlign': 'center', 'font-size': '100%', 'color':'#008cba'}),
                                style={'padding-right': '0'}, width='auto'
                            ),
                            dbc.Col(
                                html.H5(children='Catalog(s) selection', style={'font-weight':'bold','font-size':'125%', 'color':'#008cba', 'margin-bottom':'0'})
                            )
                        ]
                    )
                 ], className='subtitle', fluid=True, style={"height": "3.2vh"}
            ),
            html.Br(),
            # Section Metadata portals
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.P(children=' Metadata catalogs',
                            style={'font-weight':'bold','textAlign': 'left', 'font-size':'95%'}),
                        ], style={'padding-right': '1%'}, width='auto'
                    ),
                 html.Br(),
                 dbc.Col(
                     [
                         html.I(className="fas fa-info-circle ml-1", id='metacatalog_info', style={'textAlign': 'right', 'font-size': '100%'}),
                         dbc.Modal(
                             [
                                 dbc.ModalHeader("Metadata Portals", style={'font-weight':'bold'}),
                                    dbc.ModalBody(meta_portal_info),
                                    dbc.ModalFooter(
                                        dbc.Button("Close", id="closeMeta", class_name="ml-auto")
                                    )
                             ], id="modalMeta", centered=True, scrollable=True)
                     ]
                 )
                ]
            ),

            dcc.Checklist(
                id='metacatalog_check',
                labelStyle={'display': 'block', 'font-size': '85%',
                            'text-indent': '0px', 'margin': '0px 5px 0px 0px'},
                options=[{'label': v["sn"], 'value': v['function_name']} for v in _catalog_infos if v['type']=='meta'],
                inputStyle={'vertical-align': 'middle', 'margin': '0px 5px 0px 0px'},
                style={'-webkit-column-count': '3', 'column-count':'3', 'column-width':'8rem'}
            ),
            html.Br(),
            # Section data portals
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.P(children=' Data catalogs', style={'font-weight':'bold','textAlign': 'left', 'font-size':'95%'})
                        ], style={'padding-right': '1%'}, width='auto'
                    ),
                 html.Br(),
                 dbc.Col(
                     [
                         html.I(className="fas fa-info-circle ml-1", id='datacatalog_info', style={'textAlign': 'right', 'font-size': '100%'}),
                         dbc.Modal(
                             [
                                 dbc.ModalHeader("Data Portals", style={'font-weight':'bold'}),
                                    dbc.ModalBody(data_portal_info),
                                    dbc.ModalFooter(
                                        dbc.Button("Close", id="closeData", class_name="ml-auto")
                                    )
                             ], id="modalData", centered=True, scrollable=True)
                    ]
                 )
                 ]
            ),
            dcc.Checklist(
                id='datacatalog_check',
                labelStyle={'display': 'block', 'font-size': '85%', 'color':'black',
                            'text-indent': '0px', 'margin': '0px 5px 0px 0px'},
                options=[{'label': v['sn'], 'value': v['function_name']} for v in _catalog_infos if v['type']=='data'],
                inputStyle={'vertical-align': 'middle', 'margin': '0px 5px 0px 0px'},
                style={'-webkit-column-count': '3', 'column-count':'3', 'column-width':'5rem'}),

            html.Br(),
            # Section national portals
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.P(children=' National catalogs', style={'font-weight': 'bold', 'textAlign': 'left', 'font-size': '95%'})
                        ], style={'padding-right': '1%'}, width='auto'
                    ),
                html.Br(),
                dbc.Col(
                    [
                        html.I(className="fas fa-info-circle ml-1", id='nationalcatalog_info',
                                style={'textAlign': 'right', 'font-size': '100%'}),
                        dbc.Modal(
                            [
                                dbc.ModalHeader("National Portals", style={'font-weight': 'bold'}),
                                dbc.ModalBody(national_portal_info),
                                dbc.ModalFooter(
                                    dbc.Button("Close", id="closeNationalData", class_name="ml-auto")
                                )
                            ], id="modalNationalData", centered=True, scrollable=True)
                    ]
                )
            ],
        ),
            dbc.Container(
                [
                    dcc.Markdown(children="", style={'textAlign': 'left', 'font-size': '90%', 'padding-top': '4%',
                                                     'font-weight': 'bold'}),
                    dcc.Dropdown(
                        options=[{'label': v['sn'].replace('_', ' ').title(), 'value': v['function_name']} for v in _catalog_infos if v['type']=='national'],
                        multi=True,
                        placeholder="Select a specific portal..",
                        style={'textAlign': 'left', 'font-size': '90%'},
                        id="nationalcatalog_check"),
                ], style={'vertical-align': 'middle', 'text-align': 'right', 'justify-content': 'right'}
            ),
            html.Br(),
        dcc.Store(id='load_cat_mode', data='wait', storage_type='session'),
        dbc.Tabs(id="cat_tabs", active_tab="load", children=[
            dbc.Tab(displayed_load_tab, label="Loading options", tab_id="load",tabClassName="ms", disabled=True,
                                     label_style={'font-size': '80%', 'font-weight': 'bold', 'color':'black'}, ),
                         ]),
        ], style={"border":"0px #008cba solid", "position":"relative"})
)


# -------- Catalog stats --------

catalog_stat = dbc.Row(
    html.Div([
        dcc.Store(id={'type':"store_stat", 'index':'loading'}),
        dcc.Store(id={'type':"store_stat", 'index':'selected'}),
        dcc.Store(id="store_update_output"),
        dcc.Store(id="store_all_update_output"),
        # Portal option title
        dbc.Container(
            [
                dbc.Row([
                    dbc.Col(html.I(className="fas fa-arrow-right", style={'textAlign': 'center', 'font-size': '100%', 'color':'#008cba'}),
                            style={'padding-right': '0'}, width='auto'),
                    dbc.Col(html.H5(children='Catalog(s) statistics', style={'font-weight':'bold','font-size':'125%', 'color':'#008cba', 'margin-bottom':'0'}))
                    ])
             ],
            className='subtitle',
            fluid=True, style={"height": "3.2vh"}),
        html.Br(),
        dbc.Tabs([
            dbc.Tab(label="Sites", tab_id="sites", label_style ={'font-size': '70%'}),
            dbc.Tab(label="Selected sites", tab_id="sel", label_style ={'font-size': '70%'}),
        ], id="tabs", active_tab="sites"),
        dcc.RadioItems(['Linear', 'Log'], 'Linear', id='crossfilter-xaxis-type',
                       labelStyle={'display': 'inline-block', 'marginTop': '5px'}),

        dbc.Container(id="tab-content", className="m-0", fluid=True,
                      style={"height": "300px","background-color":"white", "border":"1px lightgrey solid"})
    ],
    style={"border":"0px grey solid"}))



catalog_pannel = dbc.Container(
    [
        html.Br(),
        catalog_picker,
        html.Br(),
        catalog_stat
    ], style={"background-color":"white"}, fluid=True
)
