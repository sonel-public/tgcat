# Python common packages
import glob

import numpy as np
import re
import pandas as pd
import geopandas as gpd
import plotly.graph_objs as go
import plotly.express as px
import ast
from shapely.geometry import Point

# Import DASH usefull methodsS
import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State, ALL
import dash_bootstrap_components as dbc
from dash.long_callback import DiskcacheLongCallbackManager

# TGCat tools for portal scrapping and displaying
import tgcat.collect.scrape as scrape
from tgcat.config import _app_config, _connectivity_path
from tgcat.info import _catalog_infos, _portal_color
from tgcat.dashboard.dropdowns import _zoom_region, _zoom_country
from tgcat.dashboard.header import header
from tgcat.dashboard.catalog_pannel import catalog_pannel, display_archives_date
from tgcat.dashboard.map_pannel import map_pannel
from dash.exceptions import PreventUpdate


# Tools to handle with running wheels
import diskcache

cache = diskcache.Cache("./cache")
background_callback_manager = DiskcacheLongCallbackManager(cache)

# link to css model for the app display
FONT_AWESOME = "https://use.fontawesome.com/releases/v5.7.2/css/all.css"


# -------- MAIN DASHBOARD DEFINITION --------
# -------------------------------------------

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.YETI, FONT_AWESOME],
                requests_pathname_prefix=_app_config['BASE_PATH']+'/',
                suppress_callback_exceptions=True,
                background_callback_manager=background_callback_manager)

# Main layout of the app
row = dbc.Container(
    [
        header,
        html.Br(),
        dbc.Row([dbc.Col(html.Div(catalog_pannel), xs=12, sm=12, md=12, lg=12, xl=3),
                 dbc.Col(html.Div(map_pannel), xs=12, sm=12, md=12, lg=12, xl=9),
                 # dbc.Col(html.Div(site_pannel), xs=12, sm=12, md=12, lg=12, xl=3)
                 ]),
    ], fluid=True,
)

app.layout = row
app.title = 'TGCAT'
server = app.server


# -------- DYNAMIC METHODS TO INTERACT IN THE APP --------
# --------------------------------------------------------

# --- POPUP WINDOWS ---

# Project informations (open when click on the button "More about the project")
@app.callback(Output("modalProject", "is_open"),
              [Input("openProject", "n_clicks"),Input("closeProject", "n_clicks")],
              [State("modalProject", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2 :
        return not is_open
    return is_open

# Informations about "Metadata portals" (when click on i symbol)
@app.callback(Output("modalMeta", "is_open"),
              [Input("metacatalog_info", "n_clicks"),
               Input("closeMeta", "n_clicks")],
              [State("modalMeta", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2 :
        return not is_open
    return is_open

# Informations about "Data portals" (when click on i symbol)
@app.callback(Output("modalData", "is_open"),
              [Input("datacatalog_info", "n_clicks"),
               Input("closeData", "n_clicks")],
              [State("modalData", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2:
        return not is_open
    return is_open

# Informations about "National data portals" (when click on i symbol)
@app.callback(Output("modalNationalData", "is_open"),
              [Input("nationalcatalog_info", "n_clicks"),
               Input("closeNationalData", "n_clicks")],
              [State("modalNationalData", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2:
        return not is_open
    return is_open

# Informations about "Chrono portals" (when click on i symbol)
@app.callback(Output("modalChrono", "is_open"),
              [Input("chrono_info", "n_clicks"),
               Input("closeChrono", "n_clicks")],
              [State("modalChrono", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2:
        return not is_open
    return is_open
# Informations about probabilities computation (when click on i symbol)
@app.callback(Output("modalProbaInfo", "is_open"),
              [Input("proba_info", "n_clicks"),
               Input("closeProbaInfo", "n_clicks")],
              [State("modalProbaInfo", "is_open")])
def toggle_modal(input1, input2, is_open):
    if input1 or input2 :
        return not is_open
    return is_open

# --- LOADING AND INITIALIZATION METHODS ---

# Select all catalogs option
@app.callback([Output("metacatalog_check", "value"),
               Output("datacatalog_check", "value"),
               Output("nationalcatalog_check", "value")],
              Input("select_all_cat", "value"),
              [State("metacatalog_check", "options"),
               State("datacatalog_check", "options"),
               State("nationalcatalog_check", "options")],
              prevent_initial_call=True)
def select_all_checklist(select, meta_opt, data_opt, nat_opt):
    meta_select = [option["value"] for option in meta_opt if select]
    data_select = [option["value"] for option in data_opt if select]
    nat_select = [option["value"] for option in nat_opt if select]
    return meta_select, data_select, nat_select


# Complex method to load the selected catalogs
@app.callback(
    output =[Output({'type':"store_stat", 'index':'loading'}, "data"),
             Output({"type":"store_table", "index":"columns"}, 'data'),
             Output({'type':"store_table", 'index':'loading'}, 'data'),
             # Output("clear_sel_button", "n_clicks"),
             Output("alert-error", 'is_open'),
             Output("alert-error", 'children')],
    inputs = [Input("cat_button", "n_clicks"),
             State("metacatalog_check", "value"),
             State("datacatalog_check", "value"),
             State("nationalcatalog_check", "value")
              ],
    # background=True,
    # #manager=background_callback_manager, #this can be set in the app constructor
    running=[(Output("cat_button", "children"),
              [dbc.Spinner(size="sm"), "Loading..."],
              "Load selected catalog(s)")],
    prevent_initial_call=True)
def load_catalog(n, meta_vals, data_vals, nat_vals):
    """
    This callback update the catalogs and create the figures, map and table.
    """

    # STEP 1 : Load catalogs

    concat = np.append(meta_vals, data_vals) # Load catalogs names
    concat = np.append(concat, nat_vals)
    value = concat[concat != None]
    # merge = True if merge_val is not None else False  # Merge option

    # Case n°1 : when no click on "Load button" or no portal selected
    if not n or len(value) == 0:
        return None, None, None, True, "WARNING : No catalog selected !"

    # Case n°2 : when click on "Load button" and 1 portal selected
    elif n and len(value) == 1:
        cat_name = value[0].replace('-', '_')
        archive_data = scrape.find(cat_name+'_?[0-9]*', scrape._metadata_path)
        # In case the archive is already created
        if len(archive_data) == 1 :
            cat = scrape.catalog_from_pickle(archive_data[0])
        else:
            print(f"No archive for the catalog {cat_name} : create the archive before select it !")
            text = f"WARNING : No archive for the catalog {cat_name} : create the archive before select it !"
            return None, None, None,  True, text

    # Case n°3 : when click on "Load button" and multiple portals selected
    elif n and len(value) != 1:
        cat_name = value[0].replace('-', '_')
        archive_data = scrape.find(cat_name + '_?[0-9]*', scrape._metadata_path)
        if len(archive_data) == 1:
            cat = scrape.catalog_from_pickle(archive_data[0])
        else:
            print(f"No archive for the catalog {cat_name} : create the archive before select it !")
            text = f"WARNING : No archive for the catalog {cat_name} : create the archive before select it !"
            return None, None, None,  True, text

        for v in value[1:]:
            cat_name = v.replace('-', '_')
            archive_data = scrape.find(cat_name + '_?[0-9]*', scrape._metadata_path)
            if len(archive_data) == 1:
                cat2 = scrape.catalog_from_pickle(archive_data[0])
            else:
                text = f"WARNING : No archive for the catalog {cat_name} : create the archive before select it !"
                return None, None, None, True, text
            #     f = v.split('-')
            #     cat2 = getattr(scrape, f[0])(rq=f[-1])
            cat = cat.union(cat2)
        # if merge :
        #     cat.merge_duplicates(inplace=True)
    # Case n°4 : All other cases ...
    else:
        return None, None, None, False, ""

    # STEP 2 : Create outputs

    # Create statistics data from catalog
    data_stat = {'sites': cat.count_pip_len().to_dict('records'),
                 'link': cat.count_pip_len().to_dict('records')}

    # Create data_table from catalog
    cols_name = ['id', 'label', 'name', 'longitude', 'latitude', 'cat','#id']
    table_columns = [{"name": i.capitalize(), "id": i} for i in cols_name]
    table_data = cat.to_dataframe()
    table_data.ids = table_data.ids.astype(str)
    table_data.h3 = table_data.h3.astype(str)
    table_data.links = table_data.links.astype(str)
    table_data.datas = table_data.datas.astype(str)
    table_data = table_data.to_dict('records')

    return data_stat, table_columns, table_data, False, ""



# Clear button to reset dataset to the original one
@app.callback(
    output=[Output({'type':"store_stat", 'index':'selected'}, "clear_data"),
            Output({'type':"store_table", 'index':'selected'}, 'clear_data'),
            Output({'type':"store_table", 'index':'site'}, 'clear_data'),
            Output("catalog_table", "selected_row_ids"),
            Output("region_dropdown", "value"),
            Output("country_dropdown", "value"),
            Output("tg_dropdown", "value"),
            Output('map', 'selectedData'),
            Output('map', 'clickData'),
            Output('timeline', 'figure', allow_duplicate=True),
            Output('catalog_table', 'filter_query')],
    inputs=[Input("clear_sel_button", "n_clicks")],
    prevent_initial_call=True)
def reset_seletion(n_reset):
    if not n_reset or n_reset==0 :
        # Return nothing when no clicking
        return dash.no_update
    else:
        dx = pd.DataFrame(columns=['Stations', 'Start', 'End'])
        init_chrono = px.timeline(dx, x_start='Start', x_end='End', y='Stations')
        init_chrono.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0}, height=360)
        return True, True, True, None, None, None, None, None, None, init_chrono, ''



# --- CHRONO METHODS ---
@app.callback(Output('timeline', 'figure'),
              Input({"type":"store_table", "index":ALL}, 'data'),
              Input('cat_source_switch', 'on'))
def render_chrono(data, cat_switch):
    """
    Generates chronos from the loading dataset.
    """

    if data[1] is None:
        return dash.no_update
    # Case 1 : No selection yet, only loading dataset (data[1])
    if data[2] is not None:
        df_cat = pd.DataFrame(data=data[2])
        list_to_concat = []
        for index, row in df_cat.iterrows():
            list_path = [list(file.iterdir())[0] for file in _connectivity_path.iterdir() if file.name.startswith(row['cat'])]
            if len(list_path) == 0:
                pass
            for path_connectivity in list_path:
                h3id = ast.literal_eval(df_cat.loc[index, 'h3'])['hex14']
                cnc = pd.read_pickle(path_connectivity)
                chr = pd.read_pickle(cnc[h3id]['path'][0])
                chr['name'] = f"{df_cat.loc[index, 'name'].upper()}_{chr.name.unique()[0].split('_')[1]} ({chr.name.unique()[0].split('_')[-1]})"
                chr['catalog'] = row['cat']
                list_to_concat.append(chr)
        chr = pd.concat(list_to_concat)

        if cat_switch is False:
            fig = px.timeline(chr, x_start="start", x_end="end", y="name", category_orders={'name': list(chr.name.values)})
        elif cat_switch is True:
            fig = px.timeline(chr, x_start="start", x_end="end", y="name", color='catalog', color_discrete_map=_portal_color, category_orders={'name': list(chr.name.values)})
        return fig
    else:
        return dash.no_update


# --- MAPPING METHODS ---

# Method to update the displayed data
@app.callback(Output('map', 'figure'),
              Input({"type":"store_table", "index":ALL}, 'data'),
              Input('cat_source_switch', 'on'),
              Input('density_switch', 'on'),
              prevent_initial_call = True)
def render_map(data, cat_switch, density_switch):
    """
    Generates maps from the loading dataset.
    """
    if cat_switch is True and data[1] is None:
        return dash.no_update

    # Case 1 : No selection yet, only loading dataset (data[1])
    if data[1] is not None:
        df_cat = pd.DataFrame(data=data[1])
        df_cat['size'] = len(df_cat)*[1]
        df_cat['opacity'] = len(df_cat) * [1]
        df_cat['density'] = len(df_cat) * [1]
        df_cat['colors'] = len(df_cat)*['dimgrey']
    # else :
    #     return dash.no_update

    # Case 2 : Geodgraphic selection, updating plot with selected dataset (data[2])
    if data[2] is not None:
        df_cat_sel = pd.DataFrame(data=data[2])
        if df_cat_sel.empty is True:
            return dash.no_update
        else:
            zoom_level, map_center = zoom_center(lons=df_cat_sel['longitude'], lats=df_cat_sel['latitude'])
            if len(df_cat_sel)<=1 :
                zoom_level = zoom_level - 8
            else:
                zoom_level = zoom_level - 1
            # Change point color of the selected sites
            mask = (df_cat['longitude'].isin(df_cat_sel['longitude'])) & (df_cat['latitude'].isin(df_cat_sel['latitude'])) & (df_cat['name'].isin(df_cat_sel['name']))
            df_cat.loc[df_cat[mask].index, "colors"] = len(df_cat[mask]) * ['#008cba']
    else :
        zoom_level = 1
        map_center = None

    # Case 3 : Selecting a site in the table (data[3])
    if data[3] is not None:
        df_cat_site = pd.DataFrame(data=data[3], index=[data[3]['id']])
        map_center = {'lon':df_cat_site['longitude'].values[0], 'lat':df_cat_site['latitude'].values[0]}
        zoom_level = 11

    if cat_switch is True :
        # Figure creation
        fig = px.scatter_mapbox(df_cat, lat=df_cat['latitude'], lon=df_cat['longitude'],
                                color=df_cat['cat'].values, size=df_cat['size'], opacity=df_cat['opacity'],
                                color_discrete_map=scrape._portal_color, mapbox_style="open-street-map",
                                zoom=zoom_level, center=map_center, size_max=zoom_level + 4,
                                text='name')
        fig.update_layout(showlegend=True, legend_title_text='Original catalog',
                          legend=dict(orientation="v", xanchor="left", yanchor="top", y=1, x=-0.2,
                                      font=dict(size=14)))

    if cat_switch is False:
        # Figure creation
        fig = px.scatter_mapbox(df_cat, lat=df_cat['latitude'], lon=df_cat['longitude'],
                                color=df_cat['colors'].values, size=df_cat['size'], opacity=df_cat['opacity'],
                                color_discrete_map='identity', mapbox_style="open-street-map",
                                zoom=zoom_level, center=map_center, size_max=zoom_level + 4,
                                text='name')
        fig.update_layout(showlegend=True, legend_title_text='Original catalog',
                          legend=dict(orientation="v", xanchor="left", yanchor="top", y=1, x=-0.2,
                                      font=dict(size=14)))

    fig.update_layout(clickmode='select+event')
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0}, height=360)
    fig.update_traces(mode="markers",
                      hovertemplate="<b>%{text}</b><br><br>" +
                                        "Longitude: %{lon:,.3f}<br>" +
                                        "Latitude: %{lat:,.3°}<br>" +
                                        "<extra></extra>")

    if density_switch is True :
        fig = px.density_mapbox(df_cat, lat='latitude', lon='longitude', z='density',
                                radius=7, zoom=zoom_level, center=map_center,mapbox_style="open-street-map")
        fig.update_layout(showlegend=True, legend_title_text='Original catalog',
                          legend=dict(orientation="v", xanchor="left", yanchor="top", y=1, x=-0.2,
                                      font=dict(size=14)))
        fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0}, height=360)

    # # Displaying the selected site in red
    # if data[3] is not None:
    #     fig.add_trace(go.Scattermapbox(lat=df_cat_site['latitude'],
    #                                    lon=df_cat_site['longitude'],
    #                                    text=df_cat_site['name'],
    #                                    mode='markers',
    #                                    hovertemplate="<b>Selected site : %{text}</b><br>" +
    #                                                  "<extra></extra>",
    #                                    marker=go.scattermapbox.Marker(size=17,color='rgb(255, 0, 0)',opacity=0.7),
    #                                    showlegend=False))

    return fig


def zoom_center(lons: tuple = None, lats: tuple = None, lonlats: tuple = None,
                format: str = 'lonlat', projection: str = 'mercator',
                width_to_height: float = 3.0) -> (float, dict):
    """
    Function to finds optimal zoom and centering for a plotly mapbox.
    Temporary solution awaiting official implementation, see: https://github.com/plotly/plotly.js/issues/3434

    Parameters
    --------
    lons: tuple, optional, longitude component of each location
    lats: tuple, optional, latitude component of each location
    lonlats: tuple, optional, gps locations
    format: str, specifying the order of longitud and latitude dimensions,
        expected values: 'lonlat' or 'latlon', only used if passed lonlats
    projection: str, only accepting 'mercator' at the moment,
        raises `NotImplementedError` if other is passed
    width_to_height: float, expected ratio of final graph's with to height,
        used to select the constrained axis.

    Returns
    --------
    zoom: float, from 1 to 20
    center: dict, gps position with 'lon' and 'lat' keys

    >>> print(zoom_center((-109.031387, -103.385460),
    ...     (25.587101, 31.784620)))
    (5.75, {'lon': -106.208423, 'lat': 28.685861})
    """
    if lons is None and lats is None:
        if isinstance(lonlats, tuple):
            lons, lats = zip(*lonlats)
        else:
            raise ValueError(
                'Must pass lons & lats or lonlats'
            )

    maxlon, minlon = max(lons), min(lons)
    maxlat, minlat = max(lats), min(lats)
    center = {
        'lon': round((maxlon + minlon) / 2, 6),
        'lat': round((maxlat + minlat) / 2, 6)
    }

    # longitudinal range by zoom level (20 to 1)
    # in degrees, if centered at equator
    lon_zoom_range = np.array([
        0.0007, 0.0014, 0.003, 0.006, 0.012, 0.024, 0.048, 0.096,
        0.192, 0.3712, 0.768, 1.536, 3.072, 6.144, 11.8784, 23.7568,
        47.5136, 98.304, 190.0544, 360.0
    ])

    if projection == 'mercator':
        margin = 1.2
        height = (maxlat - minlat) * margin * width_to_height
        width = (maxlon - minlon) * margin
        lon_zoom = np.interp(width, lon_zoom_range, range(20, 0, -1))
        lat_zoom = np.interp(height, lon_zoom_range, range(20, 0, -1))
        zoom = round(min(lon_zoom, lat_zoom), 2)
    else:
        raise NotImplementedError(
            f'{projection} projection is not implemented'
        )

    return zoom, center

# --- STATISTIC PLOT METHODS ---

# Display catalogs statistics
@app.callback(Output("tab-content", "children"),
              [Input("tabs", "active_tab"),
               Input('crossfilter-xaxis-type', 'value'),
               Input({'type':"store_stat", 'index':ALL}, "data"),
               Input('cat_source_switch', 'on')])
def render_tab_content(active_tab, xaxis_type, data, switch_on):
    """
    This callback takes the 'active_tab' property as input, as well as the
    stored graphs, and renders the tab content depending on what the value of
    'active_tab' is.
    """
    if active_tab :
        if data[-1] is not None:    # Selected sites
            dataset = data[-1]
        elif data[0] is not None:   # No selected sites
            dataset = data[0]
        else:
            return None

        if switch_on:
            for k in dataset['sites']:
                k['color'] = scrape._portal_color[k['catalog']]
        else:
            for k in dataset['sites']:
                k['color'] = 'dimgrey'

        if active_tab == "sites":
            if xaxis_type == 'Linear':
                return dcc.Graph(figure=plot_stat_sites(dataset['sites']), style={"height": "100%", "width": "100%"})
            else:
                return dcc.Graph(figure=plot_stat_sites(dataset['sites'], logscale=True), style={"height": "100%", "width": "100%"})
        if active_tab == "sel":
            if xaxis_type == 'Linear':
                if data[-1] is not None:
                    return dcc.Graph(figure=plot_stat_sites(dataset['sites'], select_only=True),
                                     style={"height": "100%", "width": "100%"})
                else:
                    return html.P(children='No selected sites to show',
                                   style={'font-size':'90%', 'text-align':'center',
                                          'color':'black', 'margin-top':'5rem'})
            else:
                if data[-1] is not None:
                    return dcc.Graph(figure=plot_stat_sites(dataset['sites'], select_only=True, logscale=True),
                                     style={"height": "100%", "width": "100%"})
                else:
                    return html.P(children='No selected sites to show',
                                   style={'font-size':'90%', 'text-align':'center',
                                          'color':'black', 'margin-top':'5rem'})

    return None


# Function to describe the sites statitics tab
def plot_stat_sites(data, logscale=False, select_only=False):
    df = pd.DataFrame(data=data).sort_values('catalog')
    if select_only:
        fig_sites = go.Figure(
            [go.Bar(y=df.catalog, x=df.len_selected, name='Selected sites',
                    orientation='h', marker=dict(color=df.color),
                    showlegend=False)])
        fig_sites.update_layout(margin=dict(l=2, r=2, t=10, b=2),
                                barmode='overlay',
                                xaxis=dict(title_text="#Selected Sites"),
                                )
    else:
        fig_sites = go.Figure(
            [go.Bar(y=df.catalog, x=df.len_tot, name='Original sites',
                    orientation='h',marker=dict(color=df.color), showlegend=False),
             go.Bar(y=df.catalog, x=df.len_selected, name='Selected sites',
                    orientation='h', marker=dict(color='#008cba'),
                    showlegend=[False if all(df.len_selected==0) else True][0])])
        fig_sites.update_layout(margin=dict(l=2, r=2, t=10, b=2),
                                barmode='overlay',
                                xaxis=dict(title_text="#Sites"),
                                legend=dict(orientation="h",yanchor="bottom",xanchor="right",y=-0.4,x=1))
    fig_sites.update_yaxes(automargin=True)
    if logscale:
        fig_sites.update_xaxes(type='log')
        if select_only:
            fig_sites.update_layout(xaxis=dict(title_text="#Selected Sites (log. scale)"))
        else:
            fig_sites.update_layout(xaxis=dict(title_text="#Sites (log. scale)"))

    return fig_sites

# --- METHOD TO HANDLE WITH TABLE DATA ---

# Display table values
@app.callback([Output("catalog_table", "columns"),
               Output("catalog_table", "data"),
               Output("site_loaded_info", "children")],
              Input({"type":"store_table", "index": ALL}, "data"),
              prevent_initial_call=True)
def render_table_values(data):
    # Starting testing that data are loaded
    if data[0] is not None:
        cat_index = [i for i,n in enumerate(data[0]) if n['name']=='Cat'][0]
        data[0][cat_index]['name'] = 'Original catalog'
        # Test if a selected dataset exist
        if data[2] is not None:
            dataset = data[2]
        # Else display all the loading dataset
        elif data[1] is not None:
            dataset = data[1]
        else:
            return None, None, "No site loaded"
        return data[0][2:], dataset, f"{len(dataset)} site(s) loaded"

    else:
        return None, None, "No site loaded"

@app.callback(
    Output("download-table", "data"),
    [Input("export_button", "n_clicks"),
     State({"type":"store_table", "index":ALL}, "data")],
    prevent_initial_call=True,
)
def save_table(n_clicks, data_table):
    if n_clicks:
        if data_table[2] is not None:
            return dcc.send_data_frame(pd.DataFrame(data_table[2]).to_csv, "tgcat_table.csv")
        elif data_table[1] is not None:
            return dcc.send_data_frame(pd.DataFrame(data_table[1]).to_csv, "tgcat_table.csv")
        else:
            return None
    else:
        return dash.no_update

# --- METHOD TO UPDATE CATALOGS PICKLES ---

# Update pickles files from selected dataportals
@app.long_callback(
    output=[Output("store_update_output", "data"),
     Output("update_select_button", "n_clicks"),
     Output("update_all_button", "n_clicks")],
    inputs=[Input("update_select_button", "n_clicks"),
     Input("update_all_button", "n_clicks"),
     State("metacatalog_check", "value"),
     State("datacatalog_check", "value"),
     State("nationalcatalog_check", "value")],
    running=[(Output("update_select_button", "children"),[dbc.Spinner(size="sm")],"Update selected"),
             (Output("update_select_button", "disabled"), True, False),
             (Output("update_all_button", "children"),[dbc.Spinner(size="sm")],"Update all"),
             (Output("update_all_button", "disabled"), True, False)],
    prevent_initial_call=True)
def update_catalogs(n_selct, n_all, meta_vals, data_vals, nat_vals):

    if not n_selct and not n_all:
        # Return nothing when no clicking
        return None, None, None

    if n_selct:
        # Load catalogs names
        concat = np.append(meta_vals, data_vals)
        concat = np.append(concat, nat_vals)
        value = concat[concat != None]

        if len(value) == 0:
            # Return nothing when no clicking
            return None, None, None

        elif len(value) != 0:
            for i,v in enumerate(value):
                cat = getattr(scrape, v.split('-')[0])(save=True, rq=v.split('-')[-1],
                                                       update=True, precise_lonlat=True, complete_scrape=True)

        return 'Older/newer archive : '+ display_archives_date(), None, None

    elif n_all:

        # Load catalogs names
        value = [d['function_name'] for d in _catalog_infos if 'function_name' in d.keys()]

        if len(value) == 0:
            # Return nothing when no clicking
            return None, None, None

        elif len(value) != 0:
            for i, v in enumerate(value):
                cat = getattr(scrape, v.split('-')[0])(save=True, rq=v.split('-')[-1],
                                                       update=True, precise_lonlat=True, complete_scrape=True)

        return 'Older/newer archive : ' + display_archives_date(), None, None

# Display and update last archive date
@app.callback(Output('archive_date', 'children'),
              [Input("store_update_output", "data")])
def update_archive_date_text(data):
    if data is None :
        return 'Older/newer archive : '+ display_archives_date()
    return data
@app.callback(
    Output("tg_dropdown", "options"),
    Input("metacatalog_check", "value"),
    Input("datacatalog_check", "value"),
    Input("nationalcatalog_check", "value"))
def update_options(meta_vals, data_vals, nat_vals):

    concat = np.append(meta_vals, data_vals)  # Load catalogs names
    concat = np.append(concat, nat_vals)
    value = concat[concat != None]
    list_path = []
    for cat in value:
        list_file = [file.iterdir() for file in _connectivity_path.iterdir() if file.name.startswith(cat)]
        for file in list_file:
            list_path.append(list(file)[0])
    pt = []
    nm = []
    ky = []
    for path in list_path:
        connectivity_table = pd.read_pickle(path)
        keys = [i for i in list(connectivity_table.keys()) if len(i.split('f')) == 7]
        new_connectivity_table = {key: connectivity_table[key] for key in keys}
        for key in list(new_connectivity_table.keys()):
            if len(list(new_connectivity_table[key].values())[0]) > 1:
                for i in range(len(list(new_connectivity_table[key].values())[0]) - 1):
                    ky.append(key)
            pt = pt + list(new_connectivity_table[key].values())[0]
            nm = nm + list(new_connectivity_table[key].values())[1]
            ky.append(key)
    d = {'keys': ky, 'names': nm, 'paths': pt}
    df = pd.DataFrame(data=d)
    df = df.drop_duplicates(subset=['keys', 'names'], keep='last')
    _zoom_TG = {}
    for index, row in df.iterrows():
        a = row.names
        p = row['keys']
        _zoom_TG[a] = p
    return [x.capitalize() for x in list(_zoom_TG.keys())]

# --- POINT(S) SELCTION ---
@app.callback(
    [Output({'type':"store_stat", 'index':'selected'}, "data"),
     Output({'type':"store_table", 'index':'selected'}, "data"),
     Output("alert-error-country", 'is_open'),
     Output("alert-error-country", 'children')],
    [Input("map", "selectedData"),
     Input("region_dropdown", "value"),
     Input("country_dropdown", "value"),
     Input("tg_dropdown", 'value'),
     State({'type':"store_stat", 'index':'loading'}, "data"),
     State({'type':"store_table", 'index':'loading'}, "data"),
     # Input('update_sel_button', 'n_clicks'),
     # Input('catalog_table', 'filter_query'),
     Input('catalog_table', 'selected_row_ids')],
    prevent_initial_call=True
)
def selection(sel_data, sel_region, sel_country, sel_tg, stat_data, table_data, row_table):

    if table_data is not None:
        df_all_data = pd.DataFrame(table_data)
    else :
        return dash.no_update
    # 1. Table selection
    if row_table is not None:
        df_selected_data = df_all_data[df_all_data.index == row_table[0]]

    # 2. Geographic selection
    if sel_data is not None:
        # Transform point selected to dataframe
        df_selected_points = pd.DataFrame(sel_data['points'])[['lon', 'lat', 'text']]
        # Select points that are in the map selection
        df_selected_data = df_all_data[(df_all_data['longitude'].isin(df_selected_points['lon'])) &
                                        (df_all_data['latitude'].isin(df_selected_points['lat'])) &
                                        (df_all_data['name'].isin(df_selected_points['text']))]


    if sel_region is not None:
        gdf_all_data = gpd.GeoDataFrame(df_all_data,
                                        geometry=gpd.points_from_xy(df_all_data['longitude'],
                                                                    df_all_data['latitude']))
        polygon = _zoom_region[sel_region]
        selection_mask = gdf_all_data.within(polygon).values
        df_selected_data = df_all_data[selection_mask][df_all_data.columns]


    if sel_country is not None:
        gdf_all_data = gpd.GeoDataFrame(df_all_data,
                                        geometry=gpd.points_from_xy(df_all_data['longitude'],
                                                                    df_all_data['latitude']))
        polygon = _zoom_country[sel_country]
        selection_mask = gdf_all_data.within(polygon).values
        df_selected_data = df_all_data[selection_mask][df_all_data.columns]
        if len(df_selected_data) == 0:
            text = f"WARNING : No tide gauge in {sel_country}"
            return None, None, True, text


    if sel_tg is not None:
        if len(sel_tg) == 0:
            return dash.no_update
        else:
            reduce_letter = pd.DataFrame(columns=['Sel TG'], data=np.array(sel_tg))
            reduce_letter = reduce_letter['Sel TG'].str.lower()
            df_selected_data = df_all_data[df_all_data['name'].isin(list(reduce_letter.values))]

    if sel_data is None and sel_region is None and sel_country is None and sel_tg is None and row_table is None:
        return dash.no_update


    # 3. Update statistics tables with data selection
    stat_data_updated = {}
    for k, v in stat_data.items():
        if k == 'sites' :   # 1. For #Site statistics, count total VS selected sites
            # Count selected sites
            cat_count = pd.DataFrame(pd.value_counts(df_selected_data['cat'].values).rename('len_selected'))
            cat_count['catalog'] = cat_count.index
            # Update Stat dict
            df = pd.DataFrame(data=v).sort_values('catalog')
            df = df.merge(cat_count, how='left', on=['catalog'],
                          suffixes=['_ol',''])[['catalog','len_tot','len_selected']]
            stat_data_updated['sites'] = df.to_dict(orient='records')

        elif k == "link" : # 2. For #Link statistics (to implement)
            stat_data_updated['link'] = v

    return stat_data_updated, df_selected_data.to_dict('records'), False, None

@app.callback(
        Output("modal-fs", "is_open"),
        Input("display_analyse_button", "n_clicks"),
        State("modal-fs", "is_open"),
    )
def toggle_extralarge_modal(n, is_open):
    if n:
        return not is_open
    return is_open

# -------- Run --------

if __name__ == '__main__':
    app.run_server(debug=True)