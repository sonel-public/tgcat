"""
This code allow to modify the top pannel of the dashboard
"""

from dash import dcc, html
import dash_bootstrap_components as dbc

# -------- Navigation bar --------

navbar_text = '''
To support the progress of scientific knowledge on ocean climate, marine ecosystems and their vulnerability,
[EuroSea](https://eurosea.eu/) works to improve the European ocean observing and forecasting system.

&nbsp;

This dashboard was created in the framework of the 
[EuroGOOS Tide Gauge Task Team](http://eurogoos.eu/tide-gauge-task-team/) 
for [EuroGOOS](https://eurogoos.eu/) and [EOOS (European Ocean Observing System)](https://www.eoos-ocean.eu/), 
to support tide gauge data portals interoperability and allow data providers 
and integrators to identify problems like duplicates or wrong metadata. 
'''

EUROGOOS_LOGO = "https://envri.eu/wp-content/uploads/2019/11/EuroGOOS-Logo-Standard.png"
SONEL_LOGO = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQYAAADACAMAAADRLT0TAAAA/1BMVEX////BghJIvOO8dwC9eQC/fgCkssn8/Pzb4Om+fADAgAb7+PTv49Px5ti/fQDWsYDt3crKk0Tm0LbYs4bEgAD28OjFjDI4uOI9v+vHfgDQomPTrHX3+Pro1L6otsu2wtPQ1+LiyKnJ0d7r7vLZ3+fBytnChyTNnFnKmFDdvZft7/LUrXrRpWvFjzu4bADV7PezvtGs3O+K0Opoxebl8/m94/KEpaFdtc+c1+1QutsXs+DL6fSmwtd/uNS24PJ8zOl7qa+0ikNrr76amX2mk2fFv7yhlW2BpqRmsseTnYmrkF2xjUuuj1bBwK7UxLKwoJCwp6Hb2di5lWmssb3Cm2dYyA6UAAAPbklEQVR4nO2dCXfauhLHbeQFm0AIBMjCYrOTkJQsbdPb97rfpXdf3vf/LE9eNbYl2QRj3Hv0P/f0lsVY+nk0Go3GriQJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJPV/9QzegFHp6cegWlEGP3wkMWJWewCBJ7wUGrLtepfd46Ebkpf7VZGqNxzOs8diaVidZnf/bXuXfgeGqOh5probD0cb5z3uxGVevUg9+gSlUenoBzdyn+tWZ0+OZhS+/25eq5r0/qVozB8doykXx6FCo9Apo6f7UtzaathlH7N/H4EmfWNhOhhaThEeh8rC/Nu5dVdzDWTXuAiIYHOlVbBWbKdXs33oUKu/308L9q2/hvlUpfUtgcN/EI2ecNIknn0LvLv8GFqH+TBuO6VMBFYOHbTaJvvXgU6j03ubewgLUH+PhzvLtDAzOJxgEsIi7AALG8C2urCwMgf0pG4MHwsf3rkIoVCq5tq8QTYbamPc5D4OLsIpjhfsehFC5lzqdfFu5Z+kzbcS3YD4GSf/Pfx8ihuCMiZdSt55nK/etydC5mFwxMTy+eHt3/9CLIfCCp9Xtt4RhnGYKUgxDH/f95d3T/XtsAY6SCNyo4Qgp3w6G/kaz0r9V1XDX392FPWd2HoyJU1P5ZnzD1XDIWx3oj29fPt0/ON1K73lEZzeqjL4VDFVtwwgV+t6Y37b3IYUP57KMasX25rmaajPa2/2X95Vndj/E8PEUYyi6P8+TRQsWHu+ojn9LfcLGYM6L79IzRKPwMg8GjjGYsqwuDtCprWUlpoj+Uy4MsBzPIDfah+jWlprGbUHPDULlTMbGIButw/RsG03i3vFdbhAqZ5+xf5TlbyB66g9HkdeP8SXBLnL8o/xNTBTDTeTlXY4QKhXTGRKyujxQ37JrpsFlhP6QJwU3ZHA85Opg3csoS4OZs8ccGWAKXzwKMiq7a7iKTBIvch0QZ589xyCb5sH6l1Eb6Bje7odC+aMGSwOLypwpfAkoyI3vD9fDLOrD6DHfEfHG947OmOhqE2YTyqDRkPz9Mdcp4tOpGVDAISQ8T/lUhVcpTwiVH8IB4TrIfpa01sEEw8eH/CCcvTo9JRRkw3aSnOXd2a8C/5hb7Hh29qELTAEbg4x/XufvfRxUQ7KiyssxnFVezc9NSEFWBs4JLK2s+3fQGHIZEtgQvpyfRiEECZfymgMwhpcJYziLKB3A2dnrH7+cn5/KcQW5WKuk3mECpoletE+Vn378+sPPv3z8+PGXn3/4/PXVh0+vK1Qi/nuvP3z9WaYxwBSCfEtZJ4sRCaOfAIazN69+xT3CMrFOXZ1jnd78+sUl8umn128cvX7906cPr75+/rXrfBofC8EscR2eZFbK2KGvTcO/gkKE97+dn9KuKvb4BAmRi4v6dc8xnJATTkoZSoKx+hRCeHhsI3antpYaScsPqRshBxZoVFia81JaK7lSiGxVWfyKgIPoSgs38N/1fFPoSwsjRwqNbnRu6GtpNQPFC1yaN75XkKSLPCkoiR2aTflGBWmSX8Z6L0mXeVJAyfRj+UYFmCfc2bL3JEmrHL2jIVOyj+WbK6okxA9soZkfBRXRs26li6DGYSzj5pywX6jlNkeo6IJR0zEa0d8/mIhruHOsAf//RM0HQgNdMLPxZXMOOnEND14Nq52LMaiKuuJU90y09NsvihRxVnrPLWiuURyD6So7AgOpx03uafWSRQ7TMJLGrgG7R+kiMiRM1VCQIndvTm7msoGQohhGQ2UhMVXn6/LaTt+aGpYr6WCFHvKu5ziGI2AMpoHmx606iINr9WbLbq+Xc8UlcoqZODKwFPzWfHFsN7NVeI3KFUDNQpf93r1R9Ca80KYic0a33qkf/f6Hba9c2a1Bs75Vhdu4XIvtTWic7pBohv5R6Q7Sjk2pmeZqWq6pgtwo8J3zx4kZeLkMtTm7YKiWKzEbzpeP7/Afdd8zKIssFr4LhnLNmGTJ616cS2+a8BdD/ZTUKR+Dzr3cV6VaVfQjrdE9Y0C+V0hbDqfdVpL9xIdW9KLY7vo6rFXbDQPfCZYrfsIY9FDS0vQo+C8xBp0jJ/TifGrxPtXB+r4Euhp6N1m7+qfhUPgzfA0/o4n/ecrRwzKNiquhVQ31O3aQyCavN6MqV+Mh79MZ99PpcFaiGTPiG9aqbByDD9MC3l18g1Qq3xBx2EZkU2W/GMo1U8BUeV2RlUjUtM8Js1xxA8i6SC0DRZcRVord8jFMuEvpckWRYE0hHSvr7Y79F60pyApTOjG2rDvYDcPzj92DQIr4NnVlHdMuXbHKlW8g6Y/azbbH7oJhVq4MPclFHm1d274Lhv3mIjtHA3u1al9fX15eH7dXdqvZ4Q/4rTx2fE0xZP42/BrtWGeGoi9FXNXlrq/5trXmHXs9R8jJHDdUT06uVFGQfMHLFOPAod5sYXLri4sF1sX6eGUP6InFWxSRcopYdxPOwy/der9Ujx17qiCaZO/wI2T6ioS16bLnyFDpeXNTNZC8ol+4uv1RRgigc/EZXpo9kZBN7nOz7hI5CZuCvBPXM20CmV0fQ/htdRsMLcXg76aYhpJMLtaPG4h9HIbXtdMwsG4uPQiGTGVK8TKDzgKlbVaahhEBQal6YJjDITDY2Tbio6FyKxWCd9AFHwPjTroDYKDtOlLPoUYoZDtINkA/aTUw6KgkGI4bWXtELDwrOiyFDCYaBvOE1qYDYIj2CM8LeJZ0piP8p9EwKSeRwkx8JpHnTlArohBt77p4DAN4AgMt2/bgqN5xVG/a7RsEQIQdShiD6cUZ7rwZ3682QnMgGABFqjnwMKio0aAGDSBu2B5DGzRJsRPhQV0GtzoFTtKG11U9VZTu4rLddjZl28frpYwUaEWmnMCgtufkCzRz4GBQ29ImbUHxDAwnoEG02atOrnx4XcExKlr/8VfsEP1oNQegwp8N32usAEiTsibjYkivDn0GBmBt9Ph7GTZJ9avYwZhQlzX6xQGzsBrQI9ZwDD2SklyhczGkb+pvjwF0ifFoBHLhVD8IaIbvuE9aqVKXV6twAjKDOZP80KV0TMYicL2BuBh4j9vztD2GDsHAeDQCaUHQH9JDbw6lVrRDvv5bkGeHaw48DH8PU/Nv22MAnWQ8Q4f0J8BA6ps8A6LfBUYKX4InVhEMeCmxAOaQODMXQ3q9z04YkrbpCYVLR39uI27ev87Uu8CI2Sv+XEDG0jJaJZVYt3ExpKfm94LhIpT/myh+CNUciE8Jwk+CweHZJdNNwhx4GK6lVO2CQZbTv+2KDOvAZ1K9QxNO9a4IBgcfjNvi5sCPG1K1E4asT4+hnGRK2UQCLfdpRTFIINw0Y5egcAxwpsj47L0BCIOC9zabxNfILwfxcnSelVYgxIomJorHAOKYWFOYIoNeCY+YJCM7nWDwh34MwxSMipg5FI9BBm3JVqEJwgYy4c+SXpL00p9QYhiqf4PlTPQaFI8Bzt9yhmplvBgLMShkVaQPEyE1sXkUe8O79tV/QKYjktQ5AIYVbIuhoPnF8ao1OOp0mKZxnQgIHE0Sd7yANZvXl7g1aGuWOXAnzBpL4Z7KMzAcxXIHppdhdxIvxny5btvNuOdcx4JIX+P4k1NJlOUnKkC+wXk51Tow19HIhgGvaVm6Dabd52Sf5FiaBCIxMRIFKYsV7G88lg60ie1CddkYTGnjFnl9BOc2QOr7edknZRcMGW6HMRuKTGx2QWLpiKHEn4yXWFRADNWpUwL2FzRF46AYpG6WxKJpNAJHsEysmXzFnpPIxSB5W7kR70DM4RAYamamBKsZZMtOWBjwaLfAq8T3KBjgeltWdMqhhWGQ9DVqsB0EOI03xJMr6FAWLF7NggFMO9AcEhiOCsCAL0pbZm7lEvlPZONgwFEUiSazDIpokjuspItjmPwJHgVhsoR2xeCQaF3fKO7GfjLHHjYzFQPmENrDDW+mCDCAYIya83UxTDSAYX7C0k0Q1O6AwWNxNLDbl4uTuTs5K0rMQrxwiYsBc7C2wlCjeYcohok22nsUyRIOypz9mi7csvWawHaRrsZBLmoeHsfDQDWHCIapNtp/MJ2mzhq03k1GMidMX5Y/b5LexdcUEQx6ZNvM+yY4BcY6K2BNkS4wt7tB8IIRRYaqahtnucleWkUw0PIOBENj5A6yEmCI5/AZawqgK/efKiFX2X/0KQODBLfUvYUmwDB081olwABWHe4gBytManEClj7Sxpy0SwwDDOe9TA4xOOSlMcqAgaylXGcA8g3swlhL+4c89NPf32BhkMB85KUiAIZi8g2D9aWvNTMVCS6/8x1aEi6pq+9DWqpfU87EEDEHB+1F0RjscDOG/QjnGIYWJSVLESdBH8cAF/vuBnfxGMilzYDBHRSgf5eZfjmxXZPA0ILm0ITVNMVjYPk7MDW4LhJk3nnl4qR+JHAhbAwgU+V4ksn/isZALNxg+jsQ07mvUzfBY0cFdsbBENnC+l77u3AXGR7BHujxTDtZLCjsZ4+AtUJjOHYnfw6GyI7mL4/tojGArVxWHTf5UX/rlvgKlX1fERltZtcaakNrwsXQBOaAaiRdXhCGWvrm3Tre6xYIk1nm0CGhoZOmmIwxCbJYTmIAJoZ7ShjuiMFcDFosRbpLmmvSC3fBpO4vACOVKm3afoZuK6RXvoecjMGpIAb9yvmnk/8EIbWa20zh1G0zFC1+Bo/mUc12fFOibp/E5jJHYCDLDXTTbjXDzR29Vh+slghWDKIgxQg6upnNxrPheDYbebdij8bVJfhRNX5wpn2K8O6LTCm76JQQrXFUEFK7J87dIReL5Y2MoncYINox3lONkHtLiltdG62tJUUQ0DeMR6MN1mg0G1v+P6tepxYgb1UlGyQ2noEhWf5smm7RazIVF0ZLOrXFrKY1kxgiG5aBFrQEeVEYtqh/JuH2ykj/ti9QS5SCgWoOhWHIXA2vgnsj5oykLaVlJEZPwRB7gph/eFEYpEE2DqoJtig7ajYbIknzDBg6lIYUhyHbnTLGPDKJ1E6UDAZBdvyyYIhs5QUdKw6D1Lng3DsmO3OBkgy1W3OFT09VjOhBqRgo5vA8DLdKuhBlDVWzl85MF58d/J19mf7cwua1gQyVstnlHIUal/HzgHsp6Rik62Qk4G/e3SY+4cQNtWYWMYLmemt1uZyD6ERRu8v1yj7ilETVW+2F7D7vL4SMUONmvRpQTgIKU1i/lyxh8d7XmSUutG/no61/0tnZ8VXv1Mr57yYICQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQmVXP8H14VUQ5BEWS4AAAAASUVORK5CYII="
EUROSEA_LOGO = "https://eurosea.eu/wp-content/uploads/2020/06/EUROSEA.png"

title = html.H2(
        children='Tide Gauge Portals Intercomparison',
        style={'font-weight':'bold', 'textAlign': 'center', 'margin-top':'1.5rem', 'margin-bottom':'1.5rem'})

logo =  dbc.Row([
            # Eurosea logo
            dbc.Col([
                html.A(dbc.Row([dbc.Col(html.Img(src=EUROSEA_LOGO, height="40px"))], align="center", class_name="g-0"),
                       href="https://eurosea.eu/", style={"textDecoration": "none"})
            ]),
            # Eurogoos logo
            dbc.Col([
                html.A(dbc.Row([dbc.Col(html.Img(src=EUROGOOS_LOGO, height="50px"))],align="center", class_name="g-0"),
                       href="https://eurogoos.eu/", style={"textDecoration": "none"})
            ]),
            # Sonel logo
            dbc.Col([
                html.A(dbc.Row([dbc.Col(html.Img(src=SONEL_LOGO, height="50px"))], align="center", class_name="g-0"),
                       href="https://www.sonel.org/?lang=en", style={"textDecoration": "none"})
            ]),
        ])

# Eurogoos button
info_button = dbc.Button("More about the project ?", id="openProject", class_name="btn-outline-info btn-sm")
info_modal = dbc.Modal([dbc.ModalHeader("TGPI project", style={'font-weight': 'bold', 'font-size':'22px', 'textAlign': 'center'}),
                   dbc.ModalBody(dcc.Markdown(children=navbar_text, style={'textAlign': 'justify'})),
                   dbc.ModalFooter(
                       dbc.Button("Close", id="closeProject", class_name="ml-auto"))],
                  id="modalProject")

navbar = dbc.Navbar(
    dbc.Container([
            dbc.Col(logo, xs=12, sm=12, md=12, lg=3, xl=3,  style={'textAlign': 'center'}),
            dbc.Col(title, xs=12, sm=12, md=12, lg=6, xl=6),
            dbc.Col([info_button, info_modal], xs=12, sm=12, md=12, lg=3, xl=3, style={'textAlign': 'center'}),
        ], fluid=True,
    ),color="white",dark=False)


alert = dbc.Row([
        dbc.Col([
            dbc.Alert("There is an error !",id="alert-error",is_open=False, color='danger'),
            dbc.Alert("There is an error !",id="alert-error-country",is_open=False, color='danger')
        ], xs=12, sm=12, md=12, lg=12, xl=12,

            # style={'padding-top':'0'}
    )])


# ------- Header definition --------

header = dbc.Container([navbar, alert], style={'vertical-align':'middle', 'padding-top':'10px'}, fluid=True)

