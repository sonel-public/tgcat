import numpy as np
import h3
import plotly.graph_objs as go
import plotly.express as px
import plotly.io as pio
from tgcat.core.generic import Catalog

pio.renderers.default = 'browser'

mapbox_access_token = 'pk.eyJ1IjoibHRlc3R1dCIsImEiOiJjbDMxcm9icDYwN2Q3M2ltdzg0NjduYjgwIn0.CTvWZHfB7bf5hUWRYZJ4QQ'


def map_catalog(cat):
    fig = px.scatter_mapbox(cat.to_dafr)


def h3index_to_map(h3index=[], resolution=0):
    "plot a the cell boundary on a scattermapbox"

    # Get the coordinates of the hexagon vertices
    traces = []
    for index in h3index:
        if h3.get_resolution(index) == resolution:
            vertices = h3.cell_to_boundary(index, geo_json=True)
            latitudes = [v[1] for v in vertices]
            longitudes = [v[0] for v in vertices]
            trace = go.Scattermapbox(lat=latitudes, lon=longitudes, mode='lines+markers', line=dict(width=2),
                             name=f"{index}@res={resolution}")
            traces.append(trace)
    # Create the layout for the figure
    layout = go.Layout(
        mapbox=dict(
            center=dict(lat=np.mean(latitudes), lon=np.mean(longitudes)),
            style='open-street-map',
            accesstoken=mapbox_access_token,
        ))
    # Create the Figure object and add the trace
    fig = go.Figure(data=traces, layout=layout)
    # Show the figure
    return fig

def chrono2choropleth_mapbox(connectivity):
    # Aggregate data by H3 index and calculate the mean value for each index
    agg_data = df.groupby('h3_index')['value'].mean().reset_index()

    # Create the choropleth map using Plotly Express and Mapbox
    fig = px.choropleth_mapbox(
        agg_data,
        geojson=h3.h3_set_to_multi_polygon([agg_data['h3_index'].tolist()]),
        locations='h3_index',
        color='value',
        color_continuous_scale='Viridis',  # You can choose any color scale you prefer
        range_color=(df['value'].min(), df['value'].max()),  # Customize the color range
        mapbox_style='carto-positron',  # You can choose any Mapbox style
        zoom=10,  # Adjust the initial zoom level
        center={'lat': df['latitude'].mean(), 'lon': df['longitude'].mean()},  # Adjust the initial center
        opacity=0.7,
        labels={'value': 'Mean Value'}
    )

    return fig
