
# --- Color for the scrapped catalogs ---
# ---------------------------------------
# TODO find an automatic name of RGB code for catalog
_portal_color = {'ssc': 'dimgray',
                 'slsmf': 'black',
                 'uhslc_fd': 'royalblue',
                 'uhslc_rq': 'blue',
                 'psmsl': 'green',
                 'psmsl_rlr': 'green',
                 'psmsl_metric': 'green',
                 'psmsl_links': 'lightgreen',
                 'psmsl_gnssir': 'darkgreen',
                 'emodnet': 'gold',
                 'cmems': 'salmon',
                 'bodc': 'khaki',
                 'ptwc': 'violet',
                 'eutgn': 'pink',
                 'ilico': 'brown',
                 'refmar': 'yellow',
                 'gesla': 'orange',
                 'cantgn': 'darkviolet',
                 'gloss': 'mediumvioletred',
                 'sonel': 'darkorange',
                 'noaa': 'slateblue',
                 'norwtgn': 'teal',
                 'redmar': 'lemonchiffon',
                 'indonesian': 'plum',
                 'misela': 'magenta'}

# --- Metadata for the scrapped catalogs ---
# ------------------------------------------

# If type == meta : Metadata catalog (no data informations)
# If type == data : Data catalog (data informations)

ssc_infos = {'sn': 'SSC',
             'ln': 'IOC/Unesco Sea Level Station Catalog',
             'function_name': 'ssc',
             'link': 'http://www.ioc-sealevelmonitoring.org/ssc/index.php',
             'type': 'meta'}
psml_ell_infos = {'sn': 'PSMSL Ellipsoïd',
                  'ln': 'Permanent Service for Mean Sea Level - Tide gauge linked to a geocentric ellipsoid',
                  'function_name': 'psmsl_links',
                  'link': 'https://www.psmsl.org/data/obtaining/ellipsoidal_links.json',
                  'type': 'meta'}
eutgn_infos = {'sn': 'EUTGN',
               'ln': 'European and adjacent areas Tide Gauge Network Inventory',
               'function_name': 'eutgn',
               'link': 'http://eutgn.marine.ie/geonetwork/srv/fre/catalog.search#/search',
               'type': 'meta'}
ptwc_infos = {'sn': 'PTWC',
              'ln': 'Pacific Tsunami Warning Center',
              'function_name': 'ptwc',
              'link': 'https://www.tsunami.gov/',
              'type': 'meta'}
gloss_infos = {'sn': 'GLOSS',
               'ln': 'Global Sea Level Observing System',
               'function_name': 'gloss',
               'link': 'https://gloss-sealevel.org/',
               'type': 'meta'}
ilico_infos = {'sn': 'ILICO',
               'ln': 'Infrastructure de Recherche Littorale et Côtière',
               'function_name': 'ilico',
               'link': 'https://www.ir-ilico.fr/',
               'type': 'meta'}
cmems_infos = {'sn': 'CMEMS',
               'ln': 'Copernicus Marine In Situ TAC',
               'chrono': 'To complete',
               'function_name': 'cmems',
               'link': 'http://www.marineinsitu.eu/dashboard/',
               'type': 'meta'}

psmsl_infos = {'sn': 'PSMSL',
               'ln': 'Permanent Service for Mean Sea Level',
               'chrono': '31 days if monthly else 366' ,
               'function_name': 'psmsl_rlr',
               'link': 'https://www.psmsl.org/data/obtaining/',
               'type': 'data'}
psmsl_met_infos = {'sn': 'PSMSL Metric',
               'ln': 'Permanent Service for Mean Sea Level',
               'chrono': '31 days ' ,
               'function_name': 'psmsl_metric',
               'link': 'https://psmsl.org/data/obtaining/map/data/metricMarkerList.json',
               'type': 'data'}
psmsl_gnssir_infos = {'sn': 'PSMSL GNSS IR',
                      'ln': 'Permanent Service for Mean Sea Level - GNSS Interferometric Reflectometry',
                      'chrono': '6 hours' ,
                      'function_name': 'psmsl_gnssir',
                      'link': 'https://psmsl.org/data/gnssir/table.php',
                      'type': 'data'}
misela_infos = {'sn': 'MISELA',
                      'ln': 'Minute Sea-Level Analysis',
                      'chrono': 'To complete',
                      'function_name': 'misela',
                      'link': 'https://www.vliz.be/en/imis?dasid=6673&doiid=457',
                      'type': 'data'}
uhslc_fd_infos = {'sn': 'UHSLC FD',
                  'ln': 'University of Hawaii Sea Level Center - Fast Delivery data',
                  'chrono': 'To complete',
                  'function_name': 'uhslc-fd',
                  'link': 'http://uhslc.soest.hawaii.edu/data/',
                  'type': 'data'}
uhslc_rq_infos = {'sn': 'UHSLC RQ',
                  'ln': 'University of Hawaii Sea Level Center - Research Quality data',
                  'chrono': 'To complete',
                  'function_name': 'uhslc-rq',
                  'link': 'http://uhslc.soest.hawaii.edu/data/',
                  'type': 'data'}
slsmf_infos = {'sn': 'SLSMF',
               'ln': 'Sea Level Station Monitoring Facilities',
               'chrono': 'To complete',
               'function_name': 'slsmf',
               'link': 'http://www.ioc-sealevelmonitoring.org/list.php?operator=&showall=all&output=contacts#',
               'type': 'data'}
bodc_infos = {'sn': 'BODC',
              'ln': 'British Oceanography Data Centre',
              'chrono': 'To complete',
              'function_name': 'bodc',
              'link': 'https://www.bodc.ac.uk/data/hosted_data_systems/sea_level/international/',
              'type': 'data'}
cmems_nrt_infos = {'sn': 'CMEMS NRT',
               'ln': 'Copernicus Marine In Situ TAC Near RealTime',
               'chrono': 'To complete',
               'function_name': 'cmems_nrt',
               'link': 'https://www.seanoe.org/data/00825/93670/',
               'type': 'data'}
gesla_infos = {'sn': 'GESLA-3',
               'ln': 'Global Extreme Sea Level Analysis',
               'chrono': 'To complete',
               'function_name': 'gesla',
               'link': 'https://gesla787883612.wordpress.com/downloads/',
               'type': 'data'}
emodnet_infos = {'sn': 'EMODNET',
                 'ln': 'European Marine Observation and Data Network',
                 'chrono': 'To complete',
                 'function_name': 'emodnet',
                 'link': 'https://map.emodnet-physics.eu/',
                 'type': 'data'}
sonel_infos = {'sn': 'SONEL',
               'ln': "Système d'Observation du Niveau des Eaux Littorales",
               'chrono': 'To complete',
               'function_name': 'sonel',
               'link': 'https://www.sonel.org/?lang=en',
               'type': 'data'}

refmar_infos = {'sn': 'REFMAR',
                'ln': 'Réseau de Référence des observation Marégraphiques (SHOM)',
                'chrono': 'To complete',
                'function_name': 'refmar',
                'link': 'https://services.data.shom.fr/maregraphie/service/tidegauges',
                'type': 'national'}
redmar_infos = {'sn': 'REDMAR',
                'ln': "Spanish Tide Gauge Network (Puertos del Estado)",
                'chrono': 'To complete',
                'function_name': 'redmar',
                'link': 'https://www.puertos.es/en-us/oceanografia/Pages/portus.aspx',
                'type': 'national'}
canada_infos = {'sn': 'Canadian TGN',
                'ln': 'Canadian Station Inventory',
                'chrono': 'To complete',
                'function_name': 'cantgn',
                'link': 'https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/maps-cartes/inventory-inventaire-eng.asp?user=isdm-gdsi&region=MEDS&tst=1&perm=1',
                'type': 'national'}
indonesian_infos = {'sn': 'Indonesian TGN',
                'ln': 'Indonesian Tide Gauge Inventory',
                'chrono': 'To complete',
                'function_name': 'indonesian',
                'link': 'big.go.id',
                'type': 'national'}

noaa_infos = {'sn': 'NOAA',
              'ln': 'US National Oceanic and Atmospheric Administration',
              'chrono': 'To complete',
              'function_name': 'noaa',
              'link': 'https://tidesandcurrents.noaa.gov/',
              'type': 'national'}
norwegian_infos = {'sn': 'Norwegian TGN',
                   'ln': 'Norwegian Hydrographic Service Tide Gauge Network',
                   'chrono': 'To complete',
                   'function_name': 'norwtgn',
                   'link': 'https://www.kartverket.no/en/at-sea/se-havniva/se-havniva-i-kart',
                   'type': 'national'}


# Other portals (type 'other') to handle with hyperlinks informations in the dashboard interface
uhslc = {'sn': 'UHSLC',
         'ln': 'University of Hawaii Sea Level Center',
         'link': 'https://uhslc.soest.hawaii.edu/',
         'type': 'other'}
ioc = {'sn': 'IOC',
       'ln': 'Intergovernmental Oceanographic Commission of UNESCO',
       'link': 'https://ioc.unesco.org/',
       'type': 'other'}
psml_ell = {'sn': 'PSMSL_links',
            'ln': 'Permanent Service for Mean Sea Level - Tide gauge linked to a geocentric ellipsoid',
            'link': 'https://www.psmsl.org/data/obtaining/ellipsoidal_links.json',
            'type': 'other'}
psml_gnssir = {'sn': 'PSMSL_gnssir',
               'ln': 'Permanent Service for Mean Sea Level - GNSS Interferometric Reflectometry',
               'link': 'https://psmsl.org/data/gnssir/table.php',
               'type': 'other'}
uhslc_fd = {'sn': 'UHSLC_fd',
            'ln': 'University of Hawaii Sea Level Center - Fast Delivery data',
            'link': 'http://uhslc.soest.hawaii.edu/data/',
            'type': 'other'}
uhslc_rq = {'sn': 'UHSLC_rq',
            'ln': 'University of Hawaii Sea Level Center - Reasearch quality data',
            'link': 'http://uhslc.soest.hawaii.edu/data/',
            'type': 'other'}
canada = {'sn': 'cantgn',
          'ln': 'Canadian Station Inventory',
          'link': 'https://www.isdm-gdsi.gc.ca/isdm-gdsi/twl-mne/maps-cartes/inventory-inventaire-eng.asp?user=isdm-gdsi&region=MEDS&tst=1&perm=1',
          'type': 'other'}
norwegian = {'sn': 'norwtgn',
             'ln': 'Norwegian Station Inventory',
             'link': 'https://www.kartverket.no/en/at-sea/se-havniva/se-havniva-i-kart',
             'type': 'other'}

# Global variable that handle catalogs informations
_catalog_infos = [gloss_infos, ssc_infos, psmsl_infos, psmsl_met_infos, psml_ell_infos, psmsl_gnssir_infos,
                  uhslc_fd_infos, uhslc_rq_infos, slsmf_infos, sonel_infos,
                  bodc_infos, cmems_infos, eutgn_infos, emodnet_infos,
                  refmar_infos, redmar_infos, ptwc_infos, cmems_nrt_infos, gesla_infos, noaa_infos, norwegian_infos,
                  canada_infos,   # Comment this line if ChromeDriver is not working
                  uhslc, ioc, psml_ell, psml_gnssir, uhslc_fd, uhslc_rq, canada, norwegian, indonesian_infos, misela_infos]
