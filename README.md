<img src="tgcat/dashboard/assets/tgpi_logo.png" alt='TGPI logo' width="250">

*TGPI is a Python toolbox to scrape tide gauge informations from online portals to allow tide gauge operators, data providers 
and integrators to identify problems in their metadata (like duplicates or wrong coordinates).*

## Table of content

1. [Projet genesis](#project-genesis)
2. [Code deployment](#code-deployment)
3. [Toolbox phylosophy](#toolbox-philosophy)
    - [Python objects](#python-objects)
    - [Code organization](#code-organization)
    - [Dashboard interface](#dashboard-interface)
4. [FAQ](#faq)
    - [How to use the interface ?](#faq-1-how-to-use-the-interface-)
    - [How to add a new Catalog ?](#faq-2-how-to-add-a-new-catalog-)
    - [How to update the Catalog archives ?](#faq-3-how-to-update-the-catalog-archives-)
    - [Handle with Chrome Driver ?](#faq-4-handle-with-chromedriver-)
    - [How to add a new regional selection option ?](#faq-5-how-to-add-a-new-regional-selection-option-)
    - [What about the tide gauge data comparison ?](#faq-6-what-about-the-tide-gauge-data-comparison-)

## Project genesis

To support the progress of scientific knowledge on ocean climate, marine ecosystems and their vulnerability,
[EuroSea](https://eurosea.eu/) works to improve the European ocean observing and forecasting system.

This dashboard was created by the [SONEL](https://www.sonel.org/) team, with the financial support of [EuroSea](https://eurosea.eu/) in the framework of the
[EuroGOOS Tide Gauge Task Team](http://eurogoos.eu/tide-gauge-task-team/)
for [EuroGOOS](https://eurogoos.eu/) and [EOOS (European Ocean Observing System)](https://www.eoos-ocean.eu/),
to improve tide gauge data portals interoperability and allow data providers
and integrators to identify metadata problems in their datasets (duplicates, wrong coordinates, ...).

This Python toolbox gathers all the functionalities to scrape
informations of an online tide gauge data portal.
This content is then organized according to a unique model
(a Python class called *Catalog*) allowing a direct comparison
of different portals.

> :bulb: A distinction is made between:
>
>- **Metadata catalogs** : Portals that only give sites description without link to download data.
>- **Data catalogs** : Portals that provide data description and a possible link to the data (complete or not, free or not).

## Code deployment

- This toolbox is developp using [Python 3.11](https://www.python.org/downloads/release/python-3116/).
- Python environment is created using miniconda3 and few pip install : 
```shell
conda create --channel conda-forge --channel default --name tgpi python=3.11 shapely geopandas=0.14.1 netcdf4 beautifulsoup4 iso3166 xarray dash=2.14.1 plotly=5.18.0 dash-bootstrap-components geopy requests dash-daq diskcache psutil gunicorn multiprocess importlib_metadata
pip install 'h3==4.0.0b2'
```
_All required modules and version are available in the [*requirements.txt*](./requirements.txt) file._ \
_(psutil gunicorn multiprocess importlib_metadata could be usefull only for server deployment)_
- An online tool is available [here](https://www.sonel.org/tgcat-v2/).

> :bulb: The server deployment is managed by [SONEL](https://www.sonel.org/?lang=en) services.
> For more informations, please contact [sonel@sonel.org](sonel@sonel.org).

Create a config.py file from config.py.DIST. Update BASE_PATH if tgcat is deployed to a subpath on server root URL.

## Toolbox philosophy

### Python objects

The Python code is based on 2 main Class that interact together.

#### Site() : manage information related to a tide gauge site

Main attributes are :

- *lat* / *lon* / *name* : metadata about the site
- *provider* : list of providers for this site
- *link* : dictionnary with metadata weblinks for each portal
- *data* : dictionnary with data weblinks for each portal
- *id* : dictionnary of portals ID
- *pip* : dictionnary of boolean for presence in a portal

```python
# Definition of the LROCH tide gauge site
lroch = Site(lat=46.15847800, lon=-1.22073600, 
             name='lroch',  provider=["https://www.sonel.org/"], 
             link={'SONEL': ['https://www.sonel.org/?page=maregraphe&idStation=1787']},  
             data={'monthly_means':['https://www.sonel.org/msl/Doodson/VALIDATED/dLROCH_dec.slv']}, 
             id={'SONEL':[1787]},  
             pip={'SONEL':True}) 
```

#### Catalog() : handle multiple Site() objects

Main attributes are :

- *name* : name of the Catalog
- *sites* : list of Sites
- *color* : color for this portal

```python
# Definition of the PSMSL Catalog
psmsl = Catalog(name='psmsl',  
                sites=[site1, site2, site3, ...], 
                color='red') 
```

### Code organization

Main packages are :

- [*core*](./tgcat/core) : define the main Python objets used to handle portals data (Site and Catalog)
- [*collect*](./tgcat/collect) : define functions to scrape portals and create Catalogs objects
- [*dashboard*](./tgcat/dashboard) : define the app layout and callbacks

<img src="tgcat/dashboard/assets/tgpi_code_organization.png" alt='Code organization' width="800">

### Dashboard interface

A dashboard interface has been developed to package the Python code and allow a larger use of the tool. This dashboard is deployed on a [SONEL](https://www.sonel.org/?lang=en)
server and is [available online](https://www.sonel.org/tgcat/).

> :bulb: To simplify and speed up the processing, the online app is based archives use. This means that the data portals are automatically scrapped and converted  
>into archive files. Dashboard users can then directly query this archive database. Only the server administrator has the ability to launch an archive update
>(see [FAQ 3](#faq-3-how-to-update-the-catalog-archives-)).

Main interface description :

:one: Pannel for Catalog(s) selection (checkboxes).

:two: Pannel to display some statistics about the loaded Catalog(s).

:three: Pannel to show the map of the selected Catalog(s) sites.

:four: Table with selected Catalog(s) sites main informations.

:five: Pannel that display Site information after selecting 1 site.

:six: Display the potential similar sites from other (or the same) Catalog(s).

<img src="tgcat/dashboard/assets/tgpi_interface_1.png" alt='Interface' width="1200">

:seven: When clicking on "Display site analyse", a second interface appear with all results of the potential identical site analyse.

<img src="tgcat/dashboard/assets/tgpi_interface_2.PNG" alt='Interface' width="1200">

> :information_source: All the info circle are clickable and display aditional informations. 

## FAQ

### FAQ 1 : How to use the interface ?

- Select and load the requested Catalog(s) in area :one: and click on "Load selected Catalog(s)". 

> :bulb: The main informations about the selected Catalog(s) and their sites display on area :two:, :three:, :four:. 

- Select multiple sites by :
  - Selecting an area in the sliding menu :three:.
  - Drawing an area on the map :three: using mapbox tools.
  - Filtering table :four: with the header selection tool, and clicking on "Update Selection". 

> :bulb: After a multi-site selection, the statistics graphs in :two: are updated.

- Select 1 site by :
  - Clicking on the corresponding location on map :three:.
  - Selecting the corresponding line in table :four:.

> :bulb: After a 1-site selection, the pannel :five: display the main informations for the site. You can also launch the "Potential identical site analyse" in the area :six:.

- Reset the selection by clicking on "Clear selection" in area :four:.

### FAQ 2 : How to add a new Catalog ?

To add a new Catalog to the project, follow these steps :

<img src="tgcat/dashboard/assets/tgpi_add_scrapping_function.png" alt='Add method' width="1200">

After adding and testing the new function, do not forget to update the Catalogs archives (see [FAQ 3](#faq-3-how-to-update-the-catalog-archives-)). 

> :bulb: Existing functions can help: before coding, try to find another portal close to the new one, and use its code to start the development.  

### FAQ 3 : How to update the Catalog archives ?

As detailed in [section 3.3](#dashboard-interface), the online dashboard is based on Catalogs archives, located in the SONEL server. 
These archives are regularly updated thanks to a command line programm stored in ```collect > archive.py```.

To launch an update of the archive, open a command prompt in the ```collect``` folder and launch :

```bash
python archive.py [-cat CATALOGS_NAME] [all] [no_chrome] [--help]
```

Options are :

- **-cat [CATALOGS_NAME]** : Give the list of Catalogs to scrape (ex: ['ssc','gloss'])
- **all** : Scrape and update all the available catalogs
- **no_chrome** : Do not scrap catalogs that need ChromeDriver execution
- **-h** or **--help** : Display the help menu

```bash
# Scrape and update Catalogs 'sonel','ssc' and 'psmsl'
python archive.py -cat ['sonel','ssc', 'psmsl']

# Scrape and update all available Catalogs except those using ChromeDriver execution
python archive.py all no_chrome

# Display the help
python archive.py --help
```

### FAQ 4 : Handle with ChromeDriver ?

To scrape some portals, TGPI use the python library ```selenium``` (for more details, go to the [package description](https://selenium-python.readthedocs.io/index.html)).
It could be installed on Python 3 using :

```python
pip install selenium
# See https://pypi.org/project/selenium/ for more details
```

```Selenium``` could be used to access webpages : for that, it requires a driver to interface with the chosen browser,
which needs to be download in the ```collect > ressources``` folder.

In TGPI, we consider ChromeDriver, that could be installed on Windows/Mac/Linux folowing theses steps :

1. Ensure Chromium/Google Chrome is installed on your system in a recognized location
2. [Download](https://chromedriver.chromium.org/downloads) the ChromeDriver binary for your platform (Windows/Mac/Linux)
3. Store it in the  ```collect > ressources``` folder
4. Include the ChromeDriver location in your PATH environment variable

```Selenium``` could then be used to open a webpage :

````python
import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By

# Define the browser options
options = webdriver.ChromeOptions()
options.add_argument("start-maximized") # Open the webpage at its maximum size

# Browser options depending on the system 
if os.name=='nt':   # Windows
    options.add_argument('--headless') # Runs Chrome in headless mode.
    options.add_argument('--no-sandbox') # Bypass OS security model
    options.add_argument('--disable-dev-shm-usage')
    # Create the instance of Chrome WebDriver stored in the collect > ressources folder
    driver = webdriver.Chrome('link_to_chromedriver_exe', options=options)
else :  # Linux
    options.add_argument("user-data-dir="+_app_config['CHROME_DATA_DIR'])
    # Create the instance of Chrome WebDriver stored in the collect > ressources folder
    driver = webdriver.Chrome('link_to_chromedriver', options=options)

# Load the given URL webpage
driver.get('link_to_the_page') 
time.sleep(3) # Time break to allow the page to load

# Usefull methods (examples)
page_list = driver.find_elements(By.XPATH, "//a[@class='paginate_button ']") # Find a page element in the page
html = driver.page_source # Read and store the HTML source code of the page
driver.find_elements(By.ID, "submit").click() # Click on a 'SUBMIT' button 

driver.quit() # Close the webpage (save memroy place)
````

> :bulb: More details about Selenium/ChromeDriver options and methods are available [here](https://selenium-python.readthedocs.io/navigating.html). 

### FAQ 5 : How to add a new regional selection option ?

Thanks to a drop-down menu (see area :three: in [section 3](#dashboard-interface)), the user can select sites in a predefine area.

These pre-defined area are Polygons objects, stored in a .shp file that could be edited with [QGIS](https://www.qgis.org/fr/site/) or other SIG tools.

To modify or add a new area with QGIS, open the shapefile located in ```dashboard > assets > world_area.shp``` (step :one:). The predefined area are stored
in a datatable that associate a name and the corresponding Polygon (or MultiPolygon).

<img src="tgcat/dashboard/assets/tgpi_add_area.png" alt='Add method' width="1200">

> :bulb: To add an Open Street Map background, double-click on the ```Explorer > XYZ Tiles > OpenStreetMap```.
> If the OpenStreetMap raster tile it is not available in your QGIS version, follow [this tutorial](https://www.giscourse.com/how-to-add-openstreetmap-basemaps-in-qgis-3-0/).

To add a new Geometry, select the ```world_area``` layer (step :one:) and switch to the edit mode (step :two: Click on the yellow pencil).
Then click on ```Add Polygon``` (step :two: Click on the filled Polygon) and draw the new area in the central map.
When finished, click right on the mouse : it will automatically open the attribute window (step :three:).
Give the name of the new area and confirm.

By saving the new world_area.shp file in ```dashboard > assets > world_area.shp```, the drop-down selection menu will automatically be updated.

> :warning: If the new geometry is a MultiPolygon, you must also update the ```config.txt``` file at line 35
>(add the area name to the test, next to 'oceania')

### FAQ 6 : What about the tide gauge data comparison ?

There is various data format in the different Tide Gauge portals
(online csv or json file, zip files to download with 1 site or all the sites in the world,
difference in units references, particular formatting,...).
To keep the interface efficient and not to duplicate existing databases,
the TGPI interface do not display the datasets.

However, the aim of the project is to compare metadata AND data from portals.
For that, when analysing the potential identical sites (step :seven: [the interface description](#dashboard-interface)),
the interface provides an detailed list of links to the data.
The downloading, use and interpretation of these data are left to the user.

An example of datasets comparison is available in the ```notebooks``` folder for the Nouméa Tide Gauge site in New Caledonia.
Results show that there are many sources of data, with different formats, frequencies and durations.
The non-uniformity of site identifiers also makes comparison more complicated:
some datasets are identical but are located at different points.
Others are different but associated with the same geographic site.
Therefore, care must be taken when using this information.

<img src="notebooks/noumea_analyse/Noumea-Datasets_Analyse.png" alt='Noumea datasets analyse' width="1800">

*@TGPI - April 2022*
